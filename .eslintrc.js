module.exports = {
  parserOptions: {
    parser: "@typescript-eslint/parser",
    extensions: ["eslint-mdx"],
    ecmaVersion: 2018,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  root: true,
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:jsx-a11y/recommended",
    "plugin:mdx/recommended",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended",
  ],
  plugins: [
    "@typescript-eslint",
    "@typescript-eslint/eslint-plugin",
    "react",
    "react-hooks",
    "simple-import-sort",
    "tsdoc",
    "markdown",
    "prettier",
  ],
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  rules: {
    "prettier/prettier": [
      "error",
      {
        trailingComma: "es5",
      },
    ],
    camelcase: "off",
    "@typescript-eslint/camelcase": ["off"],
    "simple-import-sort/sort": "error",
    // "jsdoc/check-tag-names": ["warn", { definedTags: ["export", "extends"] }],
    // "jsdoc/no-undefined-types": [1, { definedTypes: ["unknown", "JSX"] }],
    "tsdoc/syntax": "warn",
  },
  settings: {
    react: {
      version: "detect",
    },
    jsdoc: {
      mode: "typescript",
      tagNamePreference: {
        augments: {
          message:
            "@extends is to be used over @augments as it is more evocative of classes than @augments",
          replacement: "extends",
        },
      },
      structuredTags: {
        export: {
          name: false,
          type: false,
        },
        extends: {
          name: false,
          type: true,
        },
      },
    },
  },
  overrides: [
    {
      files: ["*.js", "*.jsx"],
      rules: {
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
      },
    },
    {
      files: ["*.ts", "*.tsx"],
      rules: {
        "react/prop-types": "off",
      },
    },
    // {
    //   files: ["**/*.md"],
    //   processor: "markdown/markdown"
    // },
    {
      files: ["**/*.md/*.js"],
      rules: {
        "no-console": "off",
        "import/no-unresolved": "off",
      },
    },
    {
      files: ["**/*.md"],
      rules: {
        "no-undef": "off",
        "no-unused-vars": "off",
        "no-console": "off",
        "padded-blocks": "off",
      },
    },
  ],
}
