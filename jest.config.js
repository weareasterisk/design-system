module.exports = {
  preset: "ts-jest",
  // The root of your source code, typically /src
  // `<rootDir>` is a token Jest substitutes
  projects: ["<rootDir>/packages/*"],
  moduleDirectories: ["node_modules"],

  // Jest transformations -- this adds support for TypeScript
  // using ts-jest
  transform: {
    "^.+\\.(ts|tsx|js|jsx)?$": "ts-jest",
  },

  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.test.json",
    },
  },

  // Runs special logic, such as cleaning up components
  // when using React Testing Library and adds special
  // extended assertions to Jest
  setupFilesAfterEnv: [
    "@testing-library/jest-dom/extend-expect",
    require.resolve("./config/setupTests.ts"),
  ],

  // Test spec file resolution pattern
  // Matches parent folder `__tests__` and filename
  // should contain `test` or `spec`.
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  cacheDirectory: ".jest-cache",
  coverageDirectory: ".jest-coverage",
  coveragePathIgnorePatterns: [
    "<rootDir>/packages/(?:.+?)/lib/",
    "__ignored__",
    "<rootDir>/packages/(?:.+?)/dist/",
  ],
  coverageReporters: ["html", "text"],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  // Module file extensions for importing
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  testPathIgnorePatterns: [
    "<rootDir>/packages/(?:.+?)/lib/",
    "__ignored__",
    "<rootDir>/packages/(?:.+?)/dist/",
  ],
  modulePathIgnorePatterns: ["<rootDir>/packages/(?:.+?)/dist/", "<rootDir>/packages/(?:.+?)/lib/"],
}
