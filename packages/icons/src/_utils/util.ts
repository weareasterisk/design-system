export const DISPLAYNAME_PREFIX = "Hyphen-UI"

/**
 * Generate the display name for a component with the proper prefix
 */
export const generateDisplayName = (componentName: string): string =>
  `${DISPLAYNAME_PREFIX}.${componentName}`
