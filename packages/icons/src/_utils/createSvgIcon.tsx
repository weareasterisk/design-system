import React from "react"

import SvgIcon from "./SvgIcon"
import { generateDisplayName } from "./util"

const createSvgIcon = (
  path: React.ReactNode,
  displayName: string,
  viewBox?: string
): typeof SvgIcon => {
  const Component = React.forwardRef((props, ref) => (
    <SvgIcon ref={ref} viewBox={viewBox} title={displayName} {...props}>
      {path}
    </SvgIcon>
  ))

  Component.displayName = generateDisplayName(displayName)

  return React.memo(Component)
}

export default createSvgIcon
