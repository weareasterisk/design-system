import createSvgIcon from "./createSvgIcon"
import SvgIcon from "./SvgIcon"

export { createSvgIcon, SvgIcon }

export default {
  createSvgIcon,
  SvgIcon,
}
