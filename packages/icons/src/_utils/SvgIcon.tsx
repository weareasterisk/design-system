import "./styles.scss"

import classNames from "classnames"
import React from "react"

import { generateDisplayName } from "./util"

export interface SvgIconProps
  extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLOrSVGElement>, HTMLOrSVGElement> {
  component?: "svg" | React.ElementType

  color?: "inherit" | string
  title?: string
  viewBox?: string
  name?: string
}

const SvgIcon = React.forwardRef<unknown, SvgIconProps>(
  (
    {
      children,
      className,
      component: Component = "svg",
      color = "inherit",
      title,
      viewBox = "0 0 24 24",
      name,
      ...props
    },
    ref
  ) => {
    return (
      <Component
        className={classNames(className, "hui-svg-icon")}
        focusable="false"
        viewBox={viewBox}
        color={color}
        aria-hidden={title ? undefined : true}
        aria-label={name}
        role={title ? "img" : undefined}
        ref={ref}
        {...props}
      >
        {children}
        {title ? <title>{title}</title> : null}
      </Component>
    )
  }
)

SvgIcon.displayName = generateDisplayName("SvgIcon")

export default SvgIcon
