import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M10 19a1 1 0 01-.64-.23 1 1 0 01-.13-1.41L13.71 12 9.39 6.63a1 1 0 01.15-1.41 1 1 0 011.46.15l4.83 6a1 1 0 010 1.27l-5 6A1 1 0 0110 19z"
      data-name="arrow-ios-forward"
    />
  </g>,
  "ArrowIosForward",
  "0 0 24 24"
)
