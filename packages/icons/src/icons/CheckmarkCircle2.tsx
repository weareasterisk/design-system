import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M12 2a10 10 0 1010 10A10 10 0 0012 2zm4.3 7.61l-4.57 6a1 1 0 01-.79.39 1 1 0 01-.79-.38l-2.44-3.11a1 1 0 011.58-1.23l1.63 2.08 3.78-5a1 1 0 111.6 1.22z"
      data-name="checkmark-circle-2"
    />
  </g>,
  "CheckmarkCircle2",
  "0 0 24 24"
)
