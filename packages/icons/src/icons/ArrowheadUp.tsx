import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="arrowhead-up">
        <path d="M6.63 11.61L12 7.29l5.37 4.48A1 1 0 0018 12a1 1 0 00.77-.36 1 1 0 00-.13-1.41l-6-5a1 1 0 00-1.27 0l-6 4.83a1 1 0 00-.15 1.41 1 1 0 001.41.14z" />
        <path d="M12.64 12.23a1 1 0 00-1.27 0l-6 4.83a1 1 0 00-.15 1.41 1 1 0 001.41.15L12 14.29l5.37 4.48A1 1 0 0018 19a1 1 0 00.77-.36 1 1 0 00-.13-1.41z" />
      </g>
    </g>
  </React.Fragment>,
  "ArrowheadUp",
  "0 0 24 24"
)
