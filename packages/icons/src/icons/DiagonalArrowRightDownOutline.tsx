import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M17 8a1 1 0 00-1 1v5.59l-8.29-8.3a1 1 0 00-1.42 1.42l8.3 8.29H9a1 1 0 000 2h8a1 1 0 001-1V9a1 1 0 00-1-1z"
      data-name="diagonal-arrow-right-down"
    />
  </g>,
  "DiagonalArrowRightDownOutline",
  "0 0 24 24"
)
