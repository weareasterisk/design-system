import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="list">
        <circle cx="4" cy="7" r="1" />
        <circle cx="4" cy="12" r="1" />
        <circle cx="4" cy="17" r="1" />
        <rect width="14" height="2" x="7" y="11" rx=".94" ry=".94" />
        <rect width="14" height="2" x="7" y="16" rx=".94" ry=".94" />
        <rect width="14" height="2" x="7" y="6" rx=".94" ry=".94" />
      </g>
    </g>
  </React.Fragment>,
  "List",
  "0 0 24 24"
)
