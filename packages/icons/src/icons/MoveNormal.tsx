import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <path d="M12 18a2 2 0 100-4 2 2 0 100 4zm8 0a2 2 0 100-4 2 2 0 100 4zm-8-7a2 2 0 100-4 2 2 0 100 4zm8 0a2 2 0 100-4 2 2 0 100 4zm-8 14a2 2 0 100-4 2 2 0 100 4zm8 0a2 2 0 100-4 2 2 0 100 4z" />,
  "MoveNormal",
  ""
)
