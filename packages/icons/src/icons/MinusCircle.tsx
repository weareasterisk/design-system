import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M12 2a10 10 0 1010 10A10 10 0 0012 2zm3 11H9a1 1 0 010-2h6a1 1 0 010 2z"
      data-name="minus-circle"
    />
  </g>,
  "MinusCircle",
  "0 0 24 24"
)
