import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="radio-button-on">
        <path d="M12 2a10 10 0 1010 10A10 10 0 0012 2zm0 18a8 8 0 118-8 8 8 0 01-8 8z" />
        <path d="M12 7a5 5 0 105 5 5 5 0 00-5-5z" />
      </g>
    </g>
  </React.Fragment>,
  "RadioButtonOn",
  "0 0 24 24"
)
