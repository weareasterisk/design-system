import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <g data-name="person-remove">
      <path d="M21 6h-4a1 1 0 000 2h4a1 1 0 000-2zM10 11a4 4 0 10-4-4 4 4 0 004 4zM16 21a1 1 0 001-1 7 7 0 00-14 0 1 1 0 001 1" />
    </g>
  </g>,
  "PersonRemove",
  "0 0 24 24"
)
