import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <g data-name="monitor">
      <path d="M19 3H5a3 3 0 00-3 3v5h20V6a3 3 0 00-3-3zM2 14a3 3 0 003 3h6v2H7a1 1 0 000 2h10a1 1 0 000-2h-4v-2h6a3 3 0 003-3v-1H2z" />
    </g>
  </g>,
  "Monitor",
  "0 0 24 24"
)
