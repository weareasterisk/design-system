import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="archive">
        <path d="M21 6a3 3 0 00-3-3H6a3 3 0 00-2 5.22V18a3 3 0 003 3h10a3 3 0 003-3V8.22A3 3 0 0021 6zM6 5h12a1 1 0 010 2H6a1 1 0 010-2zm12 13a1 1 0 01-1 1H7a1 1 0 01-1-1V9h12z" />
        <rect width="6" height="2" x="9" y="12" rx=".87" ry=".87" />
      </g>
    </g>
  </React.Fragment>,
  "ArchiveOutline",
  "0 0 24 24"
)
