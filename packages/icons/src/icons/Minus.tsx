import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path d="M19 13H5a1 1 0 010-2h14a1 1 0 010 2z" data-name="minus" />
  </g>,
  "Minus",
  "0 0 24 24"
)
