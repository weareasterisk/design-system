import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="thermometer-minus">
        <rect width="6" height="2" x="2" y="5" rx="1" ry="1" />
        <path d="M14 22a5 5 0 01-3-9V5a3 3 0 013-3 3 3 0 013 3v8a5 5 0 01-3 9zm1-12.46V5a.93.93 0 00-.29-.69A1 1 0 0014 4a1 1 0 00-1 1v4.54z" />
      </g>
    </g>
  </React.Fragment>,
  "ThermometerMinus",
  "0 0 24 24"
)
