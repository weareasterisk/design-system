import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M18 3H6a3 3 0 00-3 3v12a3 3 0 003 3h7V11h4v10h1a3 3 0 003-3V6a3 3 0 00-3-3z"
      data-name="npm"
    />
  </g>,
  "Npm",
  "0 0 24 24"
)
