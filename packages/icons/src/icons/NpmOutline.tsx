import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="npm">
        <path d="M18 21H6a3 3 0 01-3-3V6a3 3 0 013-3h12a3 3 0 013 3v12a3 3 0 01-3 3zM6 5a1 1 0 00-1 1v12a1 1 0 001 1h12a1 1 0 001-1V6a1 1 0 00-1-1z" />
        <path d="M12 9h4v10h-4z" />
      </g>
    </g>
  </React.Fragment>,
  "NpmOutline",
  "0 0 24 24"
)
