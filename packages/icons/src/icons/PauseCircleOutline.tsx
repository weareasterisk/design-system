import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="pause-circle">
        <path d="M12 2a10 10 0 1010 10A10 10 0 0012 2zm0 18a8 8 0 118-8 8 8 0 01-8 8z" />
        <path d="M15 8a1 1 0 00-1 1v6a1 1 0 002 0V9a1 1 0 00-1-1zM9 8a1 1 0 00-1 1v6a1 1 0 002 0V9a1 1 0 00-1-1z" />
      </g>
    </g>
  </React.Fragment>,
  "PauseCircleOutline",
  "0 0 24 24"
)
