import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M12 2a10 10 0 1010 10A10 10 0 0012 2zm-2 13a1 1 0 01-2 0V9a1 1 0 012 0zm6 0a1 1 0 01-2 0V9a1 1 0 012 0z"
      data-name="pause-circle"
    />
  </g>,
  "PauseCircle",
  "0 0 24 24"
)
