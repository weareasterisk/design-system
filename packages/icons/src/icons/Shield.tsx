import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M12 21.85a2 2 0 01-1-.25l-.3-.17A15.17 15.17 0 013 8.23v-.14a2 2 0 011-1.75l7-3.94a2 2 0 012 0l7 3.94a2 2 0 011 1.75v.14a15.17 15.17 0 01-7.72 13.2l-.3.17a2 2 0 01-.98.25z"
      data-name="shield"
    />
  </g>,
  "Shield",
  "0 0 24 24"
)
