import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M13.67 22h-.06a1 1 0 01-.92-.8L11 13l-8.2-1.69a1 1 0 01-.12-1.93l16-5.33A1 1 0 0120 5.32l-5.33 16a1 1 0 01-1 .68zm-6.8-11.9l5.19 1.06a1 1 0 01.79.78l1.05 5.19 3.52-10.55z"
      data-name="navigation-2"
    />
  </g>,
  "Navigation2Outline",
  "0 0 24 24"
)
