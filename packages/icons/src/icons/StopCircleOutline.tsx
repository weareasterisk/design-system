import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="stop-circle">
        <path d="M12 2a10 10 0 1010 10A10 10 0 0012 2zm0 18a8 8 0 118-8 8 8 0 01-8 8z" />
        <path d="M14.75 8h-5.5A1.25 1.25 0 008 9.25v5.5A1.25 1.25 0 009.25 16h5.5A1.25 1.25 0 0016 14.75v-5.5A1.25 1.25 0 0014.75 8zM14 14h-4v-4h4z" />
      </g>
    </g>
  </React.Fragment>,
  "StopCircleOutline",
  "0 0 24 24"
)
