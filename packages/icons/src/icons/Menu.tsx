import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="menu">
        <rect width="18" height="2" x="3" y="11" rx=".95" ry=".95" />
        <rect width="18" height="2" x="3" y="16" rx=".95" ry=".95" />
        <rect width="18" height="2" x="3" y="6" rx=".95" ry=".95" />
      </g>
    </g>
  </React.Fragment>,
  "Menu",
  "0 0 24 24"
)
