import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M17.71 6.29a1 1 0 00-1.42 0L8 14.59V9a1 1 0 00-2 0v8a1 1 0 001 1h8a1 1 0 000-2H9.41l8.3-8.29a1 1 0 000-1.42z"
      data-name="diagonal-arrow-left-down"
    />
  </g>,
  "DiagonalArrowLeftDownOutline",
  "0 0 24 24"
)
