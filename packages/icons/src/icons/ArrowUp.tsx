import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M16.21 16H7.79a1.76 1.76 0 01-1.59-1 2.1 2.1 0 01.26-2.21l4.21-5.1a1.76 1.76 0 012.66 0l4.21 5.1A2.1 2.1 0 0117.8 15a1.76 1.76 0 01-1.59 1z"
      data-name="arrow-up"
    />
  </g>,
  "ArrowUp",
  "0 0 24 24"
)
