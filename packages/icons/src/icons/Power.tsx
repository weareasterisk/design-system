import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <React.Fragment>
    <g data-name="Layer 2">
      <g data-name="power">
        <path d="M12 13a1 1 0 001-1V2a1 1 0 00-2 0v10a1 1 0 001 1z" />
        <path d="M16.59 3.11a1 1 0 00-.92 1.78 8 8 0 11-7.34 0 1 1 0 10-.92-1.78 10 10 0 109.18 0z" />
      </g>
    </g>
  </React.Fragment>,
  "Power",
  "0 0 24 24"
)
