import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <g data-name="person-delete">
      <path d="M20.47 7.5l.73-.73a1 1 0 00-1.47-1.47L19 6l-.73-.73a1 1 0 00-1.47 1.5l.73.73-.73.73a1 1 0 001.47 1.47L19 9l.73.73a1 1 0 001.47-1.5zM10 11a4 4 0 10-4-4 4 4 0 004 4zm0-6a2 2 0 11-2 2 2 2 0 012-2zM10 13a7 7 0 00-7 7 1 1 0 002 0 5 5 0 0110 0 1 1 0 002 0 7 7 0 00-7-7z" />
    </g>
  </g>,
  "PersonDeleteOutline",
  "0 0 24 24"
)
