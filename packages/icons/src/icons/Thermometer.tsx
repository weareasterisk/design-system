import React from "react"

import createSvgIcon from "../_utils/createSvgIcon"

export default createSvgIcon(
  <g data-name="Layer 2">
    <path
      d="M12 22a5 5 0 01-3-9V5a3 3 0 013-3 3 3 0 013 3v8a5 5 0 01-3 9zm1-12.46V5a.93.93 0 00-.29-.69A1 1 0 0012 4a1 1 0 00-1 1v4.54z"
      data-name="thermometer"
    />
  </g>,
  "Thermometer",
  "0 0 24 24"
)
