import * as IconNames from "./icons"

export * from "./icons"
export * from "./_utils"

export type IconType = typeof IconNames
