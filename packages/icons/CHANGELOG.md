# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.5.2](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.5.1...@weareasterisk/hyphen-ui-icons@1.5.2) (2021-01-03)


### Bug Fixes

* fix readmes ([9003cfe](https://gitlab.com/weareasterisk/design-system/commit/9003cfe9afe06580dd84719cb81b9e00ec684f16))





## [1.5.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.5.0...@weareasterisk/hyphen-ui-icons@1.5.1) (2020-10-08)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-icons





# [1.5.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.4.1...@weareasterisk/hyphen-ui-icons@1.5.0) (2020-10-05)


### Features

* **icons:** add move-normal icon ([f2449c8](https://gitlab.com/weareasterisk/design-system/commit/f2449c814c01da3a15c41780a89584b41bd3028a))





## [1.4.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.4.0...@weareasterisk/hyphen-ui-icons@1.4.1) (2020-09-21)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-icons





# [1.4.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.3.1...@weareasterisk/hyphen-ui-icons@1.4.0) (2020-09-12)


### Features

* export type with icon names in enmum-like format ([0a30b47](https://gitlab.com/weareasterisk/design-system/commit/0a30b47d82cd19a4e0e242c283e7f4a76ed55af0))





## [1.3.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.3.0...@weareasterisk/hyphen-ui-icons@1.3.1) (2020-09-05)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-icons





# [1.3.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.2.0...@weareasterisk/hyphen-ui-icons@1.3.0) (2020-08-16)


### Features

* create global storybook configuration instead of per-project ([e2c3ccf](https://gitlab.com/weareasterisk/design-system/commit/e2c3ccf29e158a7d7649c9fbdf2553587d063b0e))
* inherit font size from the parent component rendering the icon ([d0d14aa](https://gitlab.com/weareasterisk/design-system/commit/d0d14aac5945bf8138fb984127c8145548d62bc4))





# [1.2.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.0.0...@weareasterisk/hyphen-ui-icons@1.2.0) (2020-08-11)


### Features

* derive viewbox from input svg and statically assign to prevent sizing errors ([db89fae](https://gitlab.com/weareasterisk/design-system/commit/db89faee9a4791cdad739734cb374b66d5bf1c2b))





# [1.1.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-icons@1.0.0...@weareasterisk/hyphen-ui-icons@1.1.0) (2020-08-11)


### Features

* derive viewbox from input svg and statically assign to prevent sizing errors ([db89fae](https://gitlab.com/weareasterisk/design-system/commit/db89faee9a4791cdad739734cb374b66d5bf1c2b))





# @weareasterisk/hyphen-ui-icons 1.0.0 (2020-08-09)


### Features

* init icons package with generator for react components using the generic SvgIcon component in core ([92c266d](https://gitlab.com/weareasterisk/design-system/commit/92c266d5153e675ac9cad7f8ff4bf61eb32d80f9))


### Dependencies

* **@weareasterisk/hyphen-ui-core:** upgraded to 1.1.0
