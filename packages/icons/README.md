# @weareasterisk/hyphen-ui-icons

## How to add a custom icon.

1. Add your custom svgs into `src/svg/custom`
2. Name them accordingly (kebab-case). Make sure there isn't any naming conflicts with any of
   existing svgs (aka inspect `svg/eva/**/*` and `svg/custom/*`)
3. Run `yarn src:icons`. This will convert the svgs into React components.
4. Run `yarn build`. This will bundle the components into the dist folder and will eventually make
   your custom component accessible to other packages.

Tip: Only commit the following: - The svg files you added to `src/svg/custom`.

- The `src/icons/index.ts`. This "catalogues" your new svg components for future usage.
- The newly generated React files for YOUR custom svgs. DO NOT add the other updated icons - it will
  cause a huge headache whenever reviewing the changes if you re-commit the >400 icons.
