const fse = require("fs-extra")
const glob = require("glob")
const Mustache = require("mustache")
const path = require("path")
const rimraf = require("rimraf")
const SVGO = require("svgo")
const util = require("util")

const outputPath = path.resolve(path.join(__dirname, "src/icons"))
console.log(outputPath)

const globAsync = util.promisify(glob)

const svgo = new SVGO({
  floatPrecision: 4,
  plugins: [
    { cleanupAttrs: true },
    { removeDoctype: true },
    { removeXMLProcInst: true },
    { removeComments: true },
    { removeMetadata: true },
    { removeTitle: true },
    { removeDesc: true },
    { removeUselessDefs: true },
    { removeXMLNS: true },
    { removeEditorsNSData: true },
    { removeEmptyAttrs: true },
    { removeHiddenElems: true },
    { removeEmptyText: true },
    { removeEmptyContainers: true },
    { removeViewBox: true },
    { cleanupEnableBackground: true },
    { minifyStyles: true },
    { convertStyleToAttrs: true },
    { convertColors: true },
    { convertPathData: true },
    { convertTransform: true },
    { removeUnknownsAndDefaults: true },
    { removeNonInheritableGroupAttrs: true },
    {
      removeUselessStrokeAndFill: {
        // https://github.com/svg/svgo/issues/727#issuecomment-303115276
        removeNone: true,
      },
    },
    { removeUnusedNS: true },
    { cleanupIDs: true },
    { cleanupNumericValues: true },
    { cleanupListOfValues: true },
    { moveElemsAttrsToGroup: true },
    { moveGroupAttrsToElems: true },
    { collapseGroups: true },
    { removeRasterImages: true },
    { mergePaths: true },
    { convertShapeToPath: true },
    { sortAttrs: true },
    { removeDimensions: true },
    { removeAttrs: true },
    { removeElementsByAttr: true },
    { removeStyleElement: true },
    { removeScriptElement: true },
  ],
})

const getComponentName = (destPath) => {
  const splitRegex = new RegExp(`[\\${path.sep}-]+`)

  const paths = destPath
    .replace(".tsx", "")
    .split(splitRegex)
    .map((part) => part.charAt(0).toUpperCase() + part.substring(1))

  return paths.join("")
}

const cleanPaths = async ({ data }) => {
  // Optimize SVG data using SVGO
  const result = await svgo.optimize(data)

  const match = await data.match(/viewBox="([^"]+)"/)

  let viewBox

  if (match && match.length > 0) {
    viewBox = match[1]
  }

  // Extract paths from SVG string and clean
  let paths = result.data
    .replace(/<svg[^>]*>/g, "")
    .replace(/<\/svg>/g, "")
    .replace(/"\/>/g, '" />')
    .replace(/fill-opacity=/g, "fillOpacity=")
    .replace(/xlink:href=/g, "xlinkHref=")
    .replace(/clip-rule=/g, "clipRule=")
    .replace(/fill-rule=/g, "fillRule=")
    .replace(/ clip-path=".+?"/g, "") // Fix visibility issue and save some bytes.
    .replace(/<clipPath.+?<\/clipPath>/g, "") // Remove unused definitions

  // Add a fragment when necessary.
  if ((paths.match(/\/>/g) || []).length > 1) {
    paths = `<React.Fragment>${paths}</React.Fragment>`
  }

  return {
    paths,
    viewBox,
  }
}

const renameFilter = (svgPathObj) => {
  let filename = svgPathObj.base
  filename = filename.replace(".svg", ".tsx")
  filename = filename.replace(/(^.)|(-)(.)/g, (match, p1, p2, p3) => (p1 || p3).toUpperCase())
  return path.join(filename)
}

const worker = async (svgPath, template) => {
  process.stdout.write(".")

  const normalizedSvgPath = path.normalize(svgPath)
  const svgPathObj = path.parse(normalizedSvgPath)

  // const innerPath = path
  //   .dirname(normalizedSvgPath)
  //   .replace(options.svgDirPath, "")
  //   .replace(path.relative(process.cwd(), options.svgDirPath), "") // for relative dirs

  const destPath = renameFilter(svgPathObj)

  const outputFileDir = path.dirname(path.join(outputPath, destPath))
  const exists2 = await fse.exists(outputFileDir)

  if (!exists2) {
    console.log(`Creating dir: ${outputFileDir}`)
    fse.mkdirpSync(outputFileDir)
  }

  const data = await fse.readFile(svgPath, { encoding: "utf8" })
  const paths = await cleanPaths({ svgPath, data })

  const fileString = Mustache.render(template, {
    paths: paths.paths,
    componentName: getComponentName(destPath),
    viewBox: paths.viewBox,
  })

  const absDestPath = path.join(outputPath, destPath)
  await fse.writeFile(absDestPath, fileString)
}

/**
 * Generate the index.ts file to aggregate all of the icons
 *
 */
async function generateIndex() {
  const files = await globAsync(path.join(outputPath, "*.tsx"))
  const index = files.map((file) => {
    const typeName = path.basename(file).replace(".tsx", "")
    return `export { default as ${typeName} } from "./${typeName}"`
  })
  index.push("\n")
  await fse.writeFile(path.join(outputPath, "index.ts"), index.join("\n"))
}

/**
 * Run the builder
 */
async function main() {
  const paths = [
    {
      dir: "svg/eva/outline",
    },
    {
      dir: "svg/eva/fill",
    },
    {
      dir: "svg/custom",
    },
  ]

  try {
    // Clean old files
    rimraf.sync(`${outputPath}/*.tsx`)

    const exists1 = await fse.exists(outputPath)
    if (!exists1) {
      fse.mkdirp(outputPath)
    }

    paths.map(async (svgDirPathData) => {
      const svgDirPath = svgDirPathData.dir
      // eslint-disable-next-line prettier/prettier
      const [svgPaths, template] = await Promise.all([
        globAsync(path.join(__dirname, svgDirPath, "*.svg")),
        fse.readFile(path.join(__dirname, "iconTemplate"), {
          encoding: "utf8",
        }),
      ])
      const promises = []

      svgPaths.map((svgPath) => {
        promises.push(worker(svgPath, template))
      })

      await Promise.all(promises)

      await generateIndex()
    })
  } catch (e) {
    console.log(e)
  }
}

main()
