import Alignment from "@ckeditor/ckeditor5-alignment/src/alignment"
import Autoformat from "@ckeditor/ckeditor5-autoformat/src/autoformat"
import Autosave from "@ckeditor/ckeditor5-autosave/src/autosave"
import Bold from "@ckeditor/ckeditor5-basic-styles/src/bold"
import Code from "@ckeditor/ckeditor5-basic-styles/src/code"
import Italic from "@ckeditor/ckeditor5-basic-styles/src/italic"
import Strikethrough from "@ckeditor/ckeditor5-basic-styles/src/strikethrough"
import Subscript from "@ckeditor/ckeditor5-basic-styles/src/subscript"
import Superscript from "@ckeditor/ckeditor5-basic-styles/src/superscript"
import Underline from "@ckeditor/ckeditor5-basic-styles/src/underline"
import BlockQuote from "@ckeditor/ckeditor5-block-quote/src/blockquote"
import CodeBlock from "@ckeditor/ckeditor5-code-block/src/codeblock"
import ClassicEditor from "@ckeditor/ckeditor5-editor-classic/src/classiceditor"
import Essentials from "@ckeditor/ckeditor5-essentials/src/essentials"
import FontColor from "@ckeditor/ckeditor5-font/src/fontcolor"
import Heading from "@ckeditor/ckeditor5-heading/src/heading"
import Title from "@ckeditor/ckeditor5-heading/src/title"
import HorizontalLine from "@ckeditor/ckeditor5-horizontal-line/src/horizontalline"
import Image from "@ckeditor/ckeditor5-image/src/image"
import ImageCaption from "@ckeditor/ckeditor5-image/src/imagecaption"
import ImageResize from "@ckeditor/ckeditor5-image/src/imageresize"
import ImageStyle from "@ckeditor/ckeditor5-image/src/imagestyle"
import ImageToolbar from "@ckeditor/ckeditor5-image/src/imagetoolbar"
import ImageUpload from "@ckeditor/ckeditor5-image/src/imageupload"
import Indent from "@ckeditor/ckeditor5-indent/src/indent"
import IndentBlock from "@ckeditor/ckeditor5-indent/src/indentblock"
import Autolink from "@ckeditor/ckeditor5-link/src/autolink"
import Link from "@ckeditor/ckeditor5-link/src/link"
import LinkImage from "@ckeditor/ckeditor5-link/src/linkimage"
import List from "@ckeditor/ckeditor5-list/src/list"
import TodoList from "@ckeditor/ckeditor5-list/src/todolist"
import MediaEmbed from "@ckeditor/ckeditor5-media-embed/src/mediaembed"
import MediaEmbedToolbar from "@ckeditor/ckeditor5-media-embed/src/mediaembedtoolbar"
import Paragraph from "@ckeditor/ckeditor5-paragraph/src/paragraph"
import PasteFromOffice from "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice"
import RemoveFormat from "@ckeditor/ckeditor5-remove-format/src/removeformat"
import Table from "@ckeditor/ckeditor5-table/src/table"
import TableCellProperties from "@ckeditor/ckeditor5-table/src/tablecellproperties"
import TableProperties from "@ckeditor/ckeditor5-table/src/tableproperties"
import TableToolbar from "@ckeditor/ckeditor5-table/src/tabletoolbar"
import TextTransformation from "@ckeditor/ckeditor5-typing/src/texttransformation"
import SimpleUploadAdapter from "@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter"
import Watchdog from "@ckeditor/ckeditor5-watchdog/src/editorwatchdog"
import WordCount from "@ckeditor/ckeditor5-word-count/src/wordcount"

const NS = "hui"

class MinimalEditor extends ClassicEditor {}

class Editor extends ClassicEditor {}

// Plugins to include in the build.
Editor.builtinPlugins = [
  Alignment,
  Autoformat,
  Autolink,
  Autosave,
  BlockQuote,
  Bold,
  Code,
  CodeBlock,
  Essentials,
  FontColor,
  Heading,
  HorizontalLine,
  Image,
  ImageCaption,
  ImageResize,
  ImageStyle,
  ImageToolbar,
  ImageUpload,
  Indent,
  IndentBlock,
  Italic,
  Link,
  LinkImage,
  List,
  MediaEmbed,
  MediaEmbedToolbar,
  Paragraph,
  PasteFromOffice,
  RemoveFormat,
  SimpleUploadAdapter,
  Strikethrough,
  Subscript,
  Superscript,
  Table,
  TableCellProperties,
  TableProperties,
  TableToolbar,
  TextTransformation,
  Title,
  TodoList,
  Underline,
  WordCount,
]

Editor.defaultConfig = {
  toolbar: {
    items: [
      "undo",
      "redo",
      "|",
      "heading",
      "|",
      "bold",
      "italic",
      "underline",
      "strikethrough",
      "fontColor",
      "|",
      "link",
      "imageUpload",
      "mediaEmbed",
      "|",
      "blockQuote",
      "alignment",
      "indent",
      "outdent",
      "|",
      // "insertTable",
      "numberedList",
      "bulletedList",
      // "todoList",
      "|",
      "code",
      "codeBlock",
      "horizontalLine",
      "superscript",
      "subscript",
      "removeFormat",
    ],
  },
  language: "en",
  alignment: {
    options: ["left", "right", "center"],
  },
  image: {
    // Configure the available styles.
    styles: ["alignLeft", "alignCenter", "alignRight"],

    // Configure the available image resize options.
    resizeOptions: [
      {
        name: "resizeImage:original",
        label: "Original",
        value: null,
      },
      {
        name: "resizeImage:50",
        label: "50%",
        value: "50",
      },
      {
        name: "resizeImage:75",
        label: "75%",
        value: "75",
      },
    ],

    // You need to configure the image toolbar, too, so it shows the new style
    // buttons as well as the resize buttons.
    toolbar: [
      "imageStyle:alignLeft",
      "imageStyle:alignCenter",
      "imageStyle:alignRight",
      "|",
      "resizeImage",
      "|",
      "imageTextAlternative",
    ],
  },
  heading: {
    options: [
      { model: "small", title: "Small", view: { name: "small", classes: `${NS}-text-small` } },
      { model: "paragraph", title: "Normal", view: { name: "p", classes: `${NS}-text-normal` } },
      {
        model: "headingMedium",
        title: "Heading",
        view: { name: "h5", classes: `${NS}-heading-medium` },
      },
      {
        model: "headingNormal",
        title: "Subheading",
        view: { name: "h6", classes: `${NS}-heading-normal` },
      },
    ],
  },
  // table: {
  //   contentToolbar: [
  //     "tableColumn",
  //     "tableRow",
  //     "mergeTableCells",
  //     "tableCellProperties",
  //     "tableProperties",
  //   ],
  // },
}

MinimalEditor.builtinPlugins = [
  Alignment,
  Autoformat,
  Bold,
  Essentials,
  Italic,
  Link,
  List,
  Paragraph,
  PasteFromOffice,
  TextTransformation,
  Underline,
]

MinimalEditor.defaultConfig = {
  toolbar: {
    items: [
      "undo",
      "redo",
      "|",
      "bold",
      "italic",
      "underline",
      "|",
      "bulletedList",
      "numberedList",
      "alignment",
      "|",
      "link",
    ],
  },
  language: "en",
}

export default { Editor, MinimalEditor, EditorWatchdog: Watchdog }
