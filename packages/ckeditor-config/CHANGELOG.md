# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.4.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.3.1...@weareasterisk/hyphen-ui-ckeditor5-config@1.4.0) (2021-05-17)


### Features

* add additional options for images in ckeditor and update deps ([5961f54](https://gitlab.com/weareasterisk/design-system/commit/5961f54e578531afee0cbeef386c7cd3bdb0a38c))





## [1.3.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.3.0...@weareasterisk/hyphen-ui-ckeditor5-config@1.3.1) (2021-01-08)


### Bug Fixes

* rich text editor list indentation styles ([f853e7f](https://gitlab.com/weareasterisk/design-system/commit/f853e7ff8cf6aceb6254b3fe484abb4eee9a7983))





# [1.3.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.2.3...@weareasterisk/hyphen-ui-ckeditor5-config@1.3.0) (2020-12-21)


### Features

* add minimal editor config ([336a425](https://gitlab.com/weareasterisk/design-system/commit/336a425ce9fbdd48b8a6effc9a4e2c45b5c8f708))





## [1.2.3](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.2.2...@weareasterisk/hyphen-ui-ckeditor5-config@1.2.3) (2020-11-16)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-ckeditor5-config





## [1.2.2](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.2.1...@weareasterisk/hyphen-ui-ckeditor5-config@1.2.2) (2020-11-16)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-ckeditor5-config





## [1.2.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.2.0...@weareasterisk/hyphen-ui-ckeditor5-config@1.2.1) (2020-11-16)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-ckeditor5-config





# [1.2.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-ckeditor5-config@1.1.0...@weareasterisk/hyphen-ui-ckeditor5-config@1.2.0) (2020-10-10)


### Features

* add editor styles ([666d2d3](https://gitlab.com/weareasterisk/design-system/commit/666d2d3809ce42169457a422cf4ad4c993035279))





# 1.1.0 (2020-09-21)


### Features

* create base ckeditor config and watchdog with required features and custom toolbar ([6a1259d](https://gitlab.com/weareasterisk/design-system/commit/6a1259d19b4b65bbbdaace1a4c5c3cfae43c89ab))
