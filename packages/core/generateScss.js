const fs = require("fs")
const path = require("path")
const stripComments = require("strip-css-comments")

const SOURCE = path.resolve(path.join(__dirname, "src"))
const DEST = path.resolve(path.join(__dirname, "lib"))

const source = process.argv.slice(2)

let scss = source.reduce((str, current) => {
  return str + fs.readFileSync(path.resolve(path.join(SOURCE, current))).toString()
}, "")

scss = stripComments(scss)
  .replace(/\\!default/g, "")
  .replace(/(@import|\/\/).*[\n|\r\n]+/g, "")
  .replace(/\n{3,}/g, "\n\n")

if (!fs.existsSync(DEST)) fs.mkdirSync(DEST)

if (!fs.existsSync(path.resolve(path.join(DEST, "scss"))))
  fs.mkdirSync(path.resolve(path.join(DEST, "scss")))

fs.writeFileSync(path.join(path.resolve(DEST, "scss", "variables.scss")), scss)
