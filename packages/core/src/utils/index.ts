export const isProd = (): boolean => {
  return process.env.NODE_ENV === "production"
}

export default function setRef<T>(
  ref: React.MutableRefObject<T | null> | ((instance: T | null) => void) | null | undefined,
  value: T | null
): void {
  if (typeof ref === "function") {
    ref(value)
  } else if (ref) {
    ref.current = value
  }
}

// export { default as createSvgIcon } from "./createSvgIcon"
