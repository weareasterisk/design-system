declare module "foundations"
declare module "*.svg"
declare module "*.mdx" {
  // NOTE: Type checking disabled here to allow the ever-ambiguous MDXComponent to mount
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let MDXComponent: (props: any) => JSX.Element
  export default MDXComponent
}
