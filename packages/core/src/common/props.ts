import React from "react"
export const DISPLAYNAME_PREFIX = "Hyphen-UI"

/**
 * Element? Maybe? idk
 */
export type MaybeElement = JSX.Element | false | null | undefined

export type MaybeInstance = React.ComponentClass<unknown> | false | null | undefined

/**
 * Base interface of props that are common across all components
 */
export interface CommonProps {
  /**
   * Class Names
   */
  className?: string
}

/**
 * All valid HTML props for `<input>` element
 */
export type HTMLInputProps = React.InputHTMLAttributes<HTMLInputElement>

export type HTMLTextAreaProps = React.DetailedHTMLProps<
  React.TextareaHTMLAttributes<HTMLTextAreaElement>,
  HTMLTextAreaElement
>

export type HTMLDivProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
>

/**
 * Generate the display name for a component with the proper prefix
 *
 * @param componentName - The name of the component
 * @returns The generated display name that's prefixed properly
 */
export const generateDisplayName = (componentName: string): string =>
  `${DISPLAYNAME_PREFIX}.${componentName}`

/**
 * Default props for all Control components (radio, checkbox, etc.)
 */
export interface ControlProps extends CommonProps, HTMLInputProps {
  /**
   * Whether or not the control is checked
   */
  checked?: boolean

  /**
   * JSX Lavel for the control
   */
  children?: React.ReactNode

  /**
   * Whether or not the control is checked by default
   */
  defaultChecked?: boolean

  /**
   * Whether or not the control is disabled
   */
  disabled?: boolean

  /**
   * Ref handler that receives HTML `<input>` element backing this component.
   */
  inputRef?: (ref: HTMLInputElement | null) => unknown

  /**
   * Text label for the control
   */
  label?: string

  /**
   * JSX Element label for the control
   */
  labelElement?: React.ReactNode

  /**
   * Whether this control uses compact styles
   */
  compact?: boolean

  /**
   * Event handler invoked when the input value changes
   */
  onChange?: React.FormEventHandler<HTMLInputElement>

  /**
   * Whether or not the control component is rendered as a button
   *
   * This state is not implemented yet
   */
  button?: boolean
}

export interface ActionProps extends CommonProps {
  /** Whether or not the component is disabled */
  disabled?: boolean

  /** Supplementary icon component */
  icon?: React.ReactType

  // /** Action Text */
  // text?: React.ReactNode
}
