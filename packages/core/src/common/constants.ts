/**
 * The namespace that prefixes classes
 */
export const NS =
  process.env.ASTERISK_DESIGN_NAMESPACE || process.env.REACT_APP_ASTERISK_DESIGN_NAMESPACE || "hui"
