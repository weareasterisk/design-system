import * as utils from "./class_utils"
import { NS } from "./constants"

// Class names are referenced as javascript variables to ensure the namespace is evaluated cleanly

/**
 * Only top level classes need to be defined here
 */

/**
 * Modifiers
 *
 * These class names are generally not implemented globally
 * and must be implemented to a specific component or scope.
 *
 * @see "../scoped" for scoped definitions of utilities
 *
 * @sealed
 */

/** */
export const ACTIVE = `${NS}-active`
export const DISABLED = `${NS}-disabled`
export const COMPACT = `${NS}-compact`
export const MINI = `${NS}-mini`
export const ERROR = `${NS}-error`
export const ERROR_MESSAGE = `${ERROR}-message`
export const LABEL = `${NS}-label`
export const INLINE = `${NS}-inline`
export const WRAPPER = `${NS}-inline`
export const READONLY = `${NS}-readonly`
export const REQUIRED = `${NS}-required`

/**
 * Utility classes
 */

/** */
export const ELEVATION_1 = utils.elevationClass(1)
export const ELEVATION_2 = utils.elevationClass(2)
export const ELEVATION_3 = utils.elevationClass(3)
export const ELEVATION_4 = utils.elevationClass(4)

/**
 * Typography classes
 */

/** */
export const HEADING = `${NS}-heading`
export const HEADING_HERO = `${HEADING}-hero`
export const HEADING_XXLARGE = `${HEADING}-xxlarge`
export const HEADING_XLARGE = `${HEADING}-xlarge`
export const HEADING_LARGE = `${HEADING}-large`
export const HEADING_MEDIUM = `${HEADING}-medium`
export const HEADING_NORMAL = `${HEADING}-normal`

export const TEXT = `${NS}-text`

export const TEXT_NORMAL = `${TEXT}-normal`
export const TEXT_SMALL = `${TEXT}-small`
export const TEXT_TINY = `${TEXT}-tiny`

export const TEXT_BOLD = `${TEXT}-bold`
export const TEXT_UNDERLINED = `${TEXT}-underlined`

/**
 * Component Classes
 */

// Control Classes
export const CONTROL = `${NS}-control`
export const CONTROL_BUTTON = `${CONTROL}-button`
export const CONTROL_CHECKED = `${CONTROL}-checked`
export const CONTROL_INDICATOR = `${CONTROL}-indicator`
export const CONTROL_INDICATOR_CHILD = `${CONTROL_INDICATOR}-child`

// Checkbox Classes
export const CHECKBOX = `${NS}-checkbox`

// Toggle Classes
export const TOGGLE = `${NS}-toggle`
export const TOGGLE_SWITCH = `${TOGGLE}-switch`
export const TOGGLE_SLIDER = `${TOGGLE}-slider`
export const TOGGLE_STATUS_TEXT = `${TOGGLE}-status-text`
export const TOGGLE_CHECKBOX = `${TOGGLE}-checkbox`

// Number Input Classes
export const NUMBER_INPUT = `${NS}-number-input`
export const NUMBER_INPUT_COMPACT = `${NUMBER_INPUT}-compact`
export const NUMBER_INPUT_DISABLED = `${NUMBER_INPUT}-disabled`
export const NUMBER_INPUT_ERROR = `${NUMBER_INPUT}-error`

// Button Classes
export const BUTTON = `${NS}-button`
export const BUTTON_DANGER = `${BUTTON}-danger`
export const BUTTON_FILL = `${BUTTON}-fill`
export const BUTTON_OUTLINE = `${BUTTON}-outline`
export const BUTTON_PLAIN = `${BUTTON}-plain`
export const BUTTON_PLAIN_DANGER = `${BUTTON}-plain-danger`
export const BUTTON_PRIMARY = `${BUTTON}-primary`
export const BUTTON_SECONDARY = `${BUTTON}-secondary`
// export const BUTTON_LOADING = `${BUTTON}-loading`

// IconButton classes
export const ICON_BUTTON = `${NS}-icon-button`
export const ICON_BUTTON_XSMALL = `${ICON_BUTTON}-xsmall`
export const ICON_BUTTON_SMALL = `${ICON_BUTTON}-small`
export const ICON_BUTTON_LARGE = `${ICON_BUTTON}-large`
export const ICON_BUTTON_XLARGE = `${ICON_BUTTON}-xlarge`
export const ICON_BUTTON_XXLARGE = `${ICON_BUTTON}-xxlarge`

// Dropzone Classes
export const DROPZONE = `${NS}-dropzone`
export const DROPZONE_DRAG = `${DROPZONE}-drag`
export const DROPZONE_HAS_FILE = `${DROPZONE}-has-file`
export const DROPZONE_ICON = `${DROPZONE}-icon`
export const DROPZONE_ICON_DRAG = `${DROPZONE_ICON}-drag`
export const DROPZONE_ICON_SIMPLE = `${DROPZONE_ICON}-simple`
export const DROPZONE_INPUT = `${DROPZONE}-input`
export const DROPZONE_INSTRUCTIONS = `${DROPZONE}-instructions`
export const DROPZONE_INSTRUCTIONS_HAS_FILE = `${DROPZONE_INSTRUCTIONS}-has-file`
export const DROPZONE_INSTRUCTIONS_SIMPLE = `${DROPZONE_INSTRUCTIONS}-simple`

// SmallDropzone Classes
export const SMALL_DROPZONE = `${NS}-small-dropzone`
export const SMALL_DROPZONE_DRAG = `${SMALL_DROPZONE}-drag`
export const SMALL_DROPZONE_ICON = `${SMALL_DROPZONE}-icon`
export const SMALL_DROPZONE_ICON_DRAG = `${SMALL_DROPZONE_ICON}-drag`
export const SMALL_DROPZONE_ICON_ERROR = `${SMALL_DROPZONE_ICON}-error`
export const SMALL_DROPZONE_INPUT = `${SMALL_DROPZONE}-input`
export const SMALL_DROPZONE_INSTRUCTIONS = `${SMALL_DROPZONE}-instructions`

// FileItem
export const FILE_ITEM = `${NS}-file-item`
export const FILE_ITEM_ERROR = `${FILE_ITEM}-error`
export const FILE_ITEM_INFO = `${FILE_ITEM}-info`
export const FILE_ITEM_LOGO = `${FILE_ITEM}-logo`
export const FILE_ITEM_LOGO_ERROR = `${FILE_ITEM_LOGO}-error`
export const FILE_ITEM_FILENAME = `${FILE_ITEM}-filename`
export const FILE_ITEM_ERROR_MESSAGE = `${FILE_ITEM}-error-message`

// Input Classes
export const INPUT = `${NS}-input`

// Error Label Classes
export const ERROR_LABEL = `${LABEL}-error`

// Text Field Classes
export const TEXT_FIELD = `${NS}-text-field`
export const TEXT_FIELD_ACTION = `${TEXT_FIELD}-action`
export const TEXT_FIELD_AGGREGATOR = `${TEXT_FIELD}-aggregator`
export const TEXT_FIELD_BUTTON = `${TEXT_FIELD}-button`
export const TEXT_FIELD_COMPACT = `${TEXT_FIELD}-compact`
export const TEXT_FIELD_DISABLED = `${TEXT_FIELD}-disabled`
export const TEXT_FIELD_ERROR = `${TEXT_FIELD}-error`
export const TEXT_FIELD_ERROR_MESSAGE = `${TEXT_FIELD_ERROR}-message`
export const TEXT_FIELD_HELPER = `${TEXT_FIELD}-helper`
export const TEXT_FIELD_ICON_LEFT = `${TEXT_FIELD}-icon-left`
export const TEXT_FIELD_ICON_RIGHT = `${TEXT_FIELD}-icon-right`
export const TEXT_FIELD_LABEL = `${TEXT_FIELD}-label`
export const TEXT_FIELD_PREFIX = `${TEXT_FIELD}-prefix`
export const TEXT_FIELD_SUFFIX = `${TEXT_FIELD}-suffix`
export const TEXT_FIELD_WRAPPER = `${TEXT_FIELD}-wrapper`

// Text Area Classes
export const TEXT_AREA = `${NS}-text-area`
export const TEXT_AREA_CHARACTER_COUNTER = `${TEXT_AREA}-character-counter`
export const TEXT_AREA_HELPER = `${TEXT_AREA}-helper`
export const TEXT_AREA_INPUT = `${TEXT_AREA}-input`

// Card Classes
export const CARD = `${NS}-card`

// Icon Classes
export const ICON = `${NS}-icon`
export const SVG_ICON = `${NS}-svg-icon`

// Divider Classes
export const DIVIDER = `${NS}-divider`
export const DIVIDER_LIGHTER = `${DIVIDER}-lighter`
export const DIVIDER_LIGHT = `${DIVIDER}-light`
export const DIVIDER_DARK = `${DIVIDER}-dark`
export const DIVIDER_DARKER = `${DIVIDER}-darker`

// ListBox Classes
export const LIST_BOX = `${NS}-list-box`
export const LIST_BOX_EXPANDED = `${LIST_BOX}-expanded`
export const LIST_BOX_INPUT = `${LIST_BOX}-input`

export const LIST_BOX_LABEL = `${LIST_BOX}-label`

export const LIST_BOX_ERROR = `${LIST_BOX}-error`

export const LIST_BOX_MENU = `${LIST_BOX}-menu`

export const LIST_BOX_MENU_ICON = `${LIST_BOX_MENU}-icon`
export const LIST_BOX_MENU_ICON_OPEN = `${LIST_BOX_MENU_ICON}-open`

export const LIST_BOX_MENU_ITEM = `${LIST_BOX_MENU}-item`
export const LIST_BOX_MENU_ITEM_ACTIVE = `${LIST_BOX_MENU_ITEM}-active`
export const LIST_BOX_MENU_ITEM_HIGHLIGHTED = `${LIST_BOX_MENU_ITEM}-highlighted`
export const LIST_BOX_MENU_ITEM_OPTION = `${LIST_BOX_MENU_ITEM}-option`

export const LIST_BOX_FIELD = `${LIST_BOX}-field`
export const LIST_BOX_FIELD_PLACEHOLDER = `${LIST_BOX_FIELD}-placeholder`

//List Classes
export const LIST = `${NS}-list`
export const LIST_DRAGGABLE = `${LIST}-draggable`
export const LIST_ITEM = `${LIST}-item`
export const LIST_ITEM_LINE = `${LIST}-item-line`
export const LIST_ITEM_ICON = `${LIST}-item-icon`
export const LIST_ITEM_CONTENT = `${LIST}-item-content`

// Modal Classes
export const MODAL = `${NS}-modal`
export const MODAL_OVERLAY = `${MODAL}-overlay`
export const MODAL_AFTER_OPEN = `${MODAL}-after-open`
export const MODAL_BEFORE_CLOSE = `${MODAL}-before-close`
export const MODAL_HEADER = `${MODAL}-header`
export const MODAL_HEADER_CLOSE_BUTTON = `${MODAL}-header-close-button`
export const MODAL_MAIN = `${MODAL}-main`
export const MODAL_FOOTER = `${MODAL}-footer`

// A11y Classes
export const VISUALLY_HIDDEN = `${NS}-visually-hidden`

// Spinner Classes
export const SPINNER = `${NS}-spinner`
export const SPINNER_LARGE = `${SPINNER}-large`
export const SPINNER_SMALL = `${SPINNER}-small`
export const SPINNER_TINY = `${SPINNER}-tiny`
export const SPINNER_EASING_ROLLERCOASTER = `${SPINNER}-easing-rollercoaster`

// Time Picker Clases
export const TIMEPICKER = `${NS}-timepicker`
export const TIMEPICKER_DROPDOWNS = `${TIMEPICKER}-dropdowns`

// Link Classes
export const LINK = `${NS}-link`
export const LINK_DANGER = `${LINK}-danger`
export const LINK_ICON = `${LINK}-icon`
export const LINK_CONTENT = `${LINK}-content`
