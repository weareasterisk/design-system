import { NS } from "./constants"

/**
 * @internal
 *
 * @param elevation - The elevation of the box shadow
 * @returns The classname that corresponds to the provided elevation
 */
export function elevationClass(elevation: 1 | 2 | 3 | 4 | null): string {
  if (elevation == null) {
    return `${NS}-elevation-1`
  }
  return `${NS}-elevation-${elevation}`
}
