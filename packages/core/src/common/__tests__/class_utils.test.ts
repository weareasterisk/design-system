import { elevationClass } from "../class_utils"
import { NS } from "../constants"

describe("src/common/class_utils - elevationClass", () => {
  it("Returns the default class when prop is null", () => {
    const output = elevationClass(null)
    expect(output).toEqual(`${NS}-elevation-1`)
  })

  it("Returns the correctly numbered class when prop is passed", () => {
    const output = elevationClass(2)
    expect(output).toEqual(`${NS}-elevation-2`)
  })
})
