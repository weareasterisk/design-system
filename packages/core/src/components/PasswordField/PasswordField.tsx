import { EyeOffOutline, EyeOutline } from "@weareasterisk/hyphen-ui-icons"
import React, { useState } from "react"

import { Classes, generateDisplayName } from "../../common"
import { TextField, TextFieldProps } from "../TextField"

export type PasswordFieldProps = TextFieldProps

export const PasswordField = React.memo(
  React.forwardRef<HTMLInputElement, PasswordFieldProps>((props, ref) => {
    const [passwordVisible, setPasswordVisible] = useState(false)

    const PasswordButton = React.memo(() => (
      <button
        onClick={() => setPasswordVisible((passwordVisible) => !passwordVisible)}
        className={Classes.TEXT_FIELD_BUTTON}
      >
        {passwordVisible ? <EyeOffOutline /> : <EyeOutline />}
      </button>
    ))
    PasswordButton.displayName = generateDisplayName("PasswordButton")

    return (
      <React.Fragment>
        <TextField
          ref={ref}
          type={passwordVisible ? "text" : "password"}
          iconRight={PasswordButton}
          {...props}
        />
      </React.Fragment>
    )
  })
)

PasswordField.displayName = generateDisplayName("PasswordField")
