import { storiesOf } from "@storybook/react"
import { DateTimeInterval, TimePicker } from "@weareasterisk/hyphen-ui-core"
import React from "react"

export default {
  title: "TimePicker",
  component: TimePicker,
}

function onChange(e: DateTimeInterval): void {
  console.log(e)
}

storiesOf("TimePicker", module).add("onchange", () => <TimePicker onChange={onChange} />)
