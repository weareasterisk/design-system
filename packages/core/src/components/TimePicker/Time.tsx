import { Time, TimeZone } from "./TimePicker"
// Python for generating the below
// for specifier in ["AM", "PM"]:
//    for hour in range(1,13):
//      for minute in ["00", "15", "30", "45"]:
//        print('{time:"'+str(hour)+":"+minute+ specifier+ '", offset: '+ str((hour)*3600+ int(minute) * 60 + (3600*12 if specifier=="PM"else 0) + (-43200 if hour is 12 else 0))  +'},')
export const timeArray: Time[] = [
  { time: "1:00AM", offset: 3600 },
  { time: "1:15AM", offset: 4500 },
  { time: "1:30AM", offset: 5400 },
  { time: "1:45AM", offset: 6300 },
  { time: "2:00AM", offset: 7200 },
  { time: "2:15AM", offset: 8100 },
  { time: "2:30AM", offset: 9000 },
  { time: "2:45AM", offset: 9900 },
  { time: "3:00AM", offset: 10800 },
  { time: "3:15AM", offset: 11700 },
  { time: "3:30AM", offset: 12600 },
  { time: "3:45AM", offset: 13500 },
  { time: "4:00AM", offset: 14400 },
  { time: "4:15AM", offset: 15300 },
  { time: "4:30AM", offset: 16200 },
  { time: "4:45AM", offset: 17100 },
  { time: "5:00AM", offset: 18000 },
  { time: "5:15AM", offset: 18900 },
  { time: "5:30AM", offset: 19800 },
  { time: "5:45AM", offset: 20700 },
  { time: "6:00AM", offset: 21600 },
  { time: "6:15AM", offset: 22500 },
  { time: "6:30AM", offset: 23400 },
  { time: "6:45AM", offset: 24300 },
  { time: "7:00AM", offset: 25200 },
  { time: "7:15AM", offset: 26100 },
  { time: "7:30AM", offset: 27000 },
  { time: "7:45AM", offset: 27900 },
  { time: "8:00AM", offset: 28800 },
  { time: "8:15AM", offset: 29700 },
  { time: "8:30AM", offset: 30600 },
  { time: "8:45AM", offset: 31500 },
  { time: "9:00AM", offset: 32400 },
  { time: "9:15AM", offset: 33300 },
  { time: "9:30AM", offset: 34200 },
  { time: "9:45AM", offset: 35100 },
  { time: "10:00AM", offset: 36000 },
  { time: "10:15AM", offset: 36900 },
  { time: "10:30AM", offset: 37800 },
  { time: "10:45AM", offset: 38700 },
  { time: "11:00AM", offset: 39600 },
  { time: "11:15AM", offset: 40500 },
  { time: "11:30AM", offset: 41400 },
  { time: "11:45AM", offset: 42300 },
  { time: "12:00AM", offset: 0 },
  { time: "12:15AM", offset: 900 },
  { time: "12:30AM", offset: 1800 },
  { time: "12:45AM", offset: 2700 },
  { time: "1:00PM", offset: 46800 },
  { time: "1:15PM", offset: 47700 },
  { time: "1:30PM", offset: 48600 },
  { time: "1:45PM", offset: 49500 },
  { time: "2:00PM", offset: 50400 },
  { time: "2:15PM", offset: 51300 },
  { time: "2:30PM", offset: 52200 },
  { time: "2:45PM", offset: 53100 },
  { time: "3:00PM", offset: 54000 },
  { time: "3:15PM", offset: 54900 },
  { time: "3:30PM", offset: 55800 },
  { time: "3:45PM", offset: 56700 },
  { time: "4:00PM", offset: 57600 },
  { time: "4:15PM", offset: 58500 },
  { time: "4:30PM", offset: 59400 },
  { time: "4:45PM", offset: 60300 },
  { time: "5:00PM", offset: 61200 },
  { time: "5:15PM", offset: 62100 },
  { time: "5:30PM", offset: 63000 },
  { time: "5:45PM", offset: 63900 },
  { time: "6:00PM", offset: 64800 },
  { time: "6:15PM", offset: 65700 },
  { time: "6:30PM", offset: 66600 },
  { time: "6:45PM", offset: 67500 },
  { time: "7:00PM", offset: 68400 },
  { time: "7:15PM", offset: 69300 },
  { time: "7:30PM", offset: 70200 },
  { time: "7:45PM", offset: 71100 },
  { time: "8:00PM", offset: 72000 },
  { time: "8:15PM", offset: 72900 },
  { time: "8:30PM", offset: 73800 },
  { time: "8:45PM", offset: 74700 },
  { time: "9:00PM", offset: 75600 },
  { time: "9:15PM", offset: 76500 },
  { time: "9:30PM", offset: 77400 },
  { time: "9:45PM", offset: 78300 },
  { time: "10:00PM", offset: 79200 },
  { time: "10:15PM", offset: 80100 },
  { time: "10:30PM", offset: 81000 },
  { time: "10:45PM", offset: 81900 },
  { time: "11:00PM", offset: 82800 },
  { time: "11:15PM", offset: 83700 },
  { time: "11:30PM", offset: 84600 },
  { time: "11:45PM", offset: 85500 },
  { time: "12:00PM", offset: 43200 },
  { time: "12:15PM", offset: 44100 },
  { time: "12:30PM", offset: 45000 },
  { time: "12:45PM", offset: 45900 },
]

// https://gist.github.com/themeteorchef/780ce5d72e17ba887591
export const timeZoneArray: TimeZone[] = [
  {
    offset: "GMT-12:00",
    name: "Etc/GMT-12",
  },
  {
    offset: "GMT-11:00",
    name: "Etc/GMT-11",
  },
  {
    offset: "GMT-11:00",
    name: "Pacific/Midway",
  },
  {
    offset: "GMT-10:00",
    name: "America/Adak",
  },
  {
    offset: "GMT-09:00",
    name: "America/Anchorage",
  },
  {
    offset: "GMT-09:00",
    name: "Pacific/Gambier",
  },
  {
    offset: "GMT-08:00",
    name: "America/Dawson_Creek",
  },
  {
    offset: "GMT-08:00",
    name: "America/Ensenada",
  },
  {
    offset: "GMT-08:00",
    name: "America/Los_Angeles",
  },
  {
    offset: "GMT-07:00",
    name: "America/Chihuahua",
  },
  {
    offset: "GMT-07:00",
    name: "America/Denver",
  },
  {
    offset: "GMT-06:00",
    name: "America/Belize",
  },
  {
    offset: "GMT-06:00",
    name: "America/Cancun",
  },
  {
    offset: "GMT-06:00",
    name: "America/Chicago",
  },
  {
    offset: "GMT-06:00",
    name: "Chile/EasterIsland",
  },
  {
    offset: "GMT-05:00",
    name: "America/Bogota",
  },
  {
    offset: "GMT-05:00",
    name: "America/Havana",
  },
  {
    offset: "GMT-05:00",
    name: "America/New_York",
  },
  {
    offset: "GMT-04:30",
    name: "America/Caracas",
  },
  {
    offset: "GMT-04:00",
    name: "America/Campo_Grande",
  },
  {
    offset: "GMT-04:00",
    name: "America/Glace_Bay",
  },
  {
    offset: "GMT-04:00",
    name: "America/Goose_Bay",
  },
  {
    offset: "GMT-04:00",
    name: "America/Santiago",
  },
  {
    offset: "GMT-04:00",
    name: "America/La_Paz",
  },
  {
    offset: "GMT-03:00",
    name: "America/Argentina/Buenos_Aires",
  },
  {
    offset: "GMT-03:00",
    name: "America/Montevideo",
  },
  {
    offset: "GMT-03:00",
    name: "America/Araguaina",
  },
  {
    offset: "GMT-03:00",
    name: "America/Godthab",
  },
  {
    offset: "GMT-03:00",
    name: "America/Miquelon",
  },
  {
    offset: "GMT-03:00",
    name: "America/Sao_Paulo",
  },
  {
    offset: "GMT-03:30",
    name: "America/St_Johns",
  },
  {
    offset: "GMT-02:00",
    name: "America/Noronha",
  },
  {
    offset: "GMT-01:00",
    name: "Atlantic/Cape_Verde",
  },
  {
    offset: "GMT",
    name: "Europe/Belfast",
  },
  {
    offset: "GMT",
    name: "Africa/Abidjan",
  },
  {
    offset: "GMT",
    name: "Europe/Dublin",
  },
  {
    offset: "GMT",
    name: "Europe/Lisbon",
  },
  {
    offset: "GMT",
    name: "Europe/London",
  },
  {
    offset: "UTC",
    name: "UTC",
  },
  {
    offset: "GMT+01:00",
    name: "Africa/Algiers",
  },
  {
    offset: "GMT+01:00",
    name: "Africa/Windhoek",
  },
  {
    offset: "GMT+01:00",
    name: "Atlantic/Azores",
  },
  {
    offset: "GMT+01:00",
    name: "Atlantic/Stanley",
  },
  {
    offset: "GMT+01:00",
    name: "Europe/Amsterdam",
  },
  {
    offset: "GMT+01:00",
    name: "Europe/Belgrade",
  },
  {
    offset: "GMT+01:00",
    name: "Europe/Brussels",
  },
  {
    offset: "GMT+02:00",
    name: "Africa/Cairo",
  },
  {
    offset: "GMT+02:00",
    name: "Africa/Blantyre",
  },
  {
    offset: "GMT+02:00",
    name: "Asia/Beirut",
  },
  {
    offset: "GMT+02:00",
    name: "Asia/Damascus",
  },
  {
    offset: "GMT+02:00",
    name: "Asia/Gaza",
  },
  {
    offset: "GMT+02:00",
    name: "Asia/Jerusalem",
  },
  {
    offset: "GMT+03:00",
    name: "Africa/Addis_Ababa",
  },
  {
    offset: "GMT+03:00",
    name: "Asia/Riyadh89",
  },
  {
    offset: "GMT+03:00",
    name: "Europe/Minsk",
  },
  {
    offset: "GMT+03:30",
    name: "Asia/Tehran",
  },
  {
    offset: "GMT+04:00",
    name: "Asia/Dubai",
  },
  {
    offset: "GMT+04:00",
    name: "Asia/Yerevan",
  },
  {
    offset: "GMT+04:00",
    name: "Europe/Moscow",
  },
  {
    offset: "GMT+04:30",
    name: "Asia/Kabul",
  },
  {
    offset: "GMT+05:00",
    name: "Asia/Tashkent",
  },
  {
    offset: "GMT+05:30",
    name: "Asia/Kolkata",
  },
  {
    offset: "GMT+05:45",
    name: "Asia/Katmandu",
  },
  {
    offset: "GMT+06:00",
    name: "Asia/Dhaka",
  },
  {
    offset: "GMT+06:00",
    name: "Asia/Yekaterinburg",
  },
  {
    offset: "GMT+06:30",
    name: "Asia/Rangoon",
  },
  {
    offset: "GMT+07:00",
    name: "Asia/Bangkok",
  },
  {
    offset: "GMT+07:00",
    name: "Asia/Novosibirsk",
  },
  {
    offset: "GMT+08:00",
    name: "Etc/GMT+8",
  },
  {
    offset: "GMT+08:00",
    name: "Asia/Hong_Kong",
  },
  {
    offset: "GMT+08:00",
    name: "Asia/Krasnoyarsk",
  },
  {
    offset: "GMT+08:00",
    name: "Australia/Perth",
  },
  {
    offset: "GMT+08:45",
    name: "Australia/Eucla",
  },
  {
    offset: "GMT+09:00",
    name: "Asia/Irkutsk",
  },
  {
    offset: "GMT+09:00",
    name: "Asia/Seoul",
  },
  {
    offset: "GMT+09:00",
    name: "Asia/Tokyo",
  },
  {
    offset: "GMT+09:30",
    name: "Australia/Adelaide",
  },
  {
    offset: "GMT+09:30",
    name: "Australia/Darwin",
  },
  {
    offset: "GMT+09:30",
    name: "Pacific/Marquesas",
  },
  {
    offset: "GMT+10:00",
    name: "Etc/GMT+10",
  },
  {
    offset: "GMT+10:00",
    name: "Australia/Brisbane",
  },
  {
    offset: "GMT+10:00",
    name: "Australia/Hobart",
  },
  {
    offset: "GMT+10:00",
    name: "Asia/Yakutsk",
  },
  {
    offset: "GMT+10:30",
    name: "Australia/Lord_Howe",
  },
  {
    offset: "GMT+11:00",
    name: "Asia/Vladivostok",
  },
  {
    offset: "GMT+11:30",
    name: "Pacific/Norfolk",
  },
  {
    offset: "GMT+12:00",
    name: "Etc/GMT+12",
  },
  {
    offset: "GMT+12:00",
    name: "Asia/Anadyr",
  },
  {
    offset: "GMT+12:00",
    name: "Asia/Magadan",
  },
  {
    offset: "GMT+12:00",
    name: "Pacific/Auckland",
  },
  {
    offset: "GMT+12:45",
    name: "Pacific/Chatham",
  },
  {
    offset: "GMT+13:00",
    name: "Pacific/Tongatapu",
  },
  {
    offset: "GMT+14:00",
    name: "Pacific/Kiritimati",
  },
]
