import classNames from "classnames"
import React from "react"

import { Classes } from "../../common"
import { Dropdown } from "../Dropdown"
import { timeArray, timeZoneArray } from "./Time"

export interface TimePickerProps {
  onChange(interval: DateTimeInterval): void
}

export interface DateTimeInterval {
  startDate: Date
  endDate: Date
  timezone: string
}

export interface TimeZone {
  offset: string
  name: string
}

export interface Time {
  time: string
  offset: number
}

// TODO: Replace the any's with actual object when DS-8 is fixed
const timeToString = (item: any) => {
  return item?.time.slice() || ""
}

const timeZoneToString = (item: any) => {
  return item?.name.slice() + " " + item?.offset.slice() || ""
}

export function TimePicker(props: TimePickerProps): React.ReactElement {
  const timepickerClassnames = classNames(Classes.TIMEPICKER)
  const dropdownClassnames = classNames(Classes.TIMEPICKER_DROPDOWNS)

  const initialState = {
    startSeconds: 0,
    endSeconds: 0,
    timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
  }

  const [state, setState] = React.useState(initialState)

  function buildDate(startTime: number, endTime: number, timezoneName: string): DateTimeInterval {
    const date = new Date(1970, 1, 1)

    const startDateTime = new Date(date.getTime())
    startDateTime.setSeconds(date.getSeconds() + startTime)
    const endDateTime = new Date(date.getTime())
    endDateTime.setSeconds(date.getSeconds() + endTime)

    return { startDate: startDateTime, endDate: endDateTime, timezone: timezoneName }
  }

  // TODO: Replace the any's with actual datatype when DS-8 is fixed
  function onStartChange(e: any) {
    props.onChange(buildDate(e.selectedItem.offset, state.endSeconds, state.timezone))
    const modifiedState = { ...state }
    modifiedState.startSeconds = e.selectedItem.offset
    setState(modifiedState)
  }
  function onEndChange(e: any) {
    props.onChange(buildDate(state.startSeconds, e.selectedItem.offset, state.timezone))
    const modifiedState = { ...state }
    modifiedState.endSeconds = e.selectedItem.offset
    setState(modifiedState)
  }
  function onTimezoneChange(e: any) {
    props.onChange(buildDate(state.startSeconds, state.endSeconds, e.selectedItem.name))
    const modifiedState = { ...state }
    modifiedState.timezone = e.selectedItem.name
    setState(modifiedState)
  }

  return (
    <div className={timepickerClassnames}>
      <Dropdown
        className={dropdownClassnames}
        items={timeArray}
        inline
        placeholder={"Start Time"}
        label={"Start Time"}
        onSelectChange={onStartChange}
        itemToString={timeToString}
      />
      <Dropdown
        className={dropdownClassnames}
        items={timeArray}
        inline
        placeholder={"End Time"}
        label={"End Time"}
        onSelectChange={onEndChange}
        itemToString={timeToString}
      />
      <Dropdown
        className={dropdownClassnames}
        items={timeZoneArray}
        inline
        placeholder={"Choose an option"}
        label={"Time Zone"}
        onSelectChange={onTimezoneChange}
        itemToString={timeZoneToString}
      />
    </div>
  )
}
