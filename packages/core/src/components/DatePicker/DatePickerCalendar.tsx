import { ArrowBackOutline, ArrowForwardOutline } from "@weareasterisk/hyphen-ui-icons"
import React from "react"
import ReactCalendar, { CalendarProps as ReactCalendarProps } from "react-calendar"

import { generateDisplayName } from "../../common"
import { monthFormatFn } from "./utils"

const defaultFormatter = (_locale: string, date: Date) => monthFormatFn(date)

export const DatePickerCalendar = React.memo<ReactCalendarProps>(
  ({
    locale = "en-US",
    minDetail = "year",
    nextLabel = <ArrowForwardOutline />,
    prevLabel = <ArrowBackOutline />,
    next2Label = null,
    prev2Label = null,
    formatMonth = defaultFormatter,
    ...props
  }) => {
    return (
      <ReactCalendar
        locale={locale}
        minDetail={minDetail}
        nextLabel={nextLabel}
        next2Label={next2Label}
        prevLabel={prevLabel}
        prev2Label={prev2Label}
        formatMonth={formatMonth}
        {...props}
      />
    )
  }
)

DatePickerCalendar.displayName = generateDisplayName("DatePicker/Calendar")
