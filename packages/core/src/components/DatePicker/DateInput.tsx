import { CalendarOutline } from "@weareasterisk/hyphen-ui-icons"
import { format, isSameMonth, isValid, isWithinInterval, parse, parseISO } from "date-fns"
import React, { useCallback, useEffect, useMemo, useRef, useState } from "react"
import { CalendarProps, Detail } from "react-calendar"
import { Popover } from "react-tiny-popover"

import { TextFieldProps } from ".."
import { safeInvoke } from "../_utils/function"
import { generateDisplayName, KeyCodes } from "../../common"
import { TextField } from "../TextField"
import { DatePickerCalendar } from "./DatePickerCalendar"
import { INVALID_DATE_MESSAGE, OUT_OF_RANGE_MESSAGE } from "./phrases"
import {
  getDefaultMaxDate,
  getDefaultMinDate,
  getFormattedDateString as getFormattedDateStringFn,
  hasTimeChanged,
} from "./utils"

export interface DateFormatProps {
  invalidDateMessage?: string
  locale?: Locale
  outOfRangeMessage?: string
  placeholder?: string
  formatDate?: (date: Date, locale?: Locale) => string
  parseDate?: (str: string, locale?: Locale) => Date | false | null
}

export interface DateInputCommonProps {
  calendarProps?: CalendarProps | null
  initialMonth?: Date
  locale?: Locale
  maxDate?: Date
  minDate?: Date
  modifiers?: { [name: string]: (date: Date) => boolean }
}

export interface DateInputProps
  extends Omit<TextFieldProps, "value" | "defaultValue">,
    DateFormatProps,
    DateInputCommonProps {
  canClearSelection?: boolean
  clearButtonText?: string
  closeOnSelection?: boolean
  onDateChange?: (selectedDate: Date, isUserChange: boolean) => void
  onDateError?: (errorDate: Date) => void
  value?: Date | null
  defaultValue?: Date
}

const REACT_DATE_PICKER_TILE = "react-calendar__tile"
const REACT_DATE_PICKER_NAVIGATION = "react-calendar__navigation"

export const DateInput = React.memo(
  React.forwardRef<HTMLInputElement, DateInputProps>(
    (
      {
        closeOnSelection = true,
        calendarProps: calendarPropsProp = {},
        disabled = false,
        invalidDateMessage = INVALID_DATE_MESSAGE,
        outOfRangeMessage = OUT_OF_RANGE_MESSAGE,
        value: valueProp,
        defaultValue,
        minDate = getDefaultMinDate(),
        maxDate = getDefaultMaxDate(),
        parseDate: parseDateProp = (str: string, locale?: Locale) =>
          parse(str, "MM/dd/yyyy", new Date(), { locale }),
        formatDate: formatDateProp = (date: Date, locale?: Locale) =>
          format(date, "MM/dd/yyyy", { locale }),
        locale,
        onFocus,
        onClick,
        onChange,
        onDateChange,
        onDateError,
        onBlur,
        placeholder = "MM/DD/YYYY",
        ...props
      },
      ref
    ) => {
      const [isInputFocused, setIsInputFocused] = useState(false)
      const [isOpen, setIsOpen] = useState(false)
      const [value, setValue] = useState<Date | null | undefined>(
        valueProp !== undefined
          ? typeof valueProp === "string"
            ? parseISO(valueProp)
            : valueProp
          : typeof defaultValue === "string"
          ? parseISO(defaultValue)
          : defaultValue
      )

      useEffect(() => {
        setValue(typeof valueProp === "string" ? parseISO(valueProp) : valueProp)
      }, [valueProp])

      const [valueString, setValueString] = useState<string | null>(null)

      const [lastTabbableElement, setLastTabbableElement] = useState<HTMLElement | null>(null)

      const [view, setView] = useState<Detail>("month")

      const inputRef = ref
      const popoverRef = useRef<HTMLDivElement>(null)

      const isInRange = useCallback(
        (val: Date | null) => {
          if (val == null) return false
          return isWithinInterval(val, { start: minDate, end: maxDate })
        },
        [maxDate, minDate]
      )

      const _handleClosePopover = () => {
        setIsOpen(false)
      }

      const parseDate = (dateString: string | null): Date | null => {
        if (
          dateString === outOfRangeMessage ||
          dateString === invalidDateMessage ||
          dateString == null
        ) {
          return null
        }
        const newDate = parseDateProp(dateString, locale)
        return newDate === false ? ((undefined as unknown) as Date) : newDate
      }

      const formatDate = useCallback(
        (date: Date): string => {
          if (!isValid(date) || !isInRange(date)) {
            return ""
          }
          return formatDateProp(date, locale)
        },
        [formatDateProp, isInRange, locale]
      )

      const _getFormattedDateString = useCallback(
        (value: false | Date | null | undefined) =>
          getFormattedDateStringFn(value, {
            invalidDateMessage,
            outOfRangeMessage,
            formatDate,
            minDate,
            maxDate,
            locale,
          }),
        [formatDate, invalidDateMessage, locale, maxDate, minDate, outOfRangeMessage]
      )

      const dateString = useMemo(() => {
        return isInputFocused ? valueString : _getFormattedDateString(value)
      }, [_getFormattedDateString, isInputFocused, value, valueString])

      const dateValue = useMemo(() => (isValid(value) ? value : null), [value])

      const calendarProps: CalendarProps = {
        ...calendarPropsProp,
      }
      const _handleDateChange = (
        newValue: Date | null,
        isUserChange: boolean,
        didSubmitWithEnter = false
      ) => {
        const prevDate = value

        // TODO: Re-think this strategy in case using multiple selections in an input
        const parsedValue = newValue

        const isOpen =
          !isUserChange ||
          !closeOnSelection ||
          (prevDate != null &&
            (!(
              (prevDate == null) !== (parsedValue == null) ||
              isSameMonth(prevDate as Date, parsedValue as Date)
            ) ||
              hasTimeChanged(prevDate as Date, parsedValue as Date))) ||
          view !== "month"

        const isInputFocused = didSubmitWithEnter ? true : false
        if (valueProp === undefined) {
          const valueString = _getFormattedDateString(parsedValue)
          setIsInputFocused(isInputFocused)
          setIsOpen(isOpen)
          setValue(parsedValue)
          setValueString(valueString)
        } else {
          setIsInputFocused(isInputFocused)
          setIsOpen(isOpen)
        }
        onDateChange?.(newValue!, isUserChange)
      }

      const _handleInputFocus = (e: React.FocusEvent<HTMLInputElement>) => {
        const valueString = value == null || !value ? "" : formatDate(value)
        setIsInputFocused(true)
        setIsOpen(true)
        setValueString(valueString)
        safeInvoke(onFocus, e)
      }

      const _handleInputClick = (e: React.SyntheticEvent<HTMLDivElement>) => {
        e.stopPropagation()
        safeInvoke(onClick, e)
      }

      const _handleInputChange = (e: React.SyntheticEvent<HTMLInputElement>) => {
        const valueString = (e.target as HTMLInputElement).value
        const value = parseDate(valueString)

        if (isValid(value) && isInRange(value)) {
          if (valueProp === undefined) {
            setValue(value)
            setValueString(valueString)
          } else {
            setValueString(valueString)
          }
          // Assertion is safe due to conditions in wrapping `if` statement
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          onDateChange?.(value!, true)
        } else {
          if (valueString.length === 0) {
            onDateChange?.((null as unknown) as Date, true)
          }
          setValueString(valueString)
        }
        safeInvoke(onChange, e)
      }

      const _handleInputBlur = (e: React.FocusEvent<HTMLInputElement>) => {
        const stateVal = valueString
        const date = parseDate(stateVal)
        if (
          valueString &&
          valueString?.length > 0 &&
          valueString !== _getFormattedDateString(value)
        ) {
          if (valueProp === undefined) {
            setIsInputFocused(false)
            setValue(date)
            setValueString(null)
          } else {
            setIsInputFocused(false)
          }

          if (date && isNaN(date?.valueOf())) {
            onDateError?.((undefined as unknown) as Date)
          } else if (!isInRange(date)) {
            // Assertion is safe due to above conditional
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            onDateError?.(date!)
          } else {
            // Assertion is safe due to above conditional
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            onDateChange?.(date!, true)
          }
        } else {
          if (valueString && valueString.length === 0) {
            setIsInputFocused(false)
            setValue(null)
            setValueString(null)
          } else {
            setIsInputFocused(false)
          }
        }
        _registerPopoverBlurHandler()
        safeInvoke(onBlur, e)
      }

      const _handleInputKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.which === KeyCodes.ENTER) {
          const nextDate = parseDate(valueString)
          _handleDateChange(nextDate, true, true)
        } else if (e.which === KeyCodes.TAB) {
          setIsOpen(false)
        } else if (e.which === KeyCodes.ESCAPE) {
          setIsOpen(false)
          e.currentTarget?.blur()
        }
      }

      const _getLastTabbableElement = () => {
        const tabbableElements = popoverRef.current?.querySelectorAll(
          "button, [tabindex]:not([tabindex='-1'])"
        )
        const numOfElements = tabbableElements?.length ?? 0
        const lastTabbablElement =
          numOfElements > 0 && tabbableElements ? tabbableElements[numOfElements - 1] : null

        return lastTabbablElement as HTMLElement | null
      }

      const _handlePopoverBlur = (e: FocusEvent) => {
        let relatedTarget = e.relatedTarget as HTMLElement
        if (relatedTarget == null) {
          // Support for IE11
          relatedTarget = document.activeElement as HTMLElement
        }
        const eventTarget = e.target as HTMLElement

        if (
          relatedTarget == null ||
          (popoverRef.current != null && !popoverRef.current.contains(relatedTarget))
        ) {
          // Exclude the following blur operations that makes "body" the activeElement
          // and would close the Popover unexpectedly
          // - On disabled change months buttons
          // - DayPicker day elements, their "blur" will be managed at its own onKeyDown
          const isChangeMonthEvt = eventTarget.classList.contains(REACT_DATE_PICKER_NAVIGATION)
          const isChangeMonthButtonDisabled =
            isChangeMonthEvt && (eventTarget as HTMLButtonElement).disabled
          const isDayPickerDayEvt = eventTarget.classList.contains(REACT_DATE_PICKER_TILE)
          if (!isChangeMonthButtonDisabled && !isDayPickerDayEvt) {
            _handleClosePopover()
          }
        } else if (relatedTarget != null) {
          _unregisterPopoverBlurHandler()
          setLastTabbableElement(_getLastTabbableElement())
          lastTabbableElement?.addEventListener("blur", _handlePopoverBlur)
        }
      }

      const _unregisterPopoverBlurHandler = () => {
        lastTabbableElement?.removeEventListener("blur", _handlePopoverBlur)
      }

      const _registerPopoverBlurHandler = () => {
        if (popoverRef.current != null) {
          _unregisterPopoverBlurHandler()
          setLastTabbableElement(_getLastTabbableElement())
          lastTabbableElement?.addEventListener("blur", _handlePopoverBlur)
        }
      }

      const isErrorState = useMemo(() => {
        return value != null && (!isValid(value) || !isInRange(value))
      }, [isInRange, value])

      const wrappedPopoverContent = (
        <div ref={popoverRef}>
          <DatePickerCalendar
            {...calendarProps}
            value={dateValue}
            onChange={(date) => _handleDateChange(date as Date, true)}
            returnValue="start"
            view={view}
            onViewChange={({ view }) => setView(view)}
          />
        </div>
      )

      const CloneCalendar = React.forwardRef(() =>
        React.createElement(CalendarOutline, {
          onClick: () => setIsOpen((e) => !e),
        })
      )

      CloneCalendar.displayName = "ClonedCalendarInternal"

      return (
        <React.Fragment>
          <Popover
            isOpen={isOpen}
            content={wrappedPopoverContent}
            onClickOutside={() => _handleClosePopover()}
            reposition={false}
            padding={8}
            positions={["bottom"]}
            align="start"
            ref={inputRef}
          >
            <TextField
              autoComplete="off"
              type="text"
              placeholder={placeholder}
              {...props}
              disabled={disabled}
              onBlur={_handleInputBlur}
              onChange={_handleInputChange}
              onClick={_handleInputClick}
              onFocus={_handleInputFocus}
              onKeyDown={_handleInputKeyDown}
              error={isErrorState}
              value={dateString || ""}
              iconLeft={CloneCalendar}
            />
          </Popover>
        </React.Fragment>
      )
    }
  )
)

DateInput.displayName = generateDisplayName("DatePicker/DateInput")
