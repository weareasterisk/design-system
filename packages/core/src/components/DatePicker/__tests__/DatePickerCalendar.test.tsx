import { render } from "@testing-library/react"
import React from "react"

import { DatePickerCalendar } from "../DatePickerCalendar"

const testDate = new Date(2020, 0, 1)

describe("components/DatePicker/DatePickerCalendar - DatePickerCalendar", () => {
  it("Should match the snapshot", () => {
    const { container } = render(
      <DatePickerCalendar defaultActiveStartDate={testDate} defaultValue={testDate} />
    )
    expect(container).toMatchSnapshot()
  })
})
