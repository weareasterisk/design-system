import { monthFormatFn } from "../utils"

const testDate = new Date(2020, 0, 1)

describe("components/DatePicker/utils - monthFormatFn", () => {
  it("Returns the formatted date correctly", () => {
    const output = monthFormatFn(testDate)
    expect(output).toBe("Jan")
  })
})
