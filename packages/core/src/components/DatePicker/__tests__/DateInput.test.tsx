import { render } from "@testing-library/react"
import React from "react"

import { DateInput } from "../DateInput"

const testDate = new Date(2020, 0, 1)

describe("components/DatePicker/DateInput - DateInput", () => {
  it("Should match the snapshot", () => {
    const { container } = render(<DateInput />)
    expect(container).toMatchSnapshot()
  })

  it("handles null inputs without crashing", () => {
    expect(() => render(<DateInput value={null} />))
  })

  it("handles input strings without crashing", () => {
    expect(() => render(<DateInput value={"7/8/1998" as any} />))
  })
})
