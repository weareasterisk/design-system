import { DateInput, DatePickerCalendar } from "@weareasterisk/hyphen-ui-core"
import React, { useState } from "react"

import mdx from "./DatePicker.mdx"

export default {
  title: "core/DatePicker",
  parameters: {
    docs: {
      page: mdx,
    },
  },
}

export const UncontrolledDatePickerCalendar = (): JSX.Element => {
  return (
    <React.Fragment>
      <DatePickerCalendar />
    </React.Fragment>
  )
}

export const ControlledDateInput = (): JSX.Element => {
  const [value, setValue] = useState(new Date())

  const _handleChange = (newVal: any) => {
    setValue(newVal)
    console.log(newVal)
  }
  return (
    <React.Fragment>
      <DateInput value={value} onDateChange={(date) => _handleChange(date)} />
    </React.Fragment>
  )
}
