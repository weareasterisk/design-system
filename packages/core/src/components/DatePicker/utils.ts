import { endOfDay, format, isValid, isWithinInterval, startOfDay } from "date-fns"

import { DateFormatProps, DateInputCommonProps } from "./DateInput"
import { INVALID_DATE_MESSAGE, OUT_OF_RANGE_MESSAGE } from "./phrases"

export const monthFormatFn = (date: Date): string => format(date, "LLL")

export const Months = Object.freeze({
  JANUARY: 0,
  FEBRUARY: 1,
  MARCH: 2,
  APRIL: 3,
  MAY: 4,
  JUNE: 5,
  JULY: 6,
  AUGUST: 7,
  SEPTEMBER: 8,
  OCTOBER: 9,
  NOVEMBER: 10,
  DECEMBER: 11,
})

export const getFormattedDateString = (
  date: Date | false | null | undefined,
  props: Pick<
    DateFormatProps & DateInputCommonProps,
    "outOfRangeMessage" | "invalidDateMessage" | "minDate" | "maxDate" | "formatDate" | "locale"
  >,
  ignoreRange = false
): string => {
  if (date == null || date == undefined) {
    return ""
  } else if (!isValid(date)) {
    return props.invalidDateMessage || INVALID_DATE_MESSAGE
  } else if (
    ignoreRange ||
    (date &&
      props.minDate &&
      props.maxDate &&
      isWithinInterval(date, { start: startOfDay(props.minDate), end: endOfDay(props.maxDate) }))
  ) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return props.formatDate!(date as Date, props.locale)
  } else {
    return props.outOfRangeMessage || OUT_OF_RANGE_MESSAGE
  }
}

export const getDefaultMaxDate = (): Date => {
  const date = new Date()
  date.setFullYear(date.getFullYear() + 500)
  date.setMonth(Months.DECEMBER, 31)
  return date
}

export const getDefaultMinDate = (): Date => {
  const date = new Date()
  date.setFullYear(date.getFullYear() - 500)
  date.setMonth(Months.JANUARY, 1)
  return date
}

export const hasTimeChanged = (prevDate: Date, nextDate: Date): boolean => {
  return (
    (prevDate == null) !== (nextDate == null) ||
    nextDate.getHours() !== prevDate.getHours() ||
    nextDate.getMinutes() !== prevDate.getMinutes() ||
    nextDate.getSeconds() !== prevDate.getSeconds() ||
    nextDate.getMilliseconds() !== prevDate.getMilliseconds()
  )
}
