import React from "react"

/**
 * Disables scroll on the body when included in a component.
 * Commonly used in modals.
 *
 * @param hide - Whether or not to hide overflow. Defaults to true
 */
export const useLockBodyScroll = (hide = true): void => {
  React.useLayoutEffect(() => {
    const originalStyle = window.getComputedStyle(document.body).overflow

    document.body.style.overflow = hide === true ? "hidden" : originalStyle

    return () => {
      document.body.style.overflow = originalStyle
    }
  }, [hide])
}
