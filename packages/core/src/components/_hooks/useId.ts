import { useRef } from "react"

let id = 0

/**
 * Provides a unique identifier with an optional prefix. Useful for dynamically
 * creating an `id` field for elements that require them.
 *
 * @param prefix - An optional prefix to attach to the returned ID
 * @returns - A unique ID to be used in elements
 */
export const useId = (prefix?: string): string => {
  const ref = useRef(++id)

  return prefix ? `${prefix}-${ref.current}` : ref.current.toString()
}
