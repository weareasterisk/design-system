import { useEffect, useMemo, useRef } from "react"

interface UseTimeout {
  start: (overrideTimeout?: number | undefined) => void
  stop: VoidFunction
  restart: VoidFunction
}

export const useTimeout = (callback: CallableFunction, timeout = 0): UseTimeout => {
  // const [timeRemaining, setTimeRemaining] = useState<number>(timeout)

  const timeoutId = useRef<number>()
  const handler = useMemo(() => {
    return {
      start(overrideTimeout?: number) {
        handler.stop()
        timeoutId.current = window.setTimeout(
          callback,
          overrideTimeout === undefined ? timeout : overrideTimeout
        )
      },

      stop() {
        if (timeoutId.current) {
          window.clearTimeout(timeoutId.current)
        }
      },

      restart() {
        handler.stop()
        handler.start()
      },
    }
  }, [callback, timeout])

  useEffect(() => {
    return () => {
      handler.stop()
    }
  }, [handler])

  return handler
}
