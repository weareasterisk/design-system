import { useEffect } from "react"
import { MutableRefObject } from "react"

/**
 * Call a function when a click event fires outside of the ref element
 *
 * @param ref - The ref of the element
 * @param callback - The function that's called when a click occurs outside of the element
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const useOutsideClick = (ref: MutableRefObject<any>, callback: () => void): void => {
  const handleClick = (e: MouseEvent) => {
    if (ref.current && !ref.current.contains(e.target)) {
      callback()
    }
  }

  useEffect(() => {
    document.addEventListener("click", handleClick)

    return () => {
      document.removeEventListener("click", handleClick)
    }
  })
}
