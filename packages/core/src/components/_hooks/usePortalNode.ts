import { useEffect, useState } from "react"

const activePortals = new Map()

/**
 * Create a node and append it to the document's body
 *
 * @param id - Specify the ID to give the created node
 * @returns - The created node
 */
const createAndAppendNode = (id?: string): HTMLDivElement => {
  const node = document.createElement("div")
  if (id) {
    node.id = id
  }
  document.body.appendChild(node)
  return node
}

/**
 * Find a portal node or create one if it doesn't exist
 *
 * @param id - Specify the ID of the node to search for
 * @returns - The created or discovered node
 */
const findOrCreatePortalNode = (id?: string): HTMLElement => {
  if (!id) {
    return createAndAppendNode()
  }

  const existingNode = document.getElementById(id)
  if (existingNode) {
    return existingNode
  }

  return createAndAppendNode(id)
}

/**
 * Find a portal root or create one if it doesn't exist
 *
 * @param id - Specify the ID of a root to search for
 * @returns - Return created or discovered root
 */
const findOrCreatePortalRoot = (id: string | undefined): [HTMLElement, () => void] => {
  const node = findOrCreatePortalNode(id)
  if (!id) {
    const cleanup = () => document.body.removeChild(node)
    return [node, cleanup]
  }

  const currentPortalCount = activePortals.get(id) || 0
  activePortals.set(id, currentPortalCount + 1)

  const cleanup = () => {
    const currentPortalCount = activePortals.get(id)
    if (currentPortalCount === 1) {
      document.body.removeChild(node)
      activePortals.delete(id)
    } else {
      activePortals.set(id, currentPortalCount - 1)
    }
  }

  return [node, cleanup]
}

/**
 * Create or reference an existing root in the template for portals
 *
 * @param id - Specify the ID of a root to reference
 * @returns - Returns the node to use as a root if found/created
 */
export const usePortalNode = (id?: string): HTMLElement | null => {
  const [portalNode, setPortalNode] = useState<HTMLElement | null>(null)

  useEffect(() => {
    const [node, cleanup] = findOrCreatePortalRoot(id)
    setPortalNode(node)

    return () => {
      cleanup()
      setPortalNode(null)
    }
  }, [id])

  return portalNode
}
