import { Dispatch, SetStateAction, useEffect, useLayoutEffect, useState } from "react"

/**
 * Use state with callback nested in an effect
 *
 * @param initialState - The initial state
 * @param callback - The callback to execute each time the state is updated
 * @returns Regular setState vars
 */
export function useStateWithCallback<S>(
  initialState: S | (() => S),
  callback: (arg: S) => void
): [S, Dispatch<SetStateAction<S>>] {
  const [state, setState] = useState(initialState)

  useEffect(() => callback(state), [state, callback])

  return [state, setState]
}

/**
 * Use state with callback nested in a layout effect
 *
 * @param initialState - The initial state
 * @param callback - The callback to execute each time the state is updated
 * @returns Regular setState vars
 */
export function useStateWithCallbackLayout<S>(
  initialState: S | (() => S),
  callback: (arg: S) => void
): [S, Dispatch<SetStateAction<S>>] {
  const [state, setState] = useState(initialState)

  useLayoutEffect(() => callback(state), [state, callback])

  return [state, setState]
}
