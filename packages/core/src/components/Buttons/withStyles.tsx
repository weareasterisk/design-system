/**
 * This component should not be exported to the public API
 */

import classNames from "classnames"
import React from "react"

import { ActionProps } from "../../common"
import * as Classes from "../../common/classes"

export type ButtonVariants =
  | "primary"
  | "secondary"
  | "danger"
  | "outline"
  | "plain"
  | "plain-danger"

export type ButtonSizes = "normal" | "compact" | "mini"

/**
 * Abstract interface extended by implemented components
 */
export interface AbstractButtonProps extends ActionProps {
  size?: ButtonSizes
  /** Use the `normal` size */
  normal?: boolean
  /** Use the `compact` size */
  compact?: boolean
  /** Use the `mini` size */
  mini?: boolean

  variant?: ButtonVariants
  primary?: boolean
  secondary?: boolean
  danger?: boolean
  outline?: boolean
  plain?: boolean
  plainDanger?: boolean

  /** Disable the button */
  disabled?: boolean

  loading?: boolean // TODO: implement
  fill?: boolean
}

export const withStyles = <P extends AbstractButtonProps>(
  WrappedComponent: React.ComponentType<P>
): React.FC<P> => {
  const ComponentWithProps = (props: P) => {
    const {
      className,
      disabled,
      normal,
      compact,
      mini,
      size = "normal",
      variant = "primary",
      primary,
      secondary,
      danger,
      outline,
      plain,
      plainDanger,
      fill,
      ...rest
      // loading, // TODO: Loading state
    } = props

    /**
     * `Variant` / `Size` derivation ensures that only one state/style
     * is applied at a time.
     */

    const derivedVariant: ButtonVariants | null = primary
      ? "primary"
      : secondary
      ? "secondary"
      : danger
      ? "danger"
      : outline
      ? "outline"
      : plain
      ? "plain"
      : plainDanger
      ? "plain-danger"
      : null

    // `Normal` state is used by default
    const derivedSize: ButtonSizes | null = normal
      ? "normal"
      : compact
      ? "compact"
      : mini
      ? "mini"
      : null

    const trueSize = derivedSize ?? size

    const trueVariant = derivedVariant ?? variant

    const classes = classNames(
      Classes.BUTTON,
      {
        [Classes.DISABLED]: disabled,
        [Classes.COMPACT]: trueSize === "compact",
        [Classes.MINI]: trueSize === "mini",
        [Classes.BUTTON_PRIMARY]: trueVariant === "primary",
        [Classes.BUTTON_SECONDARY]: trueVariant === "secondary",
        [Classes.BUTTON_DANGER]: trueVariant === "danger",
        [Classes.BUTTON_OUTLINE]: trueVariant === "outline",
        [Classes.BUTTON_PLAIN]: trueVariant === "plain",
        [Classes.BUTTON_PLAIN_DANGER]: trueVariant === "plain-danger",
        [Classes.BUTTON_FILL]: fill,
        // [Classes.BUTTON_]: || === ,
      },
      className
    )
    return <WrappedComponent className={classes} disabled={disabled} {...(rest as P)} />
  }
  return ComponentWithProps
}
