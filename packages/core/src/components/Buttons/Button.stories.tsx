import { boolean, select, text, withKnobs } from "@storybook/addon-knobs"
import React from "react"

import { AnchorButton, Button } from "./Button"
import mdx from "./Buttons.mdx"
import { ButtonSizes, ButtonVariants } from "./withStyles"

export default {
  title: "core/Buttons",
  decorators: [withKnobs],
  parameters: {
    component: Button,
    subcomponents: { AnchorButton },
    docs: {
      page: mdx,
    },
  },
}

// const variants = {
//   "Primary button (primary)": "primary",
//   "Secondary button (secondary)": "secondary",
//   "Danger button (danger)": "danger",
//   "Outline button (outline)": "outline",
//   "Plain button (plain)": "plain",
//   "Plain Danger button (plain danger)": "plain-danger",
// }

const variants = {
  primary: "primary",
  secondary: "secondary",
  danger: "danger",
  outline: "outline",
  plain: "plain",
  "plain-danger": "plain-danger",
}

export type SelectTypeKnobValue = string | number | null | undefined

const sizes = {
  normal: "normal",
  compact: "compact",
  mini: "mini",
}

const props = {
  regular: () => {
    return {
      className: "some-class",
      children: text("Children (children)", "Playground Button"),
      size: select<ButtonSizes>(
        "Button size (size) [Broken down props take priority]",
        sizes,
        "normal"
      ),
      normal: boolean("Normal size (normal)", false),
      compact: boolean("Compact size (compact)", false),
      mini: boolean("Mini size (mini)", false),
      variant: select<ButtonVariants>(
        "Button variant (variant) [Broken down props take priority]",
        variants,
        "primary"
      ),
      primary: boolean("Primary variant (primary)", false),
      secondary: boolean("Secondary variant (secondary)", false),
      danger: boolean("Danger variant (danger)", false),
      outline: boolean("Outline variant (outline)", false),
      plain: boolean("Plain variant (plain)", false),
      plainDanger: boolean("Plain Danger variant (plainDanger)", false),
      fill: boolean("Fill button to container (fill)", false),
      disabled: boolean("Disable button (disable)", false),
    }
  },
}

export const _Default = (): JSX.Element => {
  return <Button>Button</Button>
}
_Default.story = {
  name: "Button",
}

export const Primary = (): JSX.Element => {
  return <Button primary>Primary</Button>
}

export const Secondary = (): JSX.Element => {
  return <Button secondary>Secondary</Button>
}

export const Danger = (): JSX.Element => {
  return <Button danger>Danger</Button>
}

export const Outline = (): JSX.Element => {
  return <Button outline>Outline</Button>
}

export const Plain = (): JSX.Element => {
  return <Button plain>Plain</Button>
}

export const PlainDanger = (): JSX.Element => {
  return <Button plainDanger>Plain Danger</Button>
}

export const Fill = (): JSX.Element => {
  return <Button fill>Fill</Button>
}

export const Disabled = (): JSX.Element => {
  return <Button disabled>Disabled</Button>
}

export const Anchor = (): JSX.Element => {
  return (
    <AnchorButton primary href="https://google.com/" target="_blank" rel="noopener noreferrer">
      Anchor Button
    </AnchorButton>
  )
}

export const AnchorDisabled = (): JSX.Element => {
  return (
    <AnchorButton
      disabled
      href="https://weareasterisk.com/"
      target="_blank"
      rel="noopener noreferrer"
    >
      Anchor Disabled
    </AnchorButton>
  )
}

export const Playground = (): JSX.Element => {
  const regularProps = props.regular()

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexWrap: "wrap",
      }}
    >
      <Button {...regularProps} />
    </div>
  )
}
