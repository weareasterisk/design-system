import React from "react"

import { generateDisplayName } from "../../common"
import { AbstractButtonProps, withStyles } from "./withStyles"

export interface ButtonProps
  extends AbstractButtonProps,
    React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {}

const ButtonUnstyled = React.memo(
  React.forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => {
    return <button {...props} ref={ref} />
  })
)
ButtonUnstyled.displayName = generateDisplayName("Button")
export const Button = withStyles(ButtonUnstyled)

export interface AnchorButtonProps
  extends AbstractButtonProps,
    React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement> {}

const AnchorButtonUnstyled = React.memo(
  React.forwardRef<HTMLAnchorElement, AnchorButtonProps>(({ children, ...props }, ref) => {
    return (
      <a {...props} ref={ref}>
        {/* `Children` must be provided explicitly to meet lint requirements for Anchors */}
        {children}
      </a>
    )
  })
)
AnchorButtonUnstyled.displayName = generateDisplayName("AnchorButton")
export const AnchorButton = withStyles(AnchorButtonUnstyled)
