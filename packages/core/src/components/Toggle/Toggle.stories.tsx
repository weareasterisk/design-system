import { storiesOf } from "@storybook/react"
import { Toggle } from "@weareasterisk/hyphen-ui-core"
import React from "react"

export default {
  title: "Toggle",
  component: Toggle,
}
function onChange(e: React.ChangeEvent<HTMLInputElement>): void {
  alert("changed " + e)
}
storiesOf("Toggle", module)
  .add("plain", () => <Toggle />)
  .add("disabled", () => <Toggle disabled={true} />)
  .add("compact", () => <Toggle compact={true} />)
  .add("compact disabled", () => <Toggle disabled={true} compact={true} />)
  .add("plain initial true", () => <Toggle defaultChecked={true} />)
  .add("disabled initial false", () => <Toggle disabled={true} defaultChecked={false} />)
  .add("compact initial true", () => <Toggle compact={true} defaultChecked={true} />)
  .add("compact disabled initial false", () => (
    <Toggle disabled={true} compact={true} defaultChecked={false} />
  ))
  .add("Compact With Parent", () => (
    <Toggle compact={true} defaultChecked={false} onChange={onChange} />
  ))
  .add("compact disabled initial true", () => (
    <Toggle disabled={true} compact={true} defaultChecked={true} />
  ))
  .add("compact disabled initial true label", () => (
    <Toggle
      disabled={true}
      compact={true}
      defaultChecked={true}
      label={"I'm a little label short and stout"}
    />
  ))
  .add("disabled initial true label", () => (
    <Toggle disabled={true} defaultChecked={true} label={"SCREAMING"} />
  ))
