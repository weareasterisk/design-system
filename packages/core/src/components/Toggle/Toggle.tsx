import classNames from "classnames"
import React from "react"

import { Classes } from "../../common"
import { CommonProps, generateDisplayName } from "../../common/props"

export interface ToggleProps extends CommonProps, React.InputHTMLAttributes<HTMLInputElement> {
  disabled?: boolean
  compact?: boolean
  label?: string
  labelClassName?: string
}

export function Toggle({
  disabled,
  compact,
  onChange,
  checked,
  className,
  label,
  labelClassName,
  ...props
}: ToggleProps): React.ReactElement {
  const toggleParentClassname = classNames(Classes.TOGGLE, className)
  const toggleClassnames = classNames(
    Classes.TOGGLE_SWITCH,
    {
      [Classes.DISABLED]: disabled,
      [Classes.COMPACT]: compact,
    },
    labelClassName
  )

  return (
    <div className={toggleParentClassname}>
      <label className={toggleClassnames}>
        <input
          type="checkbox"
          className={Classes.TOGGLE_CHECKBOX}
          onChange={onChange}
          checked={checked}
          disabled={disabled}
          {...props}
        />
        <span className={Classes.TOGGLE_SLIDER}></span>
      </label>
      {label && <span className={Classes.TOGGLE_STATUS_TEXT}>{label}</span>}
    </div>
  )
}

Toggle.displayName = generateDisplayName("Toggle")
