import { render } from "@testing-library/react"
import React from "react"

import { TextField } from "./TextField"

describe("components/TextField/TextField - TextField", () => {
  it("matches the snapshot", () => {
    expect(render(<TextField />).container).toMatchSnapshot()
  })
})
