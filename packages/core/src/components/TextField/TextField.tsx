import { AlertCircle } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React from "react"

import { safeInvoke } from "../_utils/function"
import { Classes, CommonProps, generateDisplayName, KeyCodes } from "../../common"
import { Input, InputProps } from "../Input"

/**
 * Props for the TextField Component
 */
export interface TextFieldProps extends CommonProps, InputProps {
  /**
   * Action text
   */
  action?: string

  /**
   * Action function executed when the action is clicked.
   * The `action` prop must be present for this to be relevant
   * */
  onActionClick?(): void

  /**
   * Error message
   */
  errorMessage?: string
  /**
   * Helper text
   */
  helper?: string

  /**
   * Icon rendered on the left of the input
   */
  iconLeft?: React.ReactType

  /**
   * Icon rendered on the right of the input
   */
  iconRight?: React.ReactType

  /**
   * Input label
   */
  label?: string

  /**
   * Whether or not the label is inline
   */
  labelInline?: boolean

  /**
   * Placeholder Text
   */
  placeholder?: string

  /**
   * Prefix
   */
  prefix?: string

  /**
   * Suffix
   */
  suffix?: string

  /**
   * Ref for the input element
   */
  inputRef?: React.Ref<HTMLInputElement>
}

function resolve(anchorEl: unknown) {
  return typeof anchorEl === "function" ? anchorEl() : anchorEl
}

export const TextField = React.memo(
  React.forwardRef<HTMLDivElement, TextFieldProps>(
    (
      {
        className,
        compact,
        action,
        onActionClick,
        helper,
        placeholder,
        disabled,
        error,
        errorMessage,
        label,
        labelInline,
        iconLeft,
        iconRight,
        prefix,
        suffix,
        required,
        readOnly,
        type = "text",
        inputRef,
        ...props
      },
      ref
    ) => {
      const classes = classNames(
        Classes.TEXT_FIELD,
        {
          [Classes.TEXT_FIELD_COMPACT]: compact,
          [Classes.COMPACT]: compact,
          [Classes.TEXT_FIELD_DISABLED]: disabled,
          [Classes.DISABLED]: disabled,
          [Classes.TEXT_FIELD_ERROR]: error,
          [Classes.ERROR]: error,
        },
        className
      )

      const labelClasses = classNames(Classes.TEXT_FIELD_LABEL, {
        [Classes.INLINE]: labelInline,
        [Classes.COMPACT]: compact,
        [Classes.DISABLED]: disabled,
        [Classes.ERROR]: error,
      })

      const wrapperClasses = classNames(Classes.TEXT_FIELD_WRAPPER, {
        [Classes.DISABLED]: disabled,
        [Classes.ERROR]: error,
        [Classes.READONLY]: readOnly,
        [Classes.COMPACT]: compact,
      })

      const fieldAggregatorClasses = classNames(Classes.TEXT_FIELD_AGGREGATOR, {
        [Classes.DISABLED]: disabled,
        [Classes.ERROR]: error,
        [Classes.READONLY]: readOnly,
      })

      const actionClasses = classNames(Classes.TEXT_FIELD_ACTION, {
        [Classes.INLINE]: labelInline,
      })

      const helperClasses = classNames(Classes.TEXT_FIELD_HELPER, {
        [Classes.COMPACT]: compact,
      })

      const errorMessageClasses = classNames(Classes.TEXT_FIELD_ERROR_MESSAGE, {
        [Classes.COMPACT]: compact,
      })

      const IconLeft = resolve(iconLeft)
      const IconRight = resolve(iconRight)

      const iconLeftClasses = classNames(Classes.TEXT_FIELD_ICON_LEFT)
      const iconRightClasses = classNames(Classes.TEXT_FIELD_ICON_RIGHT)

      const labelRequiredClasses = classNames(Classes.REQUIRED)

      const _handleActionKeyDown = (event: React.KeyboardEvent<HTMLSpanElement>) => {
        const keyCode = event.keyCode
        switch (keyCode) {
          case KeyCodes.SPACE: {
            safeInvoke(onActionClick)
            break
          }
          case KeyCodes.ENTER: {
            safeInvoke(onActionClick)
            break
          }
          default: {
            break
          }
        }
      }

      return (
        <React.Fragment>
          <div className={classes}>
            <label className={labelClasses}>
              <span>
                {label}
                {required && <span className={labelRequiredClasses}>{" *"}</span>}
              </span>
              <span
                role={onActionClick ? "button" : undefined}
                className={actionClasses}
                onClick={onActionClick}
                onKeyDown={onActionClick ? _handleActionKeyDown : undefined}
                tabIndex={onActionClick ? 0 : undefined}
              >
                {action}
              </span>
            </label>
            <div className={fieldAggregatorClasses} ref={ref}>
              {/*
                  Prefix and suffix are ordered appropriately using the CSS3 order: property.
                  This allows us to use the sibling selector to modify the border radius
                  of the component if a prefix or suffix is present without hinderence.
              */}
              {prefix && (
                <div className={Classes.TEXT_FIELD_PREFIX} aria-describedby="Prefix to the input">
                  <span>{prefix}</span>
                </div>
              )}
              {suffix && (
                <div className={Classes.TEXT_FIELD_SUFFIX} aria-describedby="Suffix to the input">
                  <span>{suffix}</span>
                </div>
              )}
              <div className={wrapperClasses}>
                {IconLeft && <span className={iconLeftClasses}>{<IconLeft />}</span>}
                <Input
                  ref={inputRef}
                  type={type}
                  disabled={disabled}
                  compact={compact}
                  error={error}
                  readOnly={readOnly}
                  placeholder={placeholder}
                  data-invalid={error}
                  {...props}
                />
                {IconRight && <span className={iconRightClasses}>{<IconRight />}</span>}
              </div>
            </div>
            {helper && !error && <div className={helperClasses}>{helper}</div>}
            {error && (
              <div className={errorMessageClasses}>
                <AlertCircle />
                <span>{errorMessage || "Error"}</span>
              </div>
            )}
          </div>
        </React.Fragment>
      )
    }
  )
)

TextField.displayName = generateDisplayName("TextField")
