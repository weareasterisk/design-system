export const isFunction = (fn: unknown): fn is CallableFunction => {
  return typeof fn === "function"
}

/**
 * Safely invokes a function if it exists and returns `undefined` if it doesn't
 *
 * @param fn - The function to be safely invoked
 * @param args - Any parameters to be passed to the invoked function
 * @returns Returns and calls the safely invoked function if it is a funciton, and returns undefined if not
 */
export const safeInvoke = (
  fn: CallableFunction | undefined,
  ...args: unknown[]
): CallableFunction | undefined => {
  if (isFunction(fn)) {
    return fn(...args)
  }
  return undefined
}

/**
 * Safely invokes a function if it exists and returns the function itself if it doesn't
 *
 * @param fn - The function entity to be safely invoked
 * @param args - Any parameters to be passed to the invoked function
 * @returns Returns and calls the safely invoked function with parameters if it is a function, otherwise just call the function
 */
export const safeInvokeEntity = (
  fn: CallableFunction | undefined,
  ...args: unknown[]
): CallableFunction | undefined => {
  return isFunction(fn) ? fn(...args) : fn
}
