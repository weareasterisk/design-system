import { Checkmark } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React from "react"

import { Classes, generateDisplayName } from "../../common"

/**
 * @internal
 * Props for the `ListBoxMenuItem` component
 */
export interface ListBoxMenuItemProps
  extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLLIElement>, HTMLLIElement> {
  /** Whether the item is currently active */
  isActive: boolean
  /** Whether the item is currently highlighted/selected */
  isHighlighted: boolean
  /** Whether to use compact styling when rendering */
  compact: boolean
}

/** @internal */
export const ListBoxMenuItem = React.memo(
  React.forwardRef<HTMLLIElement, ListBoxMenuItemProps>(
    ({ children, isActive, isHighlighted, compact, ...props }, ref) => {
      const classes = classNames(Classes.LIST_BOX_MENU_ITEM, {
        [Classes.LIST_BOX_MENU_ITEM_ACTIVE]: isActive,
        [Classes.LIST_BOX_MENU_ITEM_HIGHLIGHTED]: isHighlighted,
        [Classes.COMPACT]: compact,
      })

      return (
        <li className={classes} ref={ref} {...props}>
          <div className={Classes.LIST_BOX_MENU_ITEM_OPTION}>
            <span>{children}</span>
            {isActive && <Checkmark />}
          </div>
        </li>
      )
    }
  )
)

ListBoxMenuItem.displayName = generateDisplayName("ListBoxMenuItem")
