import { InfoOutline } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React from "react"

import { Classes, generateDisplayName } from "../../common"
import { ListBoxElementCommonProps } from "./ListBox"

/**
 * @internal
 * Props for the ListBoxLabel component
 */
export interface ListBoxLabelProps
  extends ListBoxElementCommonProps,
    React.DetailedHTMLProps<React.HTMLAttributes<HTMLLabelElement>, HTMLLabelElement> {
  /** Tooltip helper for the component */
  tooltip?: string
  /** Specify the label text of the component */
  label?: string
}

/** @internal */
export const ListBoxLabel = React.memo(
  React.forwardRef<HTMLLabelElement, ListBoxLabelProps>(
    ({ className, children, compact, disabled, tooltip, label, ...props }, ref) => {
      const labelClasses = classNames(
        Classes.LIST_BOX_LABEL,
        {
          [Classes.COMPACT]: compact,
          [Classes.DISABLED]: disabled,
        },
        className
      )
      return (
        <label {...props} ref={ref}>
          {(label || tooltip) && (
            <div className={labelClasses}>
              {label}
              {tooltip && <InfoOutline title={tooltip} />}
            </div>
          )}
          {children}
        </label>
      )
    }
  )
)

ListBoxLabel.displayName = generateDisplayName("ListBoxLabel")
