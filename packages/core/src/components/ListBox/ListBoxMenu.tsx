import classNames from "classnames"
import React from "react"

import { Classes, CommonProps, generateDisplayName } from "../../common"

/** @internal
 * Props for the `ListBoxMenu` component
 * */
export interface ListBoxMenuProps
  extends CommonProps,
    React.DetailedHTMLProps<React.HTMLAttributes<HTMLUListElement>, HTMLUListElement> {}

/** @internal */
export const ListBoxMenu = React.memo(
  React.forwardRef<HTMLUListElement, ListBoxMenuProps>(({ children, className, ...props }, ref) => {
    const classes = classNames(Classes.LIST_BOX_MENU, className)
    return (
      <ul className={classes} role="listbox" ref={ref} {...props}>
        {children}
      </ul>
    )
  })
)

ListBoxMenu.displayName = generateDisplayName("ListBoxMenu")
