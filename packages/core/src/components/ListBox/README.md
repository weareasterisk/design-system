# `ListBox` Component

`ListBox` powers a variety of dropdown-esque components in Hyphen such as:

- Dropdown

This component is not exported in the public API and is exclusively used to build auxiliary
components internally.
