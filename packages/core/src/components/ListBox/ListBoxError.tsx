import { AlertCircle } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React from "react"

import { Classes, generateDisplayName, HTMLDivProps } from "../../common"
import { ListBoxElementCommonProps } from "./ListBox"

/** @internal
 * Props for the `ListBoxError` componen
 * */
export interface ListBoxErrorProps extends ListBoxElementCommonProps, HTMLDivProps {
  /** Whether or not the component is in an error state */
  error?: boolean
  /** Message to render when the component is in an error state */
  errorMessage?: string
}

/** @internal */
export const ListBoxError = React.memo(
  React.forwardRef<HTMLDivElement, ListBoxErrorProps>(
    ({ className, error, errorMessage, compact, ...props }, ref) => {
      const errorClassNames = classNames(
        Classes.LIST_BOX_ERROR,
        {
          [Classes.COMPACT]: compact,
        },
        className
      )

      return (
        <React.Fragment>
          {error && (
            <div className={errorClassNames} ref={ref} {...props}>
              <AlertCircle title="Error" />
              <span>{errorMessage || "Error"}</span>
            </div>
          )}
        </React.Fragment>
      )
    }
  )
)

ListBoxError.displayName = generateDisplayName("ListBoxError")
