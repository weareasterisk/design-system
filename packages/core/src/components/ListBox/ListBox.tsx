import classNames from "classnames"
import React from "react"

import { Classes, CommonProps, generateDisplayName, HTMLDivProps, Key } from "../../common"

/**
 * @internal
 * Props common to `ListBox` elements
 * */
export interface ListBoxElementCommonProps extends CommonProps {
  /** Specify whether to use the compact stylings */
  compact?: boolean
  /** Specify whether the component is in a `disabled` state */
  disabled?: boolean
}

/**
 * @internal
 * Props for the ListBox Component
 */
export interface ListBoxProps extends ListBoxElementCommonProps, HTMLDivProps {
  /** Specify whether the ListBox is in an error state */
  error?: boolean
  /** Specify if the ListBox is open */
  isOpen?: boolean
  /** Specify whether the listbox will use inline styles */
  inline?: boolean
}

/**
 * Prevent event propogation when the user clicks on the element
 * @internal
 * @param event - Mouse Event on click
 */
const handleClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
  event.preventDefault()
  event.stopPropagation()
}

/**
 * Stop event propogation when the user presses escape
 * @internal
 * @param event - Keyboard event on keypress
 */
const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
  if (event.key === Key.Escape) {
    event.stopPropagation()
  }
}
/** @internal */
export const ListBox = React.memo(
  React.forwardRef<HTMLDivElement, ListBoxProps>(
    ({ className, children, compact, disabled, error, isOpen, inline, ...props }, ref) => {
      const listBoxClasses = classNames(
        Classes.LIST_BOX,
        {
          [Classes.COMPACT]: compact,
          [Classes.ERROR]: error,
          [Classes.DISABLED]: disabled,
          [Classes.LIST_BOX_EXPANDED]: isOpen,
          [Classes.INLINE]: inline,
        },
        className
      )

      return (
        <React.Fragment>
          {/* Ignore statements below to allow passing of functions that prevent event propogation */}
          {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions */}
          <div
            className={listBoxClasses}
            ref={ref}
            data-invalid={error || undefined}
            onKeyDown={handleKeyDown}
            onClick={handleClick}
            {...props}
          >
            {children}
          </div>
        </React.Fragment>
      )
    }
  )
)

ListBox.displayName = generateDisplayName("ListBox")
