import React from "react"

import { ListBox, ListBoxError, ListBoxLabel } from "."

export default {
  title: "core/internal/List Box",
  parameters: {
    component: ListBox,
  },
}

export const _Default = (): JSX.Element => {
  return <ListBox disabled>AasdsadAA</ListBox>
}
_Default.story = {
  name: "List Box",
}

export const Error = (): JSX.Element => {
  return <ListBoxError error>Error Text</ListBoxError>
}

export const Label = (): JSX.Element => {
  return <ListBoxLabel label="Label" tooltip="Tooltip" />
}
