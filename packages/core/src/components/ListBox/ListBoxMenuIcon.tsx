import { ChevronDownOutline } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React, { useEffect, useState } from "react"

import { Classes, generateDisplayName, HTMLDivProps } from "../../common"
import { ListBoxElementCommonProps } from "./ListBox"

/**
 * @internal
 * Props for the `ListBoxMenu` component
 */
export interface ListBoxMenuIconProps extends ListBoxElementCommonProps, HTMLDivProps {
  isOpen?: boolean
}

/** @internal */
export const ListBoxMenuIcon = React.memo(
  React.forwardRef<HTMLDivElement, ListBoxMenuIconProps>(({ isOpen, compact }, ref) => {
    const [description, setDescription] = useState<string>("open.menu")

    useEffect(() => {
      setDescription(isOpen ? "close.menu" : "open.menu")
    }, [isOpen])

    const classes = classNames(Classes.LIST_BOX_MENU_ICON, {
      [Classes.LIST_BOX_MENU_ICON_OPEN]: isOpen,
      [Classes.COMPACT]: compact,
    })

    return (
      <div className={classes} ref={ref}>
        <ChevronDownOutline aria-label={description} title={description} />
      </div>
    )
  })
)

ListBoxMenuIcon.displayName = generateDisplayName("ListBoxMenuIcon")
