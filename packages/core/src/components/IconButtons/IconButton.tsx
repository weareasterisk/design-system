import classNames from "classnames"
import React, { ReactNode } from "react"

import { Classes, generateDisplayName } from "../../common"
import { Button } from "../Buttons/Button"
import { AbstractButtonProps } from "../Buttons/withStyles"

export interface IconButtonProps
  extends AbstractButtonProps,
    React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
  content: ReactNode
  xsmall?: boolean
  small?: boolean
  large?: boolean
  xlarge?: boolean
  xxlarge?: boolean
}
export const IconButton = React.memo(
  React.forwardRef<HTMLButtonElement, IconButtonProps>(
    ({ xsmall, small, large, xlarge, xxlarge, content, ...props }, ref) => {
      const size = xxlarge
        ? Classes.ICON_BUTTON_XXLARGE
        : xlarge
        ? Classes.ICON_BUTTON_XLARGE
        : large
        ? Classes.ICON_BUTTON_LARGE
        : small
        ? Classes.ICON_BUTTON_SMALL
        : xsmall
        ? Classes.ICON_BUTTON_XSMALL
        : ""

      const classes = classNames(Classes.ICON_BUTTON, size)
      return (
        <Button plain className={classes} {...props} ref={ref}>
          {content}
        </Button>
      )
    }
  )
)
IconButton.displayName = generateDisplayName("IconButton")
