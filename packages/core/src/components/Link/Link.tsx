import { ExternalLink } from "@weareasterisk/hyphen-ui-icons"
import classnames from "classnames"
import React from "react"

import { Classes } from "../../common"

interface LinkProps
  extends React.DetailedHTMLProps<
    React.AnchorHTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  > {
  disabled?: boolean
  inline?: boolean
  icon?: boolean
  danger?: boolean
}

export const Link = React.memo(
  React.forwardRef<HTMLAnchorElement, LinkProps>(
    ({ disabled, inline, icon, danger, href, children, ...props }, ref) => {
      const linkClasses = classnames(Classes.LINK, Classes.LINK_CONTENT, {
        [Classes.DISABLED]: disabled,
        [Classes.INLINE]: inline,
        [Classes.ICON]: icon,
        [Classes.LINK_DANGER]: danger,
      })

      const iconClasses = classnames(Classes.LINK_ICON)
      const linkContentClasses = classnames(Classes.LINK_CONTENT)

      return (
        <React.Fragment>
          <a className={linkClasses} ref={ref} href={href} {...props}>
            {children}
            {icon && (
              <span className={iconClasses}>
                <ExternalLink />
              </span>
            )}
          </a>
        </React.Fragment>
      )
    }
  )
)

Link.displayName = "Link"
