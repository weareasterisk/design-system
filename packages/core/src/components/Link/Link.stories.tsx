import { Link } from "@weareasterisk/hyphen-ui-core"
import React, { useRef, useState } from "react"

import mdx from "./Link.mdx"

export default {
  title: "core/Link",
  parameters: {
    docs: {
      page: mdx,
    },
  },
}

export const Standalone = (): JSX.Element => {
  return (
    <React.Fragment>
      Tset <Link href={"https://www.google.com?" + new Date().getTime()}>TESTS</Link>
    </React.Fragment>
  )
}

export const Standalone_Icon = (): JSX.Element => {
  return (
    <React.Fragment>
      Test{" "}
      <Link href={"https://www.google.com?" + new Date().getTime()} icon={true}>
        TESTS
      </Link>
    </React.Fragment>
  )
}

export const Standalone_Inline = (): JSX.Element => {
  return (
    <React.Fragment>
      <Link href={"https://https://www.google.com"} inline={true}>
        TESTS
      </Link>
    </React.Fragment>
  )
}

export const Danger = (): JSX.Element => {
  return (
    <React.Fragment>
      <div>
        Test{" "}
        <Link href={"https://www.google.com?" + new Date().getTime()} danger={true}>
          TESTS
        </Link>
      </div>
    </React.Fragment>
  )
}

export const Disabled = (): JSX.Element => {
  return (
    <React.Fragment>
      Test{" "}
      <Link href={"https://www.google.com?" + new Date().getTime()} disabled={true}>
        TESTS
      </Link>
    </React.Fragment>
  )
}

export const Standalone_Icon_In_Header = (): JSX.Element => {
  return (
    <React.Fragment>
      <h1>
        Test Tst
        <Link href={"https://www.google.com?" + new Date().getTime()} icon={true}>
          TESTS
        </Link>
      </h1>
    </React.Fragment>
  )
}
