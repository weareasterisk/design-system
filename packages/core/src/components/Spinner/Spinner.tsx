import { SpinnerWithoutTrack, SpinnerWithTrack } from "@weareasterisk/hyphen-ui-icons"
import classnames from "classnames"
import React, { forwardRef, Fragment, memo } from "react"

import { Classes } from "../../common"

export enum SpinnerSize {
  large = "large",
  medium = "medium",
  small = "small",
  tiny = "tiny",
}

export enum SpinnerEasing {
  linear = "linear",
  rollercoaster = "rollercoaster",
}

export interface SpinnerProps {
  size?: SpinnerSize
  hideTrack?: boolean
  easing?: SpinnerEasing
  large?: boolean
  medium?: boolean
  small?: boolean
  tiny?: boolean
}

export const Spinner = memo(
  forwardRef<HTMLElement, SpinnerProps>(
    (
      {
        size = SpinnerSize.medium,
        hideTrack,
        easing = SpinnerEasing.rollercoaster,
        large,
        small,
        tiny,
      },
      ref
    ) => {
      const derivedSize: SpinnerSize | null = large
        ? SpinnerSize.large
        : small
        ? SpinnerSize.small
        : tiny
        ? SpinnerSize.tiny
        : null

      const trueSize = derivedSize ?? size

      const SpinnerClasses = classnames(Classes.SPINNER, {
        [Classes.SPINNER_LARGE]: trueSize == SpinnerSize.large || large,
        [Classes.SPINNER_SMALL]: trueSize == SpinnerSize.small || small,
        [Classes.SPINNER_TINY]: trueSize == SpinnerSize.tiny || tiny,
        [Classes.SPINNER_EASING_ROLLERCOASTER]: easing === SpinnerEasing.rollercoaster,
      })

      return (
        <Fragment>
          <span className={SpinnerClasses} ref={ref}>
            {hideTrack ? <SpinnerWithoutTrack /> : <SpinnerWithTrack />}
          </span>
        </Fragment>
      )
    }
  )
)

Spinner.displayName = "Spinner"
