import { Spinner, SpinnerEasing, SpinnerSize } from "@weareasterisk/hyphen-ui-core"
import React, { useRef, useState } from "react"

import mdx from "./Spinner.mdx"

export default {
  title: "core/Spinner",
  parameters: {
    docs: {
      page: mdx,
    },
  },
}

export const Primary = (): JSX.Element => {
  return (
    <React.Fragment>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "1fr 1fr 1fr 1fr",
          justifyContent: "space-evenly",
          alignContent: "center",
          gridGap: 16,
        }}
      >
        <h2 style={{ gridColumn: "span 4" }}>Rollercoaster Spin (default)</h2>
        <Spinner large />
        <Spinner />
        <Spinner small />
        <Spinner tiny />

        <h2 style={{ gridColumn: "span 4" }}>Hidden Track, Rollercoaster Spin</h2>
        <Spinner hideTrack={true} size={SpinnerSize.large} />
        <Spinner hideTrack={true} />
        <Spinner hideTrack={true} size={SpinnerSize.small} />
        <Spinner hideTrack={true} size={SpinnerSize.tiny} />

        <h2 style={{ gridColumn: "span 4" }}>Linear Spin</h2>
        <Spinner easing={SpinnerEasing.linear} size={SpinnerSize.large} />
        <Spinner easing={SpinnerEasing.linear} />
        <Spinner easing={SpinnerEasing.linear} size={SpinnerSize.small} />
        <Spinner easing={SpinnerEasing.linear} size={SpinnerSize.tiny} />

        <h2 style={{ gridColumn: "span 4" }}>Hidden Track, Linear Spin</h2>
        <Spinner easing={SpinnerEasing.linear} hideTrack={true} size={SpinnerSize.large} />
        <Spinner easing={SpinnerEasing.linear} hideTrack={true} />
        <Spinner easing={SpinnerEasing.linear} hideTrack={true} size={SpinnerSize.small} />
        <Spinner easing={SpinnerEasing.linear} hideTrack={true} size={SpinnerSize.tiny} />
      </div>
    </React.Fragment>
  )
}

export const Playground = (): JSX.Element => {
  return (
    <React.Fragment>
      <Spinner />
    </React.Fragment>
  )
}
