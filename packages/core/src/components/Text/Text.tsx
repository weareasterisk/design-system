import classNames from "classnames"
import React, { ReactNode } from "react"

import * as Classes from "../../common/classes"
import { CommonProps, generateDisplayName } from "../../common/props"

/**
 * Props for the body text component
 */
export interface TextProps
  extends CommonProps,
    React.DetailedHTMLProps<React.HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {
  /**
   * The tag that the text component will use to render content
   */
  tag?: keyof JSX.IntrinsicElements

  /**
   * Children passed to the Text component
   */
  children: ReactNode

  /**
   * The size of the body text
   */
  size?: "normal" | "small" | "tiny"

  /**
   * Use the normal body text styling
   */
  normal?: boolean

  /**
   * Use the small body text styling
   */
  small?: boolean

  /**
   * Use the tiny body text styling
   */
  tiny?: boolean

  /**
   * Whether or not the text is bolded
   */
  bold?: boolean

  /**
   * Whether or not the text is underlined
   */
  underline?: boolean
}

/**
 * Text component to render body text
 */
export const Text = React.memo(
  React.forwardRef<HTMLElement, TextProps>(
    (
      {
        className,
        children,
        tag = "p",
        size = "normal",
        normal = true,
        small,
        tiny,
        bold = false,
        underline = false,
        ...props
      },
      ref
    ) => {
      const classes = classNames(
        {
          [Classes.TEXT_BOLD]: bold,
          [Classes.TEXT_UNDERLINED]: underline,
          [Classes.TEXT_NORMAL]: size === "normal" || normal,
          [Classes.TEXT_SMALL]: size === "small" || small,
          [Classes.TEXT_TINY]: size === "tiny" || tiny,
        },
        className
      )

      return React.createElement(
        tag,
        {
          className: classes,
          ref: ref,
          ...props,
        },
        children
      )
    }
  )
)

Text.displayName = generateDisplayName("Text")
