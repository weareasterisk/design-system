import React, { useEffect } from "react"
import { useState } from "react"
import { DragDropContext, Droppable } from "react-beautiful-dnd"

import { ActionProps, generateDisplayName } from "../../../common"
import { Item } from "./ListItem"

export interface ItemProps extends ActionProps {
  className?: string
  items: Array<any>
  renderFn: (item: any) => JSX.Element
  id: string
  onChange: (items: Array<any>) => any
}

export const DraggableList: React.FC<ItemProps> = ({
  items,
  renderFn,
  className,
  id,
  onChange,
}) => {
  const [itemsList, setItems] = useState(items)

  useEffect(() => {
    setItems(items)
  }, [items])

  const onDragEnd = (result: any) => {
    const { destination, source } = result

    if (!destination) {
      return
    }

    if (destination.droppableId === source.droppableId && destination.index == source.index) {
      return
    }

    const newItems = Array.from(itemsList)
    newItems.splice(source.index, 1)
    newItems.splice(destination.index, 0, itemsList[source.index])

    setItems(newItems)
    onChange(newItems)
  }

  useEffect(() => {
    setItems(items)
  }, [items])

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId={id}>
        {(provided) => (
          <div ref={provided.innerRef} {...provided.droppableProps}>
            {itemsList.map((item: any, index: number) => {
              return (
                <Item item={renderFn(item)} index={index} key={index} className={className}>
                  {provided.placeholder}
                </Item>
              )
            })}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  )
}

DraggableList.displayName = generateDisplayName("DraggableList")
