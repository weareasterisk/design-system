import { MoveOutline } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React from "react"
import { Draggable } from "react-beautiful-dnd"

import { ActionProps, generateDisplayName } from "../../../common"
import * as Classes from "../../../common/classes"

export interface ItemProps extends ActionProps {
  className?: string
  item: JSX.Element
  index: number
}

export const Item: React.FC<ItemProps> = ({ className, item, index }) => {
  const classes = classNames(Classes.LIST_ITEM)
  const lineClass = classNames(Classes.LIST_ITEM_LINE)
  const iconClass = classNames(Classes.LIST_ITEM_ICON)
  const contentClass = classNames(Classes.LIST_ITEM_CONTENT, className)

  return (
    <Draggable draggableId={index.toString()} index={index}>
      {(provided: any) => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          className={classes}
        >
          <MoveOutline className={iconClass} />
          <div className={lineClass} />
          <div className={contentClass}>{item}</div>
        </div>
      )}
    </Draggable>
  )
}
Item.displayName = generateDisplayName("Item")
