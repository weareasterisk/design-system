/**
 * This component should not be exported to the public API
 */

import classNames from "classnames"
import React from "react"

import { ActionProps } from "../../common"
import * as Classes from "../../common/classes"

export type ListVariants = "draggable"

export type ListSizes = "normal"

/**
 * Abstract interface extended by implemented components
 */
export interface AbstractListProps extends ActionProps {
  size?: ListSizes
  /** Use the `normal` size */
  normal?: boolean

  variant?: ListVariants
  draggable?: boolean

  /** Disable the button */
  disabled?: boolean

  loading?: boolean // TODO: implement
  fill?: boolean
}

export const withStyles = <P extends AbstractListProps>(
  WrappedComponent: React.ComponentType<P>
): React.FC<P> => {
  const ComponentWithProps = (props: P) => {
    const {
      className,
      disabled,
      normal,
      size = "normal",
      variant = "primary",
      draggable,
      fill,
      ...rest
      // loading, // TODO: Loading state
    } = props

    /**
     * `Variant` / `Size` derivation ensures that only one state/style
     * is applied at a time.
     */

    const derivedVariant: ListVariants | null = draggable ? "draggable" : null

    // `Normal` state is used by default
    const derivedSize: ListSizes | null = normal ? "normal" : null

    const trueSize = derivedSize ?? size

    const trueVariant = derivedVariant ?? variant

    const classes = classNames(
      Classes.LIST,
      {
        [Classes.DISABLED]: disabled,
        [Classes.LIST_DRAGGABLE]: trueVariant === "draggable",
      },
      className
    )
    return <WrappedComponent className={classes} disabled={disabled} {...(rest as P)} />
  }
  return ComponentWithProps
}
