import { boolean, select, text, withKnobs } from "@storybook/addon-knobs"
import React from "react"

import { DraggableList } from "./components/List"
import { List } from "./List"
import mdx from "./Lists.mdx"
import { ListSizes, ListVariants } from "./withStyles"

export default {
  title: "core/Lists",
  decorators: [withKnobs],
  parameters: {
    component: List,
    subcomponents: { DraggableList },
    docs: {
      page: mdx,
    },
  },
}

const variants = {
  draggable: "draggable",
}

export type SelectTypeKnobValue = string | number | null | undefined

const sizes = {
  normal: "normal",
}

const props = {
  regular: () => {
    return {
      className: "some-class",
      children: text("Children (children)", "Playground List Item"),
      size: select<ListSizes>(
        "List size (size) [Broken down props take priority]",
        sizes,
        "normal"
      ),
      normal: boolean("Normal size (normal)", false),
      variant: select<ListVariants>(
        "List variant (variant) [Broken down props take priority]",
        variants,
        "draggable"
      ),
      draggable: boolean("draggable variant (primary)", false),
    }
  },
}

export const _Default = (): JSX.Element => {
  return (
    <ol>
      <li>Energy Sword</li>
      <li>Vera</li>
      <li>Keyblade</li>
      <li>ACR</li>
    </ol>
  )
}
_Default.story = {
  name: "List",
}

const Example = (items: Array<JSX.Element>): void => {
  console.log(items)
}

export const Draggable = (): JSX.Element => {
  const example = [
    { id: "884", item: "Portal Gun" },
    { id: "884", item: "Identity Disc" },
    { id: "884", item: "Master Sword" },
  ]

  const exampleFn = (example) => {
    return <div key={example.id}>{example.item}</div>
  }

  return <DraggableList items={example} renderFn={exampleFn} id={"sfd9sa0fd"} onChange={Example} />
}

export const Playground = (): JSX.Element => {
  const regularProps = props.regular()

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexWrap: "wrap",
      }}
    >
      <List {...regularProps} />
    </div>
  )
}
