import React from "react"

import { generateDisplayName } from "../../common"
import { AbstractListProps, withStyles } from "./withStyles"

export type ListProps = AbstractListProps

const ListUnstyled = React.memo(
  React.forwardRef<HTMLOListElement, ListProps>((props, ref) => {
    return <ol {...props} ref={ref} />
  })
)
ListUnstyled.displayName = generateDisplayName("List")
export const List = withStyles(ListUnstyled)

export type DraggableListProps = AbstractListProps

const DraggableListUnstyled = React.memo(
  React.forwardRef<HTMLOListElement, DraggableListProps>(({ children, ...props }, ref) => {
    return (
      <ol {...props} ref={ref}>
        {children}
      </ol>
    )
  })
)
DraggableListUnstyled.displayName = generateDisplayName("DraggableList")
export const DraggableList = withStyles(DraggableListUnstyled)
