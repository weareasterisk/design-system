import { withKnobs } from "@storybook/addon-knobs"
import { Dropdown } from "@weareasterisk/hyphen-ui-core"
import React from "react"

import mdx from "./Dropdown.mdx"

export default {
  title: "core/Dropdown",
  decorators: [withKnobs],
  parameters: {
    component: Dropdown.type,
    docs: {
      page: mdx,
    },
  },
}

interface KeyLabelItem {
  key: string
  label: string
}

const itemToString = (item: KeyLabelItem) => {
  return "Common Prefix " + item?.label.slice() + " Common Suffix" || ""
}

export const _Default = (): JSX.Element => {
  return (
    <Dropdown
      itemToString={itemToString}
      items={[
        { key: "1", label: "Label 1" },
        { key: "2", label: "Label 2" },
        { key: "3", label: "Label 3" },
        { key: "4", label: "Label 4" },
        { key: "5", label: "Label 5" },
        { key: "6", label: "Label 6" },
        { key: "7", label: "Label 7" },
        { key: "8", label: "Label 8" },
      ]}
      label={"Test"}
    />
  )
}
_Default.story = {
  name: "Dropdown",
}

export const Compact = (): JSX.Element => {
  return (
    <Dropdown
      itemToString={itemToString}
      items={[
        { key: "1", label: "Label 1" },
        { key: "2", label: "Label 2" },
        { key: "3", label: "Label 3" },
        { key: "4", label: "Label 4" },
        { key: "5", label: "Label 5" },
        { key: "6", label: "Label 6" },
        { key: "7", label: "Label 7" },
        { key: "8", label: "Label 8" },
      ]}
      label={"Test"}
      tooltip={"asdasd"}
      compact
    />
  )
}

export const Error = (): JSX.Element => {
  return (
    <Dropdown
      itemToString={itemToString}
      items={[
        { key: "1", label: "Label 1" },
        { key: "2", label: "Label 2" },
        { key: "3", label: "Label 3" },
        { key: "4", label: "Label 4" },
        { key: "5", label: "Label 5" },
        { key: "6", label: "Label 6" },
        { key: "7", label: "Label 7" },
        { key: "8", label: "Label 8" },
      ]}
      label={"Test"}
      tooltip={"asdasd"}
      error
    />
  )
}

export const Disabled = (): JSX.Element => {
  return (
    <Dropdown
      itemToString={itemToString}
      items={[
        { key: "1", label: "Label 1" },
        { key: "2", label: "Label 2" },
        { key: "3", label: "Label 3" },
        { key: "4", label: "Label 4" },
        { key: "5", label: "Label 5" },
        { key: "6", label: "Label 6" },
        { key: "7", label: "Label 7" },
        { key: "8", label: "Label 8" },
      ]}
      label={"Test"}
      tooltip={"asdasd"}
      disabled
    />
  )
}

export const Inline = (): JSX.Element => {
  return (
    <Dropdown
      itemToString={itemToString}
      items={[
        { key: "1", label: "Label 1" },
        { key: "2", label: "Label 2" },
        { key: "3", label: "Label 3" },
        { key: "4", label: "Label 4" },
        { key: "5", label: "Label 5" },
        { key: "6", label: "Label 6" },
        { key: "7", label: "Label 7" },
        { key: "8", label: "Label 8" },
      ]}
      inline
      compact
    />
  )
}
