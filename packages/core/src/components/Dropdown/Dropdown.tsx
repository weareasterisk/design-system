import classNames from "classnames"
import { useSelect, UseSelectProps, UseSelectStateChange } from "downshift"
import React, { forwardRef, memo, Ref, useEffect } from "react"

import { Classes, CommonProps, generateDisplayName, HTMLDivProps } from "../../common"
import { ListBox, ListBoxError, ListBoxLabel, ListBoxMenu, ListBoxMenuItem } from "../ListBox"
import { ListBoxMenuIcon } from "../ListBox/ListBoxMenuIcon"

/** Props for the `Dropdown` component */
export interface DropdownProps<T> extends CommonProps, HTMLDivProps {
  /** Whether to render compact stylings for the Dropdown */
  compact?: boolean
  /** Whether the `Dropdown` is disabled */
  disabled?: boolean
  /** Props used by the `downshift` `useSelect` hook */
  downshiftProps?: UseSelectProps<T>
  /** Whether the `Dropdown` is in an error state */
  error?: boolean
  /** Message to render when the `Dropdown` is in an error state */
  errorMessage?: string
  /** The item that's already selected when the `Dropdown` is rendered */
  initialSelectedItem?: T
  /** Whether the `Dropdown` uses inline styling */
  inline?: boolean
  /** Function to use when converting passed objects into a label for `Dropdown` choices */
  itemToString?: (item: T | null) => string
  /** The items that the `Dropdown` will render in its option list */
  items: Array<T>
  /** The label for the `Dropdown` */
  label?: string
  /** Function that's executed when the `Dropdown` state changes */
  onSelectChange?: (changes: UseSelectStateChange<T>) => void
  /** The item that's currently selected by the `Dropdown` */
  selectedItem?: T
  /** Tooltip to render alongside the `Dropdown` label */
  tooltip?: string
}

export const Dropdown = memo(
  forwardRef(
    <T,>(
      {
        compact,
        disabled,
        downshiftProps,
        error,
        errorMessage,
        label,
        onSelectChange,
        items,
        itemToString,
        selectedItem: controlledSelectedItem,
        initialSelectedItem,
        inline,
        placeholder,
        tooltip,
        // Ignore this ref. It's only extrapolated as to not cause conflict with the listbox
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        ref: omittedRef,
        ...props
      }: DropdownProps<T>,
      ref: Ref<HTMLButtonElement>
    ): JSX.Element => {
      const onSelectedItemChange = (changes: UseSelectStateChange<T>): void => {
        if (onSelectChange) {
          onSelectChange(changes)
        }
      }

      const selectProps: UseSelectProps<T> = {
        ...downshiftProps,
        items,
        itemToString,
        initialSelectedItem,
        onSelectedItemChange,
      }

      const {
        isOpen,
        selectedItem,
        getToggleButtonProps,
        getLabelProps,
        getMenuProps,
        highlightedIndex,
        getItemProps,
      } = useSelect<T>(selectProps)

      useEffect(() => {
        if (controlledSelectedItem !== undefined) {
          selectProps.selectedItem = controlledSelectedItem
        }
      })

      const objectStringifyFunction =
        itemToString ||
        ((item: T): string => {
          if (typeof item === "string") {
            // slice() to ensure the item is not passed by reference
            return item.slice()
          }
          return JSON.stringify(item)
        })

      const fieldClasses = classNames(Classes.LIST_BOX_FIELD, {
        [Classes.DISABLED]: disabled,
        [Classes.COMPACT]: compact,
        [Classes.ERROR]: error,
        [Classes.INLINE]: inline,
        [Classes.LIST_BOX_FIELD_PLACEHOLDER]: !selectedItem,
      })

      return (
        <ListBox
          compact={compact}
          disabled={disabled}
          error={error}
          inline={inline}
          isOpen={isOpen}
          {...props}
        >
          <ListBoxLabel
            label={label}
            tooltip={tooltip}
            compact={compact}
            disabled={disabled}
            {...getLabelProps()}
          >
            <button
              type="button"
              ref={ref}
              disabled={disabled}
              aria-disabled={disabled}
              className={fieldClasses}
              {...getToggleButtonProps()}
            >
              <span>
                {selectedItem
                  ? objectStringifyFunction(selectedItem)
                  : placeholder || "Choose an Option"}
              </span>
              <ListBoxMenuIcon isOpen={isOpen} compact={compact} />
            </button>
            <ListBoxMenu {...getMenuProps()}>
              {isOpen &&
                items.map((item, index) => {
                  const itemProps = getItemProps({ item, index })
                  return (
                    <ListBoxMenuItem
                      key={itemProps.id}
                      isActive={selectedItem === item}
                      isHighlighted={highlightedIndex === index}
                      title={objectStringifyFunction(item)}
                      compact={compact}
                      {...itemProps}
                    >
                      {objectStringifyFunction(item)}
                    </ListBoxMenuItem>
                  )
                })}
            </ListBoxMenu>
          </ListBoxLabel>
          {!inline && <ListBoxError error={error} errorMessage={errorMessage} />}
        </ListBox>
      )
    }
  )
)

Dropdown.displayName = generateDisplayName("Dropdown")
