import { Close } from "@weareasterisk/hyphen-ui-icons"
import classnames from "classnames"
import React, { ReactNode, useCallback } from "react"
import ReactModal, { Props as ReactModalProps } from "react-modal"
import { isObject } from "util"

import { Classes } from "../../common"
import { Button } from "../Buttons"
import { Heading } from "../Heading"

export interface ModalProps extends ReactModalProps {
  cancelText?: string
  children?: ReactNode
  confirmText?: string
  onCancel?: () => Promise<void>
  onConfirm?: () => Promise<void>
  title: string
}

export const Modal = React.memo(
  React.forwardRef<ReactModal, ModalProps>(
    (
      {
        title,
        children,
        onCancel,
        onConfirm,
        cancelText = "Cancel",
        confirmText = "Submit",
        className,
        overlayClassName,
        onRequestClose,
        ...reactModalProps
      },
      ref
    ) => {
      const contentLabel = reactModalProps.contentLabel ?? title

      const ModalClasses = classnames(Classes.MODAL, className)
      const ModalAfterOpenClasses = classnames(Classes.MODAL_AFTER_OPEN)
      const ModalBeforeCloseClasses = classnames(Classes.MODAL_BEFORE_CLOSE)
      const ModalHeaderClasses = classnames(Classes.MODAL_HEADER)
      const ModalMainClasses = classnames(Classes.MODAL_MAIN)
      const ModalHeaderCloseButtonClasses = classnames(Classes.MODAL_HEADER_CLOSE_BUTTON)
      const ModalOverlayClasses = classnames(Classes.MODAL_OVERLAY, overlayClassName)
      const ModalFooterClasses = classnames(Classes.MODAL_FOOTER)

      const VisuallyHiddenClass = classnames({
        [Classes.VISUALLY_HIDDEN]: true,
      })

      const handleConfirm = useCallback(async () => {
        if (onConfirm) {
          await onConfirm()
        }
      }, [onConfirm])

      const handleCancel = useCallback(
        async (event: React.MouseEvent<Element, MouseEvent> | React.KeyboardEvent<Element>) => {
          if (onCancel) {
            await onCancel()
          }
          if (onRequestClose) {
            await onRequestClose(event)
          }
        },
        [onCancel, onRequestClose]
      )

      return (
        <React.Fragment>
          <ReactModal
            {...reactModalProps}
            ref={ref}
            contentLabel={contentLabel}
            className={{
              base: ModalClasses,
              afterOpen: ModalAfterOpenClasses,
              beforeClose: ModalBeforeCloseClasses,
            }}
            overlayClassName={{
              base: ModalOverlayClasses,
              afterOpen: ModalOverlayClasses,
              beforeClose: ModalOverlayClasses,
            }}
            closeTimeoutMS={reactModalProps.closeTimeoutMS ?? 200}
            onRequestClose={handleCancel}
          >
            <header className={ModalHeaderClasses}>
              <Heading component="h2" variant="h3" medium={true}>
                {title}
              </Heading>
              <Button
                variant="plain"
                mini={true}
                className={ModalHeaderCloseButtonClasses}
                onClick={handleCancel}
                aria-label="Close"
              >
                <Close />
                <span className={VisuallyHiddenClass}>Close {title} Modal</span>
              </Button>
            </header>
            <main className={ModalMainClasses}>{children}</main>
            {onConfirm && (
              <footer className={ModalFooterClasses}>
                <Button onClick={handleCancel} compact={true} variant="secondary">
                  {cancelText}
                </Button>
                <Button compact={true} onClick={handleConfirm}>
                  {confirmText}
                </Button>
              </footer>
            )}
          </ReactModal>
        </React.Fragment>
      )
    }
  )
)

Modal.displayName = "Modal"
