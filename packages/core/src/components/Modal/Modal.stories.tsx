import { boolean, text } from "@storybook/addon-knobs"
import { Button, Modal, TextArea, TextField } from "@weareasterisk/hyphen-ui-core"
import React, { useRef, useState } from "react"

export default {
  title: "core/Modal",
}

export const Primary = (): JSX.Element => {
  const appRef = useRef<any>(null)
  const [isOpen, setIsOpen] = useState<boolean>(false)

  return (
    <React.Fragment>
      <div ref={appRef}>
        <Button onClick={() => setIsOpen(!isOpen)}>Toggle Modal</Button>
        <Modal
          title={"Test"}
          isOpen={isOpen}
          appElement={appRef.current} // this is optional, will mount to document.body by default
          onCancel={async () => setIsOpen(false)}
          onConfirm={async () => setIsOpen(false)}
        >
          <div style={{ display: "grid", gridGap: 16 }}>
            <TextField label="Event Name" />
            <TextArea label="Event Admin Users" />
          </div>
        </Modal>
      </div>
    </React.Fragment>
  )
}

export const ModalOnly = (): JSX.Element => {
  const appRef = useRef<any>(null)
  const [isOpen, setIsOpen] = useState<boolean>(false)

  return (
    <React.Fragment>
      <div ref={appRef}>
        <Button onClick={() => setIsOpen(!isOpen)}>Toggle Modal</Button>
        <Modal
          title={"Test"}
          isOpen={isOpen}
          appElement={appRef.current} // this is optional, will mount to document.body by default
          onCancel={async () => setIsOpen(false)}
        >
          <div style={{ display: "grid", gridGap: 16 }}>
            <TextField label="Event Name" />
            <TextArea label="Event Admin Users" />
          </div>
        </Modal>
      </div>
    </React.Fragment>
  )
}

export const Playground = (): JSX.Element => {
  return (
    <React.Fragment>
      <Modal
        title={text("Title of the modal", "Change me")}
        isOpen={boolean("Show Modal", false)}
        cancelText={text("Cancel button text", "Change me")}
        confirmText={text("Confirm button text", "Change me")}
      >
        <div style={{ display: "grid", gridGap: 16 }}>
          <TextField label="Event Name" />
          <TextArea label="Event Admin Users" />
        </div>
      </Modal>
    </React.Fragment>
  )
}
