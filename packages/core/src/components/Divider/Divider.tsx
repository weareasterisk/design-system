import classNames from "classnames"
import React from "react"

import { Classes, CommonProps, generateDisplayName } from "../../common"

/**
 * Props for the divider component
 */
export interface DividerProps
  extends CommonProps,
    React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  /**
   * HTML tag to use when rendering the element
   * @defaultValue div
   */
  tagName?: keyof JSX.IntrinsicElements

  /**
   * The variant of the divider to use.
   *
   * @defaultValue light
   */
  variant?: "lighter" | "light" | "dark" | "darker"

  /**
   * Whether the divider is compacted. Compaction removes
   * vertical margin
   */
  compact?: boolean
}

export const Divider = React.memo(
  React.forwardRef<HTMLDivElement, DividerProps>(
    ({ tagName = "div", variant = "light", className, compact, ...props }, ref) => {
      const classes = classNames(
        Classes.DIVIDER,
        {
          [Classes.DIVIDER_LIGHTER]: variant === "lighter",
          // Default styles are Light
          // [Classes.DIVIDER_LIGHT]: variant === "light",
          [Classes.DIVIDER_DARK]: variant === "dark",
          [Classes.DIVIDER_DARKER]: variant === "darker",
          [Classes.COMPACT]: compact,
        },
        className
      )

      return React.createElement(tagName, {
        ...props,
        className: classes,
        ref,
      })
    }
  )
)

Divider.displayName = generateDisplayName("Divider")
