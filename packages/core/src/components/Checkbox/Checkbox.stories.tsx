import { storiesOf } from "@storybook/react"
import { Checkbox } from "@weareasterisk/hyphen-ui-core"
import React, { useState } from "react"

export default {
  title: "Checkbox",
  component: Checkbox,
}

storiesOf("Checkbox", module)
  .add("with label", () => <Checkbox label={"test test test text"} />)
  .add("controlled", () => {
    const [checkboxState, setCheckboxState] = useState<boolean>(false)

    const setAndPrintCheckboxState = (e: React.ChangeEvent<HTMLInputElement>): void => {
      const target = e.target
      const value = target.checked

      setCheckboxState(value)
    }

    return <Checkbox checked={checkboxState} onChange={setAndPrintCheckboxState} />
  })
  .add("disabled", () => <Checkbox disabled={true} />)
  .add("checked by default", () => <Checkbox defaultChecked={true} />)
  .add("disabled + checked by default", () => <Checkbox disabled={true} defaultChecked={true} />)
  .add("button + checked by default", () => <Checkbox defaultChecked={true} button={true} />)
  .add("indeterminate by default", () => <Checkbox defaultIndeterminate={true} />)
  .add("indeterminate", () => <Checkbox indeterminate={true} />)
