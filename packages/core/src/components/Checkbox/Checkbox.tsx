import classNames from "classnames"
import React, { useEffect, useState } from "react"
import { useRef } from "react"
import mergeRefs from "react-merge-refs"

import { safeInvoke } from "../_utils/function"
import { Classes } from "../../common"
import { ControlProps, generateDisplayName } from "../../common/props"
import Control from "../Control/Control"

/**
 * Internal props specific to the checkbox
 */
export interface CheckboxProps extends ControlProps {
  /**
   * Whether or not the checkbox is intermediate by default
   */
  defaultIndeterminate?: boolean

  /**
   * Whether or not the checkbox is intermediate, or "partially checked."
   *
   * This prop takes precedence over "checked"
   */
  indeterminate?: boolean
}

export const Checkbox = React.memo(
  React.forwardRef<HTMLInputElement, CheckboxProps>(
    ({ indeterminate, defaultIndeterminate, inputRef, onChange, className, ...props }, ref) => {
      const [isIndeterminate, setIsIndeterminate] = useState<boolean>(
        indeterminate || defaultIndeterminate || false
      )

      const internalRef = useRef<HTMLInputElement>(null)

      const _handleInputRef = (ref: HTMLInputElement): void => {
        safeInvoke(inputRef, ref)
      }

      const _handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const { indeterminate: interm } = event.target
        if (indeterminate == null) {
          setIsIndeterminate(interm)
        }
        safeInvoke(onChange, event)
      }

      useEffect(() => {
        if (internalRef && internalRef.current) {
          internalRef.current.indeterminate = isIndeterminate
        }
      }, [isIndeterminate])

      const checkboxClassnames = classNames(Classes.CHECKBOX, className)

      const combinedRef = mergeRefs([ref, _handleInputRef, internalRef])

      return (
        <Control
          className={checkboxClassnames}
          ref={combinedRef}
          onChange={_handleChange}
          type="checkbox"
          {...props}
        />
      )
    }
  )
)

Checkbox.displayName = generateDisplayName("Checkbox")
