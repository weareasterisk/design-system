import classNames from "classnames"
import React, { ReactNode } from "react"

import { Classes, ControlProps, generateDisplayName } from "../../common"

/**
 * Props internal to the `Control` abstraction
 */
interface ControlInternalProps extends ControlProps {
  /**
   * Class names passed to the type
   */
  typeClassName?: string

  /**
   * Children of the control state indicator
   */
  indicatorChildren?: React.ReactNode

  /**
   * Whether or not the control is rendered as a button
   */
  button?: boolean
}

/**
 * Abstraction for control elements such as `Checkbox`, `Radio`, and `Switch`
 */
const Control = React.forwardRef<HTMLInputElement, ControlInternalProps>(
  (
    {
      children,
      className,
      disabled,
      label,
      labelElement,
      compact,
      type,
      typeClassName,
      indicatorChildren,
      style,
      checked,
      button,
      ...passthrough
    },
    ref
  ) => {
    const wrapperClass = classNames(
      Classes.CONTROL,
      {
        [Classes.DISABLED]: disabled,
        [Classes.COMPACT]: compact,
        [Classes.CONTROL_CHECKED]: checked,
      },
      className
    )

    const typeClasses = classNames(typeClassName)

    const buttonClasses = classNames(Classes.CONTROL_BUTTON)

    const renderControl = (): ReactNode => {
      return (
        <label className={wrapperClass} style={style}>
          <input
            ref={ref}
            type={type}
            checked={checked}
            disabled={disabled}
            className={typeClasses}
            {...passthrough}
          />
          <span className={Classes.CONTROL_INDICATOR}>{indicatorChildren}</span>
          {(label || labelElement || children) && (
            <span>
              {label}
              {labelElement}
              {children}
            </span>
          )}
        </label>
      )
    }

    return (
      <React.Fragment>
        {button ? <div className={buttonClasses}>{renderControl()}</div> : renderControl()}
      </React.Fragment>
    )
  }
)

Control.displayName = generateDisplayName("Control")

export default React.memo(Control)
