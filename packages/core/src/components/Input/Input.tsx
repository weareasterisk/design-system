import classNames from "classnames"
import React from "react"

import { Classes, CommonProps, generateDisplayName, HTMLInputProps } from "../../common"

/**
 * Props for the Input Component
 */
export interface InputProps extends CommonProps, HTMLInputProps {
  /**
   * Whether or not to use compact styling
   */
  compact?: boolean
  /**
   * Whether or not the input is disabled
   */
  disabled?: boolean
  /**
   * Whether or not the component is in an error state
   */
  error?: boolean
}

/**
 * Input Component
 */
export const Input = React.memo(
  React.forwardRef<HTMLInputElement, InputProps>(
    ({ className, compact, disabled, error, ...props }, ref) => {
      const classes = classNames(
        Classes.INPUT,
        {
          [Classes.COMPACT]: compact,
          [Classes.DISABLED]: disabled,
          [Classes.ERROR]: error,
        },
        className
      )

      return (
        <React.Fragment>
          <input ref={ref} disabled={disabled} className={classes} {...props} />
        </React.Fragment>
      )
    }
  )
)

Input.displayName = generateDisplayName("Input")
