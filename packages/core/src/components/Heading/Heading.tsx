import classNames from "classnames"
import React from "react"

import * as Classes from "../../common/classes"
import { CommonProps, generateDisplayName } from "../../common/props"

/**
 * Props for the Heading component
 */
export interface HeadingProps
  extends CommonProps,
    React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement> {
  /**
   * Which component tag to use when rendering the heading
   */
  component?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"
  /**
   * Which style variant to use when rendering the heading
   */
  variant?: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"
  /**
   * Whether or not to use the `Hero` styles
   */
  hero?: boolean
  /**
   * Whether or not to use the `XXLarge` styles
   */
  xxlarge?: boolean
  /**
   * Whether or not to use the `XLarge` styles
   */
  xlarge?: boolean
  /**
   * Whether or not to use the `Large` styles
   */
  large?: boolean
  /**
   * Whether or not to use the `Medium` styles
   */
  medium?: boolean
  /**
   * Whether or not to use the `Normal` styles
   */
  normal?: boolean
}

/**
 * Heading Component
 */
export const Heading = React.memo(
  React.forwardRef<HTMLHeadingElement, HeadingProps>(
    (
      {
        component,
        variant = "h2",
        className,
        children,
        hero,
        xxlarge,
        xlarge,
        large,
        medium,
        normal,
        ...props
      },
      ref
    ) => {
      const classes = classNames(className, Classes.HEADING, {
        [Classes.HEADING_HERO]: variant === "h1" || hero,
        [Classes.HEADING_XXLARGE]: variant === "h2" || xxlarge,
        [Classes.HEADING_XLARGE]: variant === "h3" || xlarge,
        [Classes.HEADING_LARGE]: variant === "h4" || large,
        [Classes.HEADING_MEDIUM]: variant === "h5" || medium,
        [Classes.HEADING_NORMAL]: variant === "h6" || normal,
      })

      const derivedComp = hero
        ? "h1"
        : xxlarge
        ? "h2"
        : xlarge
        ? "h3"
        : large
        ? "h4"
        : medium
        ? "h5"
        : normal
        ? "h6"
        : null

      const comp = component ?? derivedComp ?? variant

      return React.createElement(
        comp,
        {
          className: classes,
          ref: ref,
          ...props,
        },
        children
      )
    }
  )
)

Heading.displayName = generateDisplayName("Heading")

/**
 * Hero Heading Component
 */
export const HeroHeading = React.forwardRef<HTMLHeadingElement, HeadingProps>((props, ref) => (
  <Heading component="h1" variant="h1" {...props} ref={ref} />
))
HeroHeading.displayName = generateDisplayName("HeroHeading")

/**
 * Primary Heading Component
 */
export const PrimaryHeading = React.forwardRef<HTMLHeadingElement, HeadingProps>((props, ref) => (
  <Heading component="h2" variant="h2" {...props} ref={ref} />
))
PrimaryHeading.displayName = generateDisplayName("PrimaryHeading")

/**
 * Secondary Heading Component
 */
export const SecondaryHeading = React.forwardRef<HTMLHeadingElement, HeadingProps>((props, ref) => (
  <Heading component="h3" variant="h3" {...props} ref={ref} />
))
SecondaryHeading.displayName = generateDisplayName("SecondaryHeading")

/**
 * Tertiary Heading Component
 */
export const TertiaryHeading = React.forwardRef<HTMLHeadingElement, HeadingProps>((props, ref) => (
  <Heading component="h4" variant="h4" {...props} ref={ref} />
))
TertiaryHeading.displayName = generateDisplayName("TertiaryHeading")
