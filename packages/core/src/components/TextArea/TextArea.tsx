import { AlertCircle } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React, { useEffect, useRef, useState } from "react"
import mergeRefs from "react-merge-refs"
import TextAreaAutosize, { TextareaAutosizeProps } from "react-textarea-autosize"

import { safeInvoke } from "../_utils/function"
import { Classes, CommonProps, generateDisplayName } from "../../common"

/** Additional props for the `TextArea` Component */
export interface TextAreaProps extends CommonProps, TextareaAutosizeProps {
  /** Whether or not to use compact styling */
  compact?: boolean
  /** Whether or ont to render the character counter */
  counter?: boolean
  /** Whether or not the input is disabled */
  disabled?: boolean
  /** Whether or not the component is in an error state */
  error?: boolean
  /** Error message to display when the component is in an error state */
  errorMessage?: string
  /**
   * Whether or not to grow the `TextArea` vertically as more input is inserted
   * @defaultValue true
   */
  grow?: boolean
  /** Helper text */
  helper?: string
  /** TextArea label */
  label?: string
  /** Placeholder text */
  placeholder?: string
}

export const TextArea = React.memo(
  React.forwardRef<HTMLTextAreaElement, TextAreaProps>(
    (
      {
        className,
        compact,
        counter,
        disabled,
        error,
        errorMessage,
        grow = true,
        helper,
        label,
        maxLength,
        minLength,
        maxRows,
        onChange,
        placeholder,
        required,
        minRows = 3,
        rows,
        ...props
      },
      ref
    ) => {
      const classes = classNames(
        Classes.TEXT_AREA,
        {
          [Classes.COMPACT]: compact,
          [Classes.ERROR]: error,
          [Classes.DISABLED]: disabled,
        },
        className
      )

      const characterCounterClasses = classNames(Classes.TEXT_AREA_CHARACTER_COUNTER, {
        [Classes.COMPACT]: compact,
      })

      const errorMessageClasses = classNames(Classes.ERROR_MESSAGE, {
        [Classes.COMPACT]: compact,
      })

      const helperClasses = classNames(Classes.TEXT_AREA_HELPER, {
        [Classes.COMPACT]: compact,
      })

      const labelClasses = classNames(Classes.LABEL, {
        [Classes.COMPACT]: compact,
      })

      const inputClasses = classNames(Classes.TEXT_AREA_INPUT, {
        [Classes.COMPACT]: compact,
        [Classes.ERROR]: error,
        [Classes.DISABLED]: disabled,
      })

      const labelRequiredClasses = classNames(Classes.REQUIRED)

      const internalRef = useRef<HTMLTextAreaElement>(null)

      const combinedRef = mergeRefs([ref, internalRef])

      const [characterCount, setCharacterCount] = useState(internalRef?.current?.value.length)
      const [characterCountText, setCharacterCountText] = useState("")

      const handleCounterChange = async () => {
        setCharacterCount(internalRef?.current?.value.length)
        safeInvoke(onChange)
      }

      useEffect(() => {
        setCharacterCountText(`${maxLength ? (maxLength || 0) - (characterCount || 0) : "∞"} left`)
      }, [characterCount, maxLength])

      return (
        <React.Fragment>
          <div className={classes}>
            <label>
              <div className={labelClasses}>
                <span>
                  {label}
                  {required && <span className={labelRequiredClasses}>{" *"}</span>}
                </span>
                {counter && <span className={characterCounterClasses}>{characterCountText}</span>}
              </div>
              <TextAreaAutosize
                className={inputClasses}
                disabled={disabled}
                maxLength={maxLength}
                minLength={minLength}
                placeholder={placeholder}
                required={required}
                onChange={counter ? handleCounterChange : onChange}
                maxRows={grow ? maxRows : minRows || rows}
                minRows={minRows}
                rows={rows}
                aria-invalid={error}
                ref={combinedRef}
                {...props}
              />
            </label>
            {helper && !error && <div className={helperClasses}>{helper}</div>}
            {error && (
              <div className={errorMessageClasses}>
                <AlertCircle />
                <span>{errorMessage || "Error"}</span>
              </div>
            )}
          </div>
        </React.Fragment>
      )
    }
  )
)

TextArea.displayName = generateDisplayName("TextArea")
