import { boolean, number, text, withKnobs } from "@storybook/addon-knobs"
import { TextArea } from "@weareasterisk/hyphen-ui-core"
import React from "react"

import mdx from "./TextArea.mdx"

export default {
  title: "core/Text Area",
  decorators: [withKnobs],
  parameters: {
    component: TextArea,
    docs: {
      page: mdx,
    },
  },
}

const props = {
  regular: () => {
    return {
      compact: boolean("Compact size (compact)", undefined),
      counter: boolean("Display counter (counter)", undefined),
      disabled: boolean("Disabled input (disabled)", undefined),
      error: boolean("Status of error state (error)", undefined),
      errorMessage: text("Error message (errorMessage)", "Error"),
      grow: boolean("Grow vertically automatically (grow)", undefined),
      helper: text("Helper text (helper)", "Helper"),
      label: text("Label text (label)", "Label"),
      maxLength: number("Max Length (maxLength)", undefined),
      minLength: number("Min Length (minLength)", undefined),
      placeholder: text("Placeholder (placeholder)", undefined),
      required: boolean("Required (required)", undefined),
      rows: number("Area Rows (rows)", 3),
    }
  },
}

export const _Default = (): JSX.Element => {
  return <TextArea label="Label" counter maxLength={4} />
}
_Default.story = {
  name: "Text Area",
}

export const Playground = (): JSX.Element => {
  const regularProps = props.regular()

  return <TextArea {...regularProps} />
}
