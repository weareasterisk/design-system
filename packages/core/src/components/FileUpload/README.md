# File Upload

## Example Usage

```tsx
import {
  Dropzone,
  FileItem,
  FileUploadProvider,
  SmallDropzone,
  composeUploadFileTypes,
  getImagePreviewUrl,
  presets,
  useDropzone,
  useFileUpload,
  validateFile,
} from "./index"

export const AllowMultipleImages = (): JSX.Element => {
  return (
    <FileUploadProvider allowMultiple={true} fileUploadType={presets.image}>
      <Dropzone variant="file" />
      <SmallDropzone variant="file" />
      <DebugInfo /> {/* <- some custom component that leverages useFileUpload and FileItem */}
    </FileUploadProvider>
  )
}
```

## Components / Hooks / Utils

### `<Dropzone />`

```tsx
export interface DropzoneProps {
  accept?: string // This is an override, will usually be derived from context
  allowMultiple?: boolean // This is an override, will usually be derived from context
  variant?: Variant
}
```

### `<SmallDropzone />`

Uses `DropzoneProps` interface.

### `<FileItem />`

```tsx
interface FileItemProps {
  onRemove: (filename: string) => void
  filename: string
  errorMessage?: string
  hasError?: boolean
}
```

### `useFileUpload`

```tsx
type FileRecord = {
  file: File
  error?: string
}

return {
  attachFile, // file: File => void
  detachFile, // filename: string => void
  clearAllFiles, // () => void
  selectFileByName, // filename: string => FileRecord
  selectAllFiles, // () => FileRecord[]
  selectOptions, // () => { fileUploadType: UploadFileType, allowMultiple: boolean, maxFileSize?: string }
  selectFilesWithErrors, // () => FileRecord[]
  selectValidFiles, // () => FileRecord[]
}
```

### `useDropzone`

```tsx
return {
  fileInputRef, // React.RefObject<HTMLInputElement>
  handleChange, // (e: ChangeEvent<HTMLInputElement>) => void
  handleOnDragLeave, // () => void
  handleOnDragOver, // (e: React.DragEvent<HTMLDivElement>) => void
  handleOnDrop, // (e: DragEvent<HTMLDivElement>) => void
  isFileOver, // boolean (used for styling when dragging a file over the target element)
  openFileDialog, // () => void
  allowMultiple, // boolean
  accept, // string (used for accept attribute in <input /> element)
}
```

## Contribution

This component uses a Context / Hook paradigm.

### Folder Structure

- components: presentation layer
- context: think store and reducer
- hooks: think selectors and action creators
- utils

## Important TODOs

- use React.memo on `components/*`
- use React.forwardRef on `components/*`
- enhance file validation (see `utils/validateFile`)
