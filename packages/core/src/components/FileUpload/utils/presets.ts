import { UploadFileType } from "."

type UploadFileTypesPresetName = "pdf" | "doc" | "image"

export const presets: Record<UploadFileTypesPresetName, UploadFileType> = {
  pdf: {
    extensions: ["pdf"],
    mimeTypes: ["application/pdf"],
  },
  doc: {
    extensions: ["doc", "docx"],
    mimeTypes: [
      "application/msword",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    ],
  },
  //https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
  //https://www.lifewire.com/file-extensions-and-mime-types-3469109#:~:text=MIME%20Types:%20Applications%20%20%20%20Application%20,%20%20hta%20%2036%20more%20rows
  image: {
    extensions: ["png", "jpeg", "jpg", "tiff", "tif", "gif"],
    mimeTypes: ["image/png", "image/jpeg", "image/tiff", "image/gif"],
  },
}
