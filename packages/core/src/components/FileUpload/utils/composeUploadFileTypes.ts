import { UploadFileType } from "."

/**
 * Composes the upload file types
 *
 * @param args - Upload file types to compose
 */
export const composeUploadFileTypes = (...args: UploadFileType[]): UploadFileType => {
  if (args.length === 0) {
    throw new Error("Needs at least one argument to be provided.")
  }
  const removeDuplicatesFromArray = <T>(array: T[]): T[] => Array.from(new Set<T>(array))
  const init: UploadFileType = {
    extensions: [],
    mimeTypes: [],
  }
  return args.reduce((draft, fileType) => {
    return {
      extensions: removeDuplicatesFromArray<string>([...draft.extensions, ...fileType.extensions]),
      mimeTypes: removeDuplicatesFromArray<string>([...draft.mimeTypes, ...fileType.mimeTypes]),
    }
  }, init)
}
