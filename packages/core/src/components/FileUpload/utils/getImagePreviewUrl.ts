export const getImagePreviewUrl = (file: File): Promise<unknown> =>
  new Promise((resolve) => {
    const isValidImage = /\.(jpe?g|png|gif)$/i.test(file.name) // TODO: refine this
    if (isValidImage) {
      const reader = new FileReader()
      reader.addEventListener("load", () => {
        resolve(reader.result)
      })
      reader.readAsDataURL(file)
    } else {
      throw new Error("Unable to get image preview url.")
    }
  })
