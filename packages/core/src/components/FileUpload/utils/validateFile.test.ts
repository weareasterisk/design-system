import bytes from "bytes"
import * as faker from "faker"

import { composeUploadFileTypes } from "./composeUploadFileTypes"
import { presets } from "./presets"
import { validateFile } from "./validateFile"

const createFile = () => {
  const user = {
    email: faker.internet.email(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
  }
  const toJson = (json: unknown) => JSON.stringify(json, null, 2)
  const content = [toJson(user)]
  const filename = `${user.firstName}_${user.lastName}.json`.toLowerCase()
  const options = { type: "application/json" }
  const file = new File(content, filename, options)
  return file
}

describe("utils/validateFile", () => {
  it("should throw if less than the default maxFileSize", () => {
    expect(() => {
      const file: File = {
        ...createFile(),
        size: bytes.parse("6mb"),
      }
      validateFile(file, {
        uploadFileType: presets.pdf,
      })
    }).toThrow("File needs to be less than 5 mb")
  })
  it("should throw if less than the specified maxFileSize", () => {
    expect(() => {
      const file: File = {
        ...createFile(),
        size: bytes.parse("11mb"),
      }
      validateFile(file, {
        uploadFileType: presets.pdf,
        maxFileSize: "10mb",
      })
    }).toThrow("File needs to be less than 10 mb")
  })
  it.todo("return the File if it's valid")
  it("should throw if the incorrect mimeType", () => {
    expect(() => {
      const file: File = {
        ...createFile(),
      }
      validateFile(file, {
        uploadFileType: presets.pdf,
      })
    }).toThrow("File needs to be .pdf")
  })
  it("should throw if the incorrect mimeTypes (2)", () => {
    expect(() => {
      const file: File = {
        ...createFile(),
      }
      validateFile(file, {
        uploadFileType: presets.doc,
      })
    }).toThrow("File needs to be .doc or .docx")
  })
  it("should throw if the incorrect mimeTypes (>2)", () => {
    expect(() => {
      const file: File = {
        ...createFile(),
      }
      validateFile(file, {
        uploadFileType: composeUploadFileTypes(presets.pdf, presets.doc, presets.image),
      })
    }).toThrow("File needs to be .pdf, .doc, .docx, .png, .jpeg, or .jpg")
  })
})
