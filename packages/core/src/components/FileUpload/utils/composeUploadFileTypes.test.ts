import { UploadFileType } from "."
import { composeUploadFileTypes } from "./composeUploadFileTypes"

describe("utils/composeUploadFileTypes", () => {
  it("should throw if unable to compose", () => {
    expect(() => {
      composeUploadFileTypes()
    }).toThrow("Needs at least one argument to be provided.")
  })
  it("should compose one file types", () => {
    const sample: UploadFileType = {
      extensions: ["pdf"],
      mimeTypes: ["test/pdf"],
    }
    const expected = sample
    const actual = composeUploadFileTypes(sample)
    expect(actual).toEqual(expected)
  })
  it("should compose two file types", () => {
    const samples: UploadFileType[] = [
      {
        extensions: ["pdf"],
        mimeTypes: ["test/pdf"],
      },
      {
        extensions: ["png"],
        mimeTypes: ["test/png"],
      },
    ]
    const expected: UploadFileType = {
      extensions: ["pdf", "png"],
      mimeTypes: ["test/pdf", "test/png"],
    }
    const actual = composeUploadFileTypes(...samples)
    expect(actual).toEqual(expected)
  })

  it("should compose two similar file types set-wise", () => {
    const samples: UploadFileType[] = [
      {
        extensions: ["pdf", "doc"],
        mimeTypes: ["test/pdf", "test/doc"],
      },
      {
        extensions: ["docx", "doc"],
        mimeTypes: ["test/pdf", "test/docx"],
      },
    ]
    const expected: UploadFileType = {
      extensions: ["pdf", "doc", "docx"],
      mimeTypes: ["test/pdf", "test/docx", "test/doc"],
    }
    const actual = composeUploadFileTypes(...samples)
    expect(actual.extensions.sort()).toEqual(expected.extensions.sort())
    expect(actual.mimeTypes.sort()).toEqual(expected.mimeTypes.sort())
  })
  it("should compose three unique file types", () => {
    const samples: UploadFileType[] = [
      {
        extensions: ["pdf"],
        mimeTypes: ["test/pdf"],
      },
      {
        extensions: ["docx", "doc"],
        mimeTypes: ["test/doc", "test/docx"],
      },
      {
        extensions: ["png"],
        mimeTypes: ["test/png"],
      },
    ]
    const expected: UploadFileType = {
      extensions: ["pdf", "doc", "docx", "png"],
      mimeTypes: ["test/pdf", "test/docx", "test/doc", "test/png"],
    }
    const actual = composeUploadFileTypes(...samples)
    expect(actual.extensions.sort()).toEqual(expected.extensions.sort())
    expect(actual.mimeTypes.sort()).toEqual(expected.mimeTypes.sort())
  })
  it("should compose three similar file types set-wise", () => {
    const samples: UploadFileType[] = [
      {
        extensions: ["pdf", "doc"],
        mimeTypes: ["test/pdf", "test/doc"],
      },
      {
        extensions: ["docx", "doc"],
        mimeTypes: ["test/pdf", "test/docx"],
      },
      {
        extensions: ["pdf", "png"],
        mimeTypes: ["test/png", "test/docx"],
      },
    ]
    const expected: UploadFileType = {
      extensions: ["pdf", "doc", "docx", "png"],
      mimeTypes: ["test/pdf", "test/docx", "test/doc", "test/png"],
    }
    const actual = composeUploadFileTypes(...samples)
    expect(actual.extensions.sort()).toEqual(expected.extensions.sort())
    expect(actual.mimeTypes.sort()).toEqual(expected.mimeTypes.sort())
  })
})
