export { composeUploadFileTypes } from "./composeUploadFileTypes"
export { presets } from "./presets"
export { validateFile } from "./validateFile"
export { getImagePreviewUrl } from "./getImagePreviewUrl"

export interface UploadFileType {
  extensions: string[]
  mimeTypes: string[]
}
