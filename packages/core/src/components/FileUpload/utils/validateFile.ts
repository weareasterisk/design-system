import bytes from "bytes"

import { UploadFileType } from "."

interface ValidateFileOpts {
  uploadFileType: UploadFileType
  maxFileSize?: string // defaults to 5mb
}

/**
 * A method to assist with client-side validation of files to be uploaded.
 *
 * @param file - File to be validated.
 * @param opts - Criteria for validation.
 * @returns A valid File
 */
export const validateFile = (file: File, opts: ValidateFileOpts): File => {
  const maxFileSizeInBytes = bytes.parse(opts.maxFileSize ?? "5mb")
  const maxFileSizeInMb = bytes(maxFileSizeInBytes, {
    unit: "mb",
    unitSeparator: " ",
    decimalPlaces: 1,
  })

  if (file.size > maxFileSizeInBytes) {
    throw new Error(`File needs to be less than ${maxFileSizeInMb}.`)
  }

  // Caveat: The MIME type is detected from the file extension and can be fooled or spoofed.
  // One can rename a .jpg to a .png and the MIME type will be be reported as image/png.
  // TODO: add real MIME type detection by inspecting the first few (four) bytes of the givin file for the appropriate signature
  // https://stackoverflow.com/questions/18299806/how-to-check-file-mime-type-with-javascript-before-upload
  // TODO: use `file-type` package: https://www.npmjs.com/package/file-type
  // ^ Caveat: The following file types will not be accepted by lib: doc, xls, ppt, msi, csv, svg (maybe use `is-svg` package), we will need to implement a fallback
  // ^ Caveat 2: will change this method to be async, will introduce breaking changes

  if (!opts.uploadFileType.mimeTypes.includes(file.type)) {
    const seriesOfValidExtensions = opts.uploadFileType.extensions.reduce(
      (draft, extension, index, array) => {
        const isLastExtension = index === array.length - 1
        const isFirst = index === 0
        const hasTwo = array.length === 2
        if (isFirst) {
          return `.${extension}`
        } else {
          const showComma = hasTwo ? "" : ","
          return isLastExtension
            ? `${draft}${showComma} or .${extension}`
            : `${draft}, .${extension}`
        }
      },
      ""
    )
    throw new Error(`File needs to be ${seriesOfValidExtensions}`)
  }

  return file
}
