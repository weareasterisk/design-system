// This import style is a-okay here because this file is only used
// by storybook.
import {
  composeUploadFileTypes,
  Dropzone,
  FileItem,
  FileUploadProvider,
  getImagePreviewUrl,
  presets,
  SmallDropzone,
  useDropzone,
  useFileUpload,
  validateFile,
} from "@weareasterisk/hyphen-ui-core"
import * as faker from "faker"
import React from "react"

const DebugInfo = () => {
  const {
    detachFile,
    attachFile,
    selectAllFiles,
    selectValidFiles,
    selectFilesWithErrors,
  } = useFileUpload()

  const validFiles = selectValidFiles().map((fileRecord) => {
    return (
      <FileItem
        key={fileRecord.file.name}
        filename={fileRecord.file.name}
        errorMessage={fileRecord.error}
        hasError={!!fileRecord.error}
        onRemove={detachFile}
      />
    )
  })

  const filesWithError = selectFilesWithErrors().map((fileRecord) => {
    return (
      <FileItem
        key={fileRecord.file.name}
        filename={fileRecord.file.name}
        errorMessage={fileRecord.error}
        hasError={!!fileRecord.error}
        onRemove={detachFile}
      />
    )
  })

  return (
    <div
      style={{
        display: "grid",
        gridTemplateColumns: "1fr 1fr",
        gridGap: 8,
      }}
    >
      <Column title="Invalid Files">
        <div style={{ display: "grid", gridGap: 8 }}>{filesWithError}</div>
      </Column>
      <Column title="Valid Files">
        <div style={{ display: "grid", gridGap: 8 }}>{validFiles}</div>
      </Column>
    </div>
  )
}

const Column: React.FC<any> = ({ children, title }) => {
  return (
    <section
      style={{
        display: "grid",
        gridGap: "4px",
        alignContent: "flex-start",
        // justifyContent: "flex-start"
      }}
    >
      <header
        style={{
          background: "#434343",
          color: "white",
          fontSize: "18px",
          padding: "4px 8px",
          borderRadius: "4px",
        }}
      >
        {title}
      </header>
      <div>{children}</div>
    </section>
  )
}

export const AllowMultipleImages = (): JSX.Element => {
  return (
    <FileUploadProvider allowMultiple={true} fileUploadType={presets.image}>
      <div style={{ display: "grid", gridGap: "8px", paddingBottom: "32px" }}>
        <Dropzone variant="image" />
        <SmallDropzone variant="image" />
        <DebugInfo />
      </div>
    </FileUploadProvider>
  )
}

export const AllowMultipleDocsAndPdfs = (): JSX.Element => {
  return (
    <FileUploadProvider
      allowMultiple={true}
      fileUploadType={composeUploadFileTypes(presets.doc, presets.pdf)}
    >
      <div style={{ display: "grid", gridGap: "8px", paddingBottom: "32px" }}>
        <Dropzone variant="file" />
        <SmallDropzone variant="file" />
        <DebugInfo />
      </div>
    </FileUploadProvider>
  )
}

export const AllowMultipleCustom = (): JSX.Element => {
  return (
    <FileUploadProvider
      allowMultiple={true}
      fileUploadType={composeUploadFileTypes(presets.doc, presets.pdf, presets.image)}
    >
      <div style={{ display: "grid", gridGap: "8px", paddingBottom: "32px" }}>
        <Dropzone variant="simple" />
        <SmallDropzone variant="simple" />
        <DebugInfo />
      </div>
    </FileUploadProvider>
  )
}

export const AllowSingleImage = (): JSX.Element => {
  return (
    <FileUploadProvider allowMultiple={false} fileUploadType={presets.image}>
      <div style={{ display: "grid", gridGap: "8px", paddingBottom: "32px" }}>
        <Dropzone variant="image" />
        <SmallDropzone variant="image" />
        <DebugInfo />
      </div>
    </FileUploadProvider>
  )
}

export const AllowSingleDocsAndPdf = (): JSX.Element => {
  return (
    <FileUploadProvider
      allowMultiple={false}
      fileUploadType={composeUploadFileTypes(presets.doc, presets.pdf)}
    >
      <div style={{ display: "grid", gridGap: "8px", paddingBottom: "32px" }}>
        <Dropzone variant="file" />
        <SmallDropzone variant="file" />
        <DebugInfo />
      </div>
    </FileUploadProvider>
  )
}

export const AllowSingleCustom = (): JSX.Element => {
  return (
    <FileUploadProvider
      allowMultiple={false}
      fileUploadType={composeUploadFileTypes(presets.doc, presets.pdf, presets.image)}
    >
      <div style={{ display: "grid", gridGap: "8px", paddingBottom: "32px" }}>
        <Dropzone variant="simple" />
        <SmallDropzone variant="simple" />
        <DebugInfo />
      </div>
    </FileUploadProvider>
  )
}
