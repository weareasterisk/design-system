import {
  DocumentUpload,
  DocumentUploadOutline,
  ImageUpload,
  ImageUploadOutline,
} from "@weareasterisk/hyphen-ui-icons"
import React from "react"

interface UploadIconProps {
  variant?: "single" | "multiple"
}

export const UploadDocumentIcon: React.FC<UploadIconProps> = ({ variant = "single" }) => {
  return variant === "multiple" ? <DocumentUploadOutline /> : <DocumentUpload />
}

export const UploadImageIcon: React.FC<UploadIconProps> = ({ variant = "single" }) => {
  return variant === "multiple" ? <ImageUploadOutline /> : <ImageUpload />
}
