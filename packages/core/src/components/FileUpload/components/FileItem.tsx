import { AttachOutline, SlashOutline, Trash2Outline } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React, { useCallback } from "react"

import { Classes as classes } from "../../../common"

interface FileItemProps {
  onRemove: (filename: string) => void
  filename: string
  errorMessage?: string
  hasError?: boolean
}

export const FileItem: React.FC<FileItemProps> = ({
  filename,
  onRemove,
  errorMessage,
  hasError,
}) => {
  const handleRemove = useCallback(
    (e) => {
      e.preventDefault()
      e.stopPropagation()
      onRemove(filename)
    },
    [filename, onRemove]
  )

  const fileItemClass = classNames(classes.FILE_ITEM, { [classes.FILE_ITEM_ERROR]: hasError })
  const fileItemErrorMessage = classNames(classes.FILE_ITEM_ERROR_MESSAGE)
  const fileItemFilenameClass = classNames(classes.FILE_ITEM_FILENAME)
  const fileItemInfoClass = classNames(classes.FILE_ITEM_INFO)
  const fileItemLogoClass = classNames(classes.FILE_ITEM_LOGO, {
    [classes.FILE_ITEM_LOGO_ERROR]: hasError,
  })

  return (
    <div className={fileItemClass}>
      <div className={fileItemInfoClass}>
        <span className={fileItemLogoClass}>{hasError ? <SlashOutline /> : <AttachOutline />}</span>
        <p className={fileItemFilenameClass}>{filename}</p>
        {/* TODO: Use `IconButton` component once implemented */}
        {/* <Button plainDanger onClick={handleRemove}>

        </Button> */}
        <button onClick={handleRemove} className={fileItemLogoClass}>
          <Trash2Outline />
        </button>
      </div>
      {hasError && errorMessage && (
        <div className={fileItemErrorMessage}>
          <p>{errorMessage}</p>
        </div>
      )}
    </div>
  )
}
