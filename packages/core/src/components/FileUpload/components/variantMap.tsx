import { FileOutline, ImageOutline, UploadOutline } from "@weareasterisk/hyphen-ui-icons"
import React from "react"

import { UploadDocumentIcon, UploadImageIcon } from "./icons"

export const smallDropzoneVariantMap = (
  multiple = false
): { [index: string]: { icon: React.ReactNode; instructions: string } } => {
  const s = multiple ? "s" : ""
  return {
    file: {
      icon: <FileOutline />,
      instructions: `Drag and drop file${s} here or click to upload`,
    },
    image: {
      icon: <ImageOutline />,
      instructions: `Drag and drop image${s} here or click to upload`,
    },
    simple: {
      icon: <UploadOutline />,
      instructions: `Drop to upload`,
    },
  }
}

export const dropzoneVariantMap = (
  multiple = false
): { [index: string]: { icon: React.ReactNode; instructions: string; replace: string } } => {
  const variant = multiple ? "multiple" : "single"
  const s = multiple ? "s" : ""
  return {
    file: {
      icon: <UploadDocumentIcon variant={variant} />,
      instructions: `Drag and drop file${s} here or click to upload`,
      replace: `Drag and drop or click to replace file`,
    },
    image: {
      icon: <UploadImageIcon variant={variant} />,
      instructions: `Drag and drop image${s} here or click to upload`,
      replace: `Drag and drop or click to replace image`,
    },
    simple: {
      icon: <UploadOutline />,
      instructions: `Drop to upload`,
      replace: `Drag and drop item${s} here or click to replace`,
    },
  }
}
