import { AttachOutline, SlashOutline, Trash2Outline } from "@weareasterisk/hyphen-ui-icons"
import classNames from "classnames"
import React, { useCallback } from "react"

import { Classes } from "../../../common"
import { useDropzone } from "../hooks/useDropzone"
import { useFileUpload } from "../hooks/useFileUpload"
import { DropzoneProps } from "./Dropzone"
import { smallDropzoneVariantMap } from "./variantMap"

const getAbbreviatedFilename = (filename: string) => {
  const prefix = filename.slice(0, 8)
  const suffix = filename.slice(filename.length - 6, filename.length)
  return `${prefix}...${suffix}`
}

export const SmallDropzone: React.FC<DropzoneProps> = (props) => {
  const {
    fileInputRef,
    handleChange,
    openFileDialog,
    handleOnDragLeave,
    handleOnDragOver,
    handleOnDrop,
    isFileOver,
    allowMultiple,
    accept,
  } = useDropzone()

  const { detachFile, selectAllFiles } = useFileUpload()

  const foundFile = selectAllFiles()[0]
  const hasFile = !!foundFile && !allowMultiple

  const variant = props.variant ?? "simple"

  const smallDropzoneClass = classNames(Classes.SMALL_DROPZONE, {
    [Classes.SMALL_DROPZONE_DRAG]: isFileOver,
  })
  const smallDropzoneIconClass = classNames(Classes.SMALL_DROPZONE_ICON, {
    [Classes.SMALL_DROPZONE_ICON_DRAG]: isFileOver,
    [Classes.SMALL_DROPZONE_ICON_ERROR]: !!foundFile?.error,
  })
  const smallDropzoneInputClass = classNames(Classes.SMALL_DROPZONE_INPUT)
  const smallDropzoneInstructionsClass = classNames(Classes.SMALL_DROPZONE_INSTRUCTIONS)

  // TODO: Fix exhaustive deps issue
  const handleClick = useCallback(
    hasFile ? () => detachFile(foundFile.file.name) : openFileDialog,
    [hasFile, detachFile, openFileDialog]
  )
  const variantMap = smallDropzoneVariantMap(allowMultiple)

  return (
    <div
      className={smallDropzoneClass}
      onClick={handleClick}
      onDragOver={handleOnDragOver}
      onDragLeave={handleOnDragLeave}
      onDrop={handleOnDrop}
      role="presentation"
    >
      <div className={smallDropzoneIconClass}>
        {!hasFile ? (
          variantMap[variant].icon
        ) : foundFile.error ? (
          <SlashOutline />
        ) : (
          <AttachOutline />
        )}
      </div>
      <p className={smallDropzoneInstructionsClass}>
        {!hasFile ? variantMap[variant].instructions : getAbbreviatedFilename(foundFile.file.name)}
      </p>
      {hasFile && (
        <div className={smallDropzoneIconClass}>
          <Trash2Outline />
        </div>
      )}
      <input
        type="file"
        multiple={props.allowMultiple ?? allowMultiple}
        className={smallDropzoneInputClass}
        ref={fileInputRef}
        accept={props.accept ?? accept}
        onChange={handleChange}
      />
    </div>
  )
}
