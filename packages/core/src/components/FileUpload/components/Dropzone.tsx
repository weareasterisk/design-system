import classNames from "classnames"
import React from "react"

import { Classes as classes } from "../../../common"
import { useDropzone } from "../hooks/useDropzone"
import { useFileUpload } from "../hooks/useFileUpload"
import { FileItem } from "./FileItem"
import { dropzoneVariantMap } from "./variantMap"

type Variant = "file" | "image" | "simple"

export interface DropzoneProps {
  accept?: string // This is an override, will usually be derived from context
  allowMultiple?: boolean // This is an override, will usually be derived from context
  variant?: Variant
}

export const Dropzone: React.FC<DropzoneProps> = (props) => {
  const {
    fileInputRef,
    handleChange,
    openFileDialog,
    handleOnDragLeave,
    handleOnDragOver,
    handleOnDrop,
    isFileOver,
    allowMultiple,
    accept,
  } = useDropzone()

  const { selectAllFiles, detachFile } = useFileUpload()

  const files = selectAllFiles()
  const foundFile = files[0]

  const variant = props.variant ?? "simple"

  const dropzoneClass = classNames(classes.DROPZONE, {
    [classes.DROPZONE_DRAG]: isFileOver,
    [classes.DROPZONE_HAS_FILE]: !!foundFile && !allowMultiple,
  })

  const dropzoneIconClass = classNames(classes.DROPZONE_ICON, {
    [classes.DROPZONE_ICON_DRAG]: isFileOver,
    [classes.DROPZONE_ICON_SIMPLE]: variant === "simple",
  })

  const dropzoneInputClass = classNames(classes.DROPZONE_INPUT)
  const dropzoneInstructionsClass = classNames(classes.DROPZONE_INSTRUCTIONS, {
    [classes.DROPZONE_INSTRUCTIONS_SIMPLE]: variant === "simple",
    [classes.DROPZONE_INSTRUCTIONS_HAS_FILE]: !!foundFile && !allowMultiple,
  })

  const variantMap = dropzoneVariantMap(allowMultiple)

  return (
    <div
      className={dropzoneClass}
      onClick={openFileDialog}
      onDragOver={handleOnDragOver}
      onDragLeave={handleOnDragLeave}
      onDrop={handleOnDrop}
      role="presentation"
    >
      {!allowMultiple && !!foundFile ? (
        <React.Fragment>
          <FileItem
            filename={foundFile.file.name}
            onRemove={detachFile}
            hasError={!!foundFile.error}
            errorMessage={foundFile.error}
          />
          <p className={dropzoneInstructionsClass}>{variantMap[variant].replace}</p>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div className={dropzoneIconClass}>{variantMap[variant].icon}</div>
          <p className={dropzoneInstructionsClass}>{variantMap[variant].instructions}</p>
        </React.Fragment>
      )}
      {/* TODO: emit onFocus / onBlur for focus style on root Dropzone node for a11y */}
      <input
        type="file"
        multiple={props.allowMultiple ?? allowMultiple}
        className={dropzoneInputClass}
        ref={fileInputRef}
        accept={props.accept ?? accept}
        onChange={handleChange}
      />
    </div>
  )
}
