import { useCallback, useContext } from "react"

import { FileUploadContext } from "../context/FileUploadContext"
import { FileRecord } from "../context/fileUploadReducer"

export const useFileUpload = () => {
  const [state, dispatch] = useContext(FileUploadContext)

  // action creators

  const attachFile = useCallback(
    (file: File) =>
      dispatch({
        type: "AttachFile",
        file,
      }),
    [dispatch]
  )

  const detachFile = useCallback(
    (filename: string) =>
      dispatch({
        type: "DetachFile",
        filename,
      }),
    [dispatch]
  )

  const clearAllFiles = useCallback(
    () =>
      dispatch({
        type: "ClearAllFiles",
      }),
    [dispatch]
  )

  // selectors

  const selectOptions = useCallback(() => state.options, [state.options])
  const selectFileByName = useCallback((filename) => state.files[filename], [state.files])
  const selectAllFiles = useCallback(
    (): FileRecord[] => Object.keys(state.files).map((filename) => state.files[filename]),
    [state.files]
  )
  const selectFilesWithErrors = useCallback(
    () => selectAllFiles().filter((fileRecord) => fileRecord?.error),
    [state.files]
  )
  const selectValidFiles = useCallback(
    () => selectAllFiles().filter((fileRecord) => !fileRecord?.error),
    [state.files]
  )

  return {
    attachFile,
    detachFile,
    clearAllFiles,
    selectFileByName,
    selectAllFiles,
    selectOptions,
    selectFilesWithErrors,
    selectValidFiles,
  }
}
