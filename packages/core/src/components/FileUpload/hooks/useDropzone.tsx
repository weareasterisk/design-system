import React, { ChangeEvent, DragEvent, useCallback, useMemo, useRef, useState } from "react"

import { useFileUpload } from "./useFileUpload"

export const useDropzone = () => {
  const { attachFile, selectOptions } = useFileUpload()
  const [isFileOver, setIsFileOver] = useState<boolean>()

  const fileInputRef = useRef<HTMLInputElement>(null)

  const handleOnDrop = useCallback(
    (e: DragEvent<HTMLDivElement>): void => {
      e.preventDefault()
      const filesToAttach = Array.from<File>(e.dataTransfer.files || [])
      filesToAttach.forEach(attachFile)
      setIsFileOver(false)
    },
    [attachFile, setIsFileOver]
  )

  const handleOnDragOver = useCallback(
    (e: React.DragEvent<HTMLDivElement>): void => {
      e.preventDefault()
      setIsFileOver(true)
    },
    [setIsFileOver]
  )

  const handleOnDragLeave = useCallback((): void => {
    setIsFileOver(false)
  }, [setIsFileOver])

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const filesToAttach = Array.from<File>(e.target.files || [])
      filesToAttach.forEach(attachFile)
      if (fileInputRef?.current?.value) {
        fileInputRef.current.value = ""
      }
    },
    [fileInputRef, attachFile]
  )

  const openFileDialog = useCallback(() => {
    fileInputRef?.current?.click()
  }, [fileInputRef])

  const { allowMultiple, fileUploadType } = selectOptions()

  const accept = useMemo(() => fileUploadType.extensions.map((ext) => `.${ext}`).join(","), [
    fileUploadType,
  ])

  return {
    fileInputRef,
    handleChange,
    handleOnDragLeave,
    handleOnDragOver,
    handleOnDrop,
    isFileOver,
    openFileDialog,
    allowMultiple,
    accept,
  }
}
