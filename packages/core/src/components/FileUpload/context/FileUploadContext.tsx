import React, { createContext, useReducer } from "react"

import { UploadFileType } from "../utils"
import { Action, defaultState, fileUploadReducer, FileUploadState } from "./fileUploadReducer"

export const FileUploadContext = createContext<[FileUploadState, React.Dispatch<Action>]>([
  defaultState,
  (value: Action) => value,
])

export interface FileUploadProviderProps {
  fileUploadType: UploadFileType
  allowMultiple?: boolean
}

export const FileUploadProvider: React.FC<FileUploadProviderProps> = ({
  children,
  allowMultiple = false,
  fileUploadType,
}) => {
  const initState: FileUploadState = {
    ...defaultState,
    options: {
      allowMultiple: allowMultiple,
      fileUploadType,
    },
  }
  const store = useReducer(fileUploadReducer, initState)
  return <FileUploadContext.Provider value={store}>{children}</FileUploadContext.Provider>
}
