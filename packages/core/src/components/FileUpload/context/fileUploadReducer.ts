import { presets, UploadFileType, validateFile } from "../utils"

export type FileRecord = {
  file: File
  error?: string
}

export interface FileMap {
  [filename: string]: FileRecord
}

export type FileUploadState = {
  options: {
    fileUploadType: UploadFileType
    allowMultiple: boolean
    maxFileSize?: string
  }
  files: FileMap
}

export const defaultState: FileUploadState = {
  files: {},
  options: {
    fileUploadType: presets.pdf,
    allowMultiple: false,
  },
}

export type Action =
  | {
      type: "AttachFile"
      file: File
    }
  | {
      type: "DetachFile"
      filename: string
    }
  | {
      type: "ClearAllFiles"
    }

export type FileUploadReducer = (state: FileUploadState, action: Action) => FileUploadState

export const fileUploadReducer: FileUploadReducer = (state = defaultState, action: Action) => {
  switch (action.type) {
    case "AttachFile": {
      // validate (non-blocking)
      // TODO: should we uncatch the throw?

      let error
      try {
        validateFile(action.file, {
          uploadFileType: state.options.fileUploadType,
          maxFileSize: state.options.maxFileSize,
        })
      } catch (err) {
        error = err.message
      }

      // apply

      if (state.options.allowMultiple) {
        return {
          ...state,
          files: {
            ...state.files,
            [action.file.name]: {
              file: action.file,
              error,
            },
          },
        }
      } else {
        return {
          ...state,
          files: {
            [action.file.name]: {
              file: action.file,
              error,
            },
          },
        }
      }
    }
    case "DetachFile": {
      const draft = { ...state } // TODO: need to deep copy?
      delete draft.files[action.filename] // TODO: refactor since this is a mutation?
      return { ...draft }
    }
    case "ClearAllFiles":
      return {
        options: state.options,
        files: {},
      }
    default:
      return state
  }
}
