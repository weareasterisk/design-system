import { CKEditor } from "@ckeditor/ckeditor5-react"
import { Editor, MinimalEditor } from "@weareasterisk/hyphen-ui-ckeditor5-config"
import React, { useState } from "react"

export default {
  title: "core/RichTextEditor",
}

export const Primary = (): JSX.Element => {
  const [value, setValue] = useState()

  return (
    <React.Fragment>
      <div style={{ display: "flex", marginLeft: "80px" }}>
        <CKEditor
          editor={Editor}
          data={value}
          onChange={(_event: any, editor: any) => {
            setValue(editor.getData())
          }}
        />
      </div>
    </React.Fragment>
  )
}

export const Minimal = (): JSX.Element => {
  const [value, setValue] = useState()

  return (
    <React.Fragment>
      <CKEditor
        editor={MinimalEditor}
        data={value}
        onChange={(_event: any, editor: any) => {
          setValue(editor.getData())
        }}
      />
    </React.Fragment>
  )
}

// export const Playground = (): JSX.Element => {
//   return <React.Fragment></React.Fragment>
// }
