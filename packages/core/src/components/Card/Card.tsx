import React from "react"

import { generateDisplayName } from "../../common/props"

const Card: React.FC = () => {
  return <div className={"card-root"}>Hi</div>
}

Card.displayName = generateDisplayName("Card")
export default React.memo(Card)
