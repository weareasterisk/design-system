import rollupConfig from "../../rollup.config.base"

module.exports = Object.assign({}, rollupConfig, {
  input: "./src/index.ts",
})
