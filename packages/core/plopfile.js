module.exports = (plop) => {
  plop.setGenerator("component", {
    description: "This will generate a skeleton component",
    prompts: [
      {
        type: "input",
        name: "name",
        message: "Enter the name of your component: ",
      },
    ],
    actions: [
      {
        type: "add",
        path: "src/components/{{pascalCase name}}/_{{dashCase name}}.scss",
        templateFile: "_templates/Component/_component.scss.hbs",
      },
      {
        type: "add",
        path: "src/components/{{pascalCase name}}/{{pascalCase name}}.mdx",
        templateFile: "_templates/Component/component.mdx.hbs",
      },
      {
        type: "add",
        path: "src/components/{{pascalCase name}}/{{pascalCase name}}.stories.tsx",
        templateFile: "_templates/Component/component.stories.tsx.hbs",
      },
      {
        type: "add",
        path: "src/components/{{pascalCase name}}/{{pascalCase name}}.tsx",
        templateFile: "_templates/Component/component.tsx.hbs",
      },
      {
        type: "add",
        path: "src/components/{{pascalCase name}}/index.tsx",
        templateFile: "_templates/Component/index.tsx.hbs",
      },
      {
        type: "append",
        path: "src/components/_index.scss",
        template: `@import "./{{pascalCase name}}/{{dashCase name}}\n";`,
      },
      {
        type: "append",
        path: "src/common/classes.ts",
        template:
          "// {{pascalCase name}} Classes\nexport const {{constantCase name}} = `${NS}-{{dashCase name}}`\n",
      },
      {
        type: "append",
        path: "src/components/index.ts",
        template: 'export * from "./{{pascalCase name}}"\n',
      },
    ],
  })
}
