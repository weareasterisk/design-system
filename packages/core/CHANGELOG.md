# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.20.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.19.0...@weareasterisk/hyphen-ui-core@1.20.0) (2021-05-17)


### Features

* add additional options for images in ckeditor and update deps ([5961f54](https://gitlab.com/weareasterisk/design-system/commit/5961f54e578531afee0cbeef386c7cd3bdb0a38c))





# [1.19.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.18.2...@weareasterisk/hyphen-ui-core@1.19.0) (2021-01-09)


### Bug Fixes

* fix modal heading and footer padding ([bc6e466](https://gitlab.com/weareasterisk/design-system/commit/bc6e466c1faa6bfe377875ea07d3c023f350f383))


### Features

* add prop in divider to render without margins ([d875933](https://gitlab.com/weareasterisk/design-system/commit/d875933616cac6becf338ccac30d79257def5fa3))





## [1.18.2](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.18.1...@weareasterisk/hyphen-ui-core@1.18.2) (2021-01-08)


### Bug Fixes

* rich text editor list indentation styles ([f853e7f](https://gitlab.com/weareasterisk/design-system/commit/f853e7ff8cf6aceb6254b3fe484abb4eee9a7983))





## [1.18.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.18.0...@weareasterisk/hyphen-ui-core@1.18.1) (2021-01-03)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-core





# [1.18.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.17.0...@weareasterisk/hyphen-ui-core@1.18.0) (2020-12-31)


### Features

* hide footer if there is no onConfirm ([388dc7f](https://gitlab.com/weareasterisk/design-system/commit/388dc7ff4205fdaacb87e20e05f7ec7e29ab500f))





# [1.17.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.16.0...@weareasterisk/hyphen-ui-core@1.17.0) (2020-12-21)


### Bug Fixes

* actually implement the action trigger on text fields ([3d07a3b](https://gitlab.com/weareasterisk/design-system/commit/3d07a3b584a2df10a8dffc80e280e532d9da2d83))
* asterisk in required text area/fields is now colored ([3332214](https://gitlab.com/weareasterisk/design-system/commit/333221451670c9a325f0e0ed227bb69e55593ff9))
* modal styling now passes class props and positions modal above most elements ([ecfb5c8](https://gitlab.com/weareasterisk/design-system/commit/ecfb5c82b410fc13aee8e9d2fa8cdfc01a33cc8e))
* remove extra margin in button ([a92d336](https://gitlab.com/weareasterisk/design-system/commit/a92d3368abc28aea51c3aeaa264520b3cde9b34e))
* set modal height to auto for adaptability: ([e40430d](https://gitlab.com/weareasterisk/design-system/commit/e40430de941cd40288c9c4b72b70ba41529663b4))


### Features

* create date picker input ([c1173ef](https://gitlab.com/weareasterisk/design-system/commit/c1173ef23ad516ca9bdbe7cb65682687ec2154eb))
* date picker calendar using react-calender and overridden styles with default props ([a524a07](https://gitlab.com/weareasterisk/design-system/commit/a524a07eebe099980e73b7c3468f02a021f747bf))





# [1.16.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.15.5...@weareasterisk/hyphen-ui-core@1.16.0) (2020-12-01)


### Features

* change content type ([e958ba6](https://gitlab.com/weareasterisk/design-system/commit/e958ba60284458a44b0d5c7192c9cc72d4884f1c))





## [1.15.5](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.15.4...@weareasterisk/hyphen-ui-core@1.15.5) (2020-11-28)


### Bug Fixes

* add export of IconButton ([dbaf1c4](https://gitlab.com/weareasterisk/design-system/commit/dbaf1c46bf0cdefea0183b83cdf75ba04e4ea755))





## [1.15.4](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.15.3...@weareasterisk/hyphen-ui-core@1.15.4) (2020-11-17)


### Bug Fixes

* error message rendering order ([c826e08](https://gitlab.com/weareasterisk/design-system/commit/c826e08a95f777903f6d663e8cc872ef2e26ca10))





## [1.15.3](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.15.2...@weareasterisk/hyphen-ui-core@1.15.3) (2020-11-16)


### Bug Fixes

* added render function to draggablelist ([80e8ac4](https://gitlab.com/weareasterisk/design-system/commit/80e8ac4670be61b3af8cf8e8fab72146659a3c9a))





## [1.15.2](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.15.1...@weareasterisk/hyphen-ui-core@1.15.2) (2020-11-16)


### Bug Fixes

* added useEffect to update list if items change ([f5a8329](https://gitlab.com/weareasterisk/design-system/commit/f5a83295df63965161848de968cb857be6fcc9c3))





## [1.15.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.15.0...@weareasterisk/hyphen-ui-core@1.15.1) (2020-11-15)


### Bug Fixes

* export pointer ([465d76d](https://gitlab.com/weareasterisk/design-system/commit/465d76d05817a4b4b2a30f4df887b3b6c765ded3))
* import icon from package properly ([2c46e44](https://gitlab.com/weareasterisk/design-system/commit/2c46e44834185e8b1b2504edc25168e7c37b0d28))





# [1.15.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.14.0...@weareasterisk/hyphen-ui-core@1.15.0) (2020-11-10)


### Features

* mr fixes including onchange prop ([8bda204](https://gitlab.com/weareasterisk/design-system/commit/8bda204618a886f96e18d81e2adc4ae9a3151fd1))





# [1.14.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.13.1...@weareasterisk/hyphen-ui-core@1.14.0) (2020-10-20)


### Features

* added icon buttons ([f0f9f0e](https://gitlab.com/weareasterisk/design-system/commit/f0f9f0e27015589f3a85ac1c0258d3fa386fff40))





## [1.13.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.13.0...@weareasterisk/hyphen-ui-core@1.13.1) (2020-10-20)


### Bug Fixes

* ensure modal can accept children ([d7a14fb](https://gitlab.com/weareasterisk/design-system/commit/d7a14fb5e777dafa986c5e90a59058a2e3657774))





# [1.13.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.12.0...@weareasterisk/hyphen-ui-core@1.13.0) (2020-10-20)


### Bug Fixes

* **grey:** grey is not okay ([7d8fb0d](https://gitlab.com/weareasterisk/design-system/commit/7d8fb0d9d2caa0bf33b25974c751025474f49b5b))
* **link:** changes from code review ([b44b2b7](https://gitlab.com/weareasterisk/design-system/commit/b44b2b777ec3c37382e98f114bb3e4902976e1aa))


### Features

* **link:** add the link component ([5137fa3](https://gitlab.com/weareasterisk/design-system/commit/5137fa304fd42def3baae0269e0eb9137b51a08e))
* **links:** add files for skeleton ([0eb6cac](https://gitlab.com/weareasterisk/design-system/commit/0eb6cacce39a31d9a6b92708799ca945fc1170b5))





# [1.12.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.11.0...@weareasterisk/hyphen-ui-core@1.12.0) (2020-10-10)


### Bug Fixes

* **code-review:** code review change to not use _ ([127c2a0](https://gitlab.com/weareasterisk/design-system/commit/127c2a028897302c599d1e92c1ff3a6c3c8c0551))
* **code-review:** code review changes ([40792ea](https://gitlab.com/weareasterisk/design-system/commit/40792eadf1d5d36a5db30113b312ad40f679acdc))
* **code-review:** fix spacing around timezones ([e6ee2e2](https://gitlab.com/weareasterisk/design-system/commit/e6ee2e2302d54cfd1cdc7acd1eee7c0454a0156c))


### Features

* **time:** is time real? ([62768be](https://gitlab.com/weareasterisk/design-system/commit/62768be40730d9e6e4ac82945c106b9a52c7019b))
* **timepicker:** add skeleton of Time Picker ([875fef4](https://gitlab.com/weareasterisk/design-system/commit/875fef44abde1130ec2c59178bee24cc023e9c0f))





# [1.11.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.10.0...@weareasterisk/hyphen-ui-core@1.11.0) (2020-10-10)


### Features

* add editor styles ([666d2d3](https://gitlab.com/weareasterisk/design-system/commit/666d2d3809ce42169457a422cf4ad4c993035279))





# [1.10.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.9.0...@weareasterisk/hyphen-ui-core@1.10.0) (2020-10-08)


### Features

* add component generators for noncomplex scaffolding ([9222de6](https://gitlab.com/weareasterisk/design-system/commit/9222de61ec9094e76ba24887f382bb8495923178))
* create Spinner ([14c09f7](https://gitlab.com/weareasterisk/design-system/commit/14c09f7671d08821e763ec220ce2091bbec9f3fe))





# [1.9.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.8.1...@weareasterisk/hyphen-ui-core@1.9.0) (2020-10-06)


### Bug Fixes

* **toggle:** remove unused comment ([5b8e6a9](https://gitlab.com/weareasterisk/design-system/commit/5b8e6a90e299bb14a040ecd913cad3df38a1760c))
* **toggle-label:** fix toggle label being funky ([126f289](https://gitlab.com/weareasterisk/design-system/commit/126f28955d7fc15164d5576f029adf16f57ff42a))


### Features

* **toggle:** implement toggle component ([d482af4](https://gitlab.com/weareasterisk/design-system/commit/d482af4f8990c0b75ed366ef108dc11c75188042))
* **toggle:** implement toggle label\nimplement event handler ([98f579a](https://gitlab.com/weareasterisk/design-system/commit/98f579a0f7949e607adf5f75f1bfe8de287e9ec1))





## [1.8.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.8.0...@weareasterisk/hyphen-ui-core@1.8.1) (2020-10-06)


### Bug Fixes

* remove react-modal as peer dependency ([732deb0](https://gitlab.com/weareasterisk/design-system/commit/732deb0e1b14a33415aa7bd2e5b1e6cb2e5544ae))





# [1.8.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.7.0...@weareasterisk/hyphen-ui-core@1.8.0) (2020-10-05)


### Bug Fixes

* **core/textarea:** fix styling errors in compact state ([09d4ab0](https://gitlab.com/weareasterisk/design-system/commit/09d4ab0d573dde69a58f825cc2a8368afb2732e7))


### Features

* **core/common:** add keycodes constants, typography helpers, and layout helpers ([e63f162](https://gitlab.com/weareasterisk/design-system/commit/e63f162261cbbad7cf662f57b57121b411fb4cd9))
* **core/dropdown:** create common listbox components for dropdown fields, and create dropdown component ([38d6a1e](https://gitlab.com/weareasterisk/design-system/commit/38d6a1e1c40b8e0354b40ff145f04a5ba475d47b))





# [1.7.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.6.1...@weareasterisk/hyphen-ui-core@1.7.0) (2020-09-27)


### Features

* modal component implemented ([7a03789](https://gitlab.com/weareasterisk/design-system/commit/7a0378944dd7ed7d2919ffca0c9ce1cf9ef20933))





## [1.6.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.6.0...@weareasterisk/hyphen-ui-core@1.6.1) (2020-09-24)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-core





# [1.6.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.5.1...@weareasterisk/hyphen-ui-core@1.6.0) (2020-09-22)


### Bug Fixes

* export hooks at top level ([3d7d10f](https://gitlab.com/weareasterisk/design-system/commit/3d7d10fa0ca5c002eaec9a628b30ac680f4f50e2))


### Features

* add a bunch of hooks ([0722c9c](https://gitlab.com/weareasterisk/design-system/commit/0722c9c95cd9eae2a3ecb3ed50c11572b56c7738))





## [1.5.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.5.0...@weareasterisk/hyphen-ui-core@1.5.1) (2020-09-21)


### Bug Fixes

* use react-merge-refs to compose refs in a type-safe manner ([2ec7cc5](https://gitlab.com/weareasterisk/design-system/commit/2ec7cc52276e9bf9a5e5512a54a1098493705217))





# [1.5.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.4.0...@weareasterisk/hyphen-ui-core@1.5.0) (2020-09-21)


### Features

* create base ckeditor config and watchdog with required features and custom toolbar ([6a1259d](https://gitlab.com/weareasterisk/design-system/commit/6a1259d19b4b65bbbdaace1a4c5c3cfae43c89ab))





# [1.4.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.3.0...@weareasterisk/hyphen-ui-core@1.4.0) (2020-09-18)


### Bug Fixes

* error message evaluation logic ([db32e3d](https://gitlab.com/weareasterisk/design-system/commit/db32e3d54468fc1a50caf0e219930f3d800c1fb7))


### Features

* implement text area component ([ace0b20](https://gitlab.com/weareasterisk/design-system/commit/ace0b20ba71c22ec50823def72d2cb0e62b57a17))





# [1.3.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.8...@weareasterisk/hyphen-ui-core@1.3.0) (2020-09-12)


### Features

* add mixin to generate background color with opacity ([35d8083](https://gitlab.com/weareasterisk/design-system/commit/35d8083bf1af84b3fbfeafb23cea753f6f475940))
* outline shadow variable ([11690e4](https://gitlab.com/weareasterisk/design-system/commit/11690e4efa28655277719c801fc3212c9b1aadcb))
* re-create Button component with remaining states and options ([d475014](https://gitlab.com/weareasterisk/design-system/commit/d475014dbeb2fa9a24279e618ca8e41f847b02e4))





## [1.2.8](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.7...@weareasterisk/hyphen-ui-core@1.2.8) (2020-09-05)


### Bug Fixes

* render heading state correctly ([520dd82](https://gitlab.com/weareasterisk/design-system/commit/520dd82e79686126687c130cf1bc3ff4a53ed545))





## [1.2.7](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.6...@weareasterisk/hyphen-ui-core@1.2.7) (2020-09-05)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-core





## [1.2.6](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.5...@weareasterisk/hyphen-ui-core@1.2.6) (2020-09-03)


### Bug Fixes

* properly type forwardref in text and heading ([58a0707](https://gitlab.com/weareasterisk/design-system/commit/58a07076f94f52eb4a7cbdc1b198efff6e662707))





## [1.2.5](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.4...@weareasterisk/hyphen-ui-core@1.2.5) (2020-09-03)


### Bug Fixes

* ref passthrough and component declaration for namespaced headings ([9794361](https://gitlab.com/weareasterisk/design-system/commit/97943611cf44df3f7641d97690a76200b3ef34ad))





## [1.2.4](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.3...@weareasterisk/hyphen-ui-core@1.2.4) (2020-09-02)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-core





## [1.2.3](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.2...@weareasterisk/hyphen-ui-core@1.2.3) (2020-09-02)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-core





## [1.2.2](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.1...@weareasterisk/hyphen-ui-core@1.2.2) (2020-09-02)

**Note:** Version bump only for package @weareasterisk/hyphen-ui-core





## [1.2.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.2.0...@weareasterisk/hyphen-ui-core@1.2.1) (2020-09-02)


### Bug Fixes

* button barelling ([b6a6e26](https://gitlab.com/weareasterisk/design-system/commit/b6a6e2631f0861541eb818740b356fee88a50a22))





# [1.2.0](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.1.2...@weareasterisk/hyphen-ui-core@1.2.0) (2020-08-16)


### Bug Fixes

* fix ref passthrough and props in control ([0cf6807](https://gitlab.com/weareasterisk/design-system/commit/0cf680762efbf443e37b66a186426578a52756d2))
* use box shadow for active outline and fix conditional styling in control + checkbox ([4ab81b5](https://gitlab.com/weareasterisk/design-system/commit/4ab81b516521dbe9fd414d76c21bb11062921922))


### Features

* add divider component ([e6a0279](https://gitlab.com/weareasterisk/design-system/commit/e6a0279f8868876fe6272055155f944b68d53885))
* add missing colours and tokens ([9f5eaec](https://gitlab.com/weareasterisk/design-system/commit/9f5eaec3a68dd77168641559f3d2fd5517000882))
* add passwordfield component to handle password text inputs ([f3102ca](https://gitlab.com/weareasterisk/design-system/commit/f3102caf3f6f0bf4230e0604435bcc40a1514c57))
* create a base input component with common styles ([5bc495b](https://gitlab.com/weareasterisk/design-system/commit/5bc495b6ad6ef88e8503e12399210cff5f45719a))
* create global storybook configuration instead of per-project ([e2c3ccf](https://gitlab.com/weareasterisk/design-system/commit/e2c3ccf29e158a7d7649c9fbdf2553587d063b0e))
* re-create text field component with additional states ([cb92232](https://gitlab.com/weareasterisk/design-system/commit/cb9223239702135b84572f2d08cff2e0d7690c70))
* streamline text styling and scope header and text styles appropriately ([d7160c4](https://gitlab.com/weareasterisk/design-system/commit/d7160c41d442d89ae63506e39fd9e3c47dcd9535))





## [1.1.2](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.1.0...@weareasterisk/hyphen-ui-core@1.1.2) (2020-08-11)


### Bug Fixes

* storybook setup in core ([11771ba](https://gitlab.com/weareasterisk/design-system/commit/11771ba257f47f1be02ddf0e299ab07c4eca9a76))
* textfield icon and fileupload icon placement ([a26915a](https://gitlab.com/weareasterisk/design-system/commit/a26915aaf6f0589d23dff9f2b8c2d2f6c06c3050))
* upload icon styles ([5876d93](https://gitlab.com/weareasterisk/design-system/commit/5876d93f3657534abf4cc3315047f12aae872973))





## [1.1.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphen-ui-core@1.1.0...@weareasterisk/hyphen-ui-core@1.1.1) (2020-08-11)


### Bug Fixes

* storybook setup in core ([11771ba](https://gitlab.com/weareasterisk/design-system/commit/11771ba257f47f1be02ddf0e299ab07c4eca9a76))
* textfield icon and fileupload icon placement ([a26915a](https://gitlab.com/weareasterisk/design-system/commit/a26915aaf6f0589d23dff9f2b8c2d2f6c06c3050))
* upload icon styles ([5876d93](https://gitlab.com/weareasterisk/design-system/commit/5876d93f3657534abf4cc3315047f12aae872973))





# @weareasterisk/hyphen-ui-core 1.0.0 (2020-08-06)


### Features

* init core package from foundations ([8f5ad8e](https://gitlab.com/weareasterisk/design-system/commit/8f5ad8ebbe2c569115be9a68481da604bb2080c5))

## @weareasterisk/hyphenjs-core [1.0.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphenjs-core@1.0.0...@weareasterisk/hyphenjs-core@1.0.1) (2020-08-04)

## @weareasterisk/hyphenjs-core [1.0.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphenjs-core@1.0.0...@weareasterisk/hyphenjs-core@1.0.1) (2020-08-04)

## @weareasterisk/hyphenjs-core [1.0.1](https://gitlab.com/weareasterisk/design-system/compare/@weareasterisk/hyphenjs-core@1.0.0...@weareasterisk/hyphenjs-core@1.0.1) (2020-08-04)

# @weareasterisk/hyphenjs-core 1.0.0 (2020-08-04)


### Features

* init core package from foundations ([8f5ad8e](https://gitlab.com/weareasterisk/design-system/commit/8f5ad8ebbe2c569115be9a68481da604bb2080c5))
