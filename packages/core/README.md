# @weareasterisk/hyphen-ui-core

## Storybook

You can see our published storybook here: https://hyphen-storybook.weareasterisk.com/

## `yarn dev` - automagically compile your code when you save it

You will probably also want to run `yarn storybook` at the root of the monorepo.

Additonally, you can run `yarn dev` at the root of the monorepo to achieve the same thing, plus
compiling the icons package.

## `yarn generate` - get 80% of your code done in half the time, 100% of the time

This generator was built with [Plop.js](https://plopjs.com/) -
[📜 See the documentation here](https://plopjs.com/documentation/#getting-started)

<img src="https://i.imgflip.com/4hlows.jpg" height="300" style="margin: 16px;"/>

### How to generate a component

1. Run `yarn generate`. This will prompt you to enter the name of the component you wish to create.
2. Enter the name of the component. Make sure not to use a name that is already taken (see the
   folder names of `src/components/*`)
3. Confirm that the command succeeded.
   - Check that `src/common/classes.ts`, `src/components/index.ts`, and
     `packages/core/src/components/_index.scss` were updated correctly
   - Check that `src/components/{{name}}/*` contains the appropriate files generated.

If you run into any issues, please share the issue on the
[#tech](https://weareasterisk.slack.com/archives/CQZH4RHTQ)] channel in Slack.

### Modifying the template

Our templates are located this folder `packages/core/_templates`.

[See the documentation here 📜](https://plopjs.com/documentation/#getting-started)
