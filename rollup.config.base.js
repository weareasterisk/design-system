import commonjs from "@rollup/plugin-commonjs"
import resolve from "@rollup/plugin-node-resolve"
import replace from "@rollup/plugin-replace"
import svgr from "@svgr/rollup"
import path from "path"
import nodeGlobals from "rollup-plugin-node-globals"
import external from "rollup-plugin-peer-deps-external"
import postcss from "rollup-plugin-postcss"
import { sizeSnapshot } from "rollup-plugin-size-snapshot"
import { terser } from "rollup-plugin-terser"
import typescript from "rollup-plugin-typescript2"

const pkg = require(path.join(process.cwd(), "package.json"))

export const globals = {
  "@weareasterisk/hyphen-ui-core": ["Hyphen-UI", "Core"],
  "@weareasterisk/hyphen-ui-icons": ["Hyphen-UI", "Icons"],
  classnames: "classNames",
  dom4: "window",
  react: "React",
  "react-dom": "ReactDOM",
  "react-popper": "ReactPopper",
  "react-transition-group": "ReactTransitionGroup",
  tslib: "window",
}

export default {
  treeshake: true,
  output: [
    {
      file: pkg.main,
      format: "cjs",
      exports: "named",
      sourcemap: false,
      globals,
    },
    {
      file: pkg.module,
      format: "esm",
      exports: "named",
      sourcemap: false,
      globals,
    },
  ],
  external: Object.keys(globals),
  plugins: [
    external(),
    postcss({
      config: {
        path: path.resolve(__dirname, "./postcss.config.js"),
      },
      extract: true,
      use: ["sass"],
    }),
    // url(),
    svgr(),
    resolve(),
    typescript({
      rollupCommonJSResolveHack: true,
      clean: true,
    }),
    commonjs(),
    nodeGlobals(),
    replace({ "process.env.NODE_ENV": JSON.stringify("production") }),
    sizeSnapshot({ snapshotPath: "size-snapshot.json" }),
    terser(),
  ],
}
