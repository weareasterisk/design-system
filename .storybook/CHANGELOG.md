# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.21](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.20...@asterisk/hyphen-ui-storybook@1.2.21) (2021-05-17)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.20](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.19...@asterisk/hyphen-ui-storybook@1.2.20) (2021-01-09)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.19](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.18...@asterisk/hyphen-ui-storybook@1.2.19) (2021-01-08)


### Bug Fixes

* rich text editor list indentation styles ([f853e7f](https://gitlab.com/weareasterisk/design-system/commit/f853e7ff8cf6aceb6254b3fe484abb4eee9a7983))





## [1.2.18](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.17...@asterisk/hyphen-ui-storybook@1.2.18) (2021-01-03)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.17](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.16...@asterisk/hyphen-ui-storybook@1.2.17) (2020-12-31)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.16](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.15...@asterisk/hyphen-ui-storybook@1.2.16) (2020-12-21)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.15](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.14...@asterisk/hyphen-ui-storybook@1.2.15) (2020-12-01)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.14](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.13...@asterisk/hyphen-ui-storybook@1.2.14) (2020-11-28)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.13](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.12...@asterisk/hyphen-ui-storybook@1.2.13) (2020-11-17)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.12](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.11...@asterisk/hyphen-ui-storybook@1.2.12) (2020-11-16)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.11](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.10...@asterisk/hyphen-ui-storybook@1.2.11) (2020-11-16)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.10](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.9...@asterisk/hyphen-ui-storybook@1.2.10) (2020-11-15)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.9](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.8...@asterisk/hyphen-ui-storybook@1.2.9) (2020-11-10)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.8](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.7...@asterisk/hyphen-ui-storybook@1.2.8) (2020-10-20)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.7](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.6...@asterisk/hyphen-ui-storybook@1.2.7) (2020-10-20)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.6](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.5...@asterisk/hyphen-ui-storybook@1.2.6) (2020-10-20)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.5](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.4...@asterisk/hyphen-ui-storybook@1.2.5) (2020-10-10)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.4](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.3...@asterisk/hyphen-ui-storybook@1.2.4) (2020-10-10)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.3](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.2...@asterisk/hyphen-ui-storybook@1.2.3) (2020-10-08)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.2](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.1...@asterisk/hyphen-ui-storybook@1.2.2) (2020-10-06)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





## [1.2.1](https://gitlab.com/weareasterisk/design-system/compare/@asterisk/hyphen-ui-storybook@1.2.0...@asterisk/hyphen-ui-storybook@1.2.1) (2020-10-06)

**Note:** Version bump only for package @asterisk/hyphen-ui-storybook





# 1.2.0 (2020-10-05)


### Bug Fixes

* **storybook:** re-enable docgen plugin and loader ([d8662c8](https://gitlab.com/weareasterisk/design-system/commit/d8662c8e72297c5bf0fe9fad39efb7baeea16d0c))
* add mdx type definition ([a57e8fa](https://gitlab.com/weareasterisk/design-system/commit/a57e8fab1f386fa4a8cb56326fbff37272f9f869))


### Features

* create global storybook configuration instead of per-project ([e2c3ccf](https://gitlab.com/weareasterisk/design-system/commit/e2c3ccf29e158a7d7649c9fbdf2553587d063b0e))
* turn storybook into a separate package ([3cfb9d0](https://gitlab.com/weareasterisk/design-system/commit/3cfb9d0b4ebf66b1a3400dc2ad31a30dbc2b71ca))





## [1.1.4](https://gitlab.com/weareasterisk/design-system/compare/@astetrisk/hyphen-ui-storybook@1.1.3...@astetrisk/hyphen-ui-storybook@1.1.4) (2020-09-24)

**Note:** Version bump only for package @astetrisk/hyphen-ui-storybook





## [1.1.3](https://gitlab.com/weareasterisk/design-system/compare/@astetrisk/hyphen-ui-storybook@1.1.2...@astetrisk/hyphen-ui-storybook@1.1.3) (2020-09-21)

**Note:** Version bump only for package @astetrisk/hyphen-ui-storybook





## [1.1.2](https://gitlab.com/weareasterisk/design-system/compare/@astetrisk/hyphen-ui-storybook@1.1.1...@astetrisk/hyphen-ui-storybook@1.1.2) (2020-09-12)


### Bug Fixes

* add mdx type definition ([a57e8fa](https://gitlab.com/weareasterisk/design-system/commit/a57e8fab1f386fa4a8cb56326fbff37272f9f869))





## [1.1.1](https://gitlab.com/weareasterisk/design-system/compare/@astetrisk/hyphen-ui-storybook@1.1.0...@astetrisk/hyphen-ui-storybook@1.1.1) (2020-09-05)

**Note:** Version bump only for package @astetrisk/hyphen-ui-storybook





# 1.1.0 (2020-09-04)


### Features

* create global storybook configuration instead of per-project ([e2c3ccf](https://gitlab.com/weareasterisk/design-system/commit/e2c3ccf29e158a7d7649c9fbdf2553587d063b0e))
* turn storybook into a separate package ([3cfb9d0](https://gitlab.com/weareasterisk/design-system/commit/3cfb9d0b4ebf66b1a3400dc2ad31a30dbc2b71ca))
