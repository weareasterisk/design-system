const path = require("path")
const { configure, setAddon } = require("@storybook/react")
const { IgnorePlugin } = require("webpack")

// automatically import all files ending in *.stories.tsx
// const req = require.context('..', true, /packages\/((?!node_modules).)*\/stories\/[^\/]+\.[j|t]sx?$/);

// function loadStories() {
//   req.keys().forEach(req);
// }

// configure(loadStories, module);

const TSDocgenPlugin = require("react-docgen-typescript-webpack-plugin")

module.exports = {
  stories: ["../packages/*/src/**/*.stories.@(tsx|ts|mdx)"],
  addons: [
    // "@storybook/preset-scss",
    // {
    //   name: "@storybook/preset-typescript",
    //   options: {
    //     tsLoaderOptions: {
    //       configFile: path.resolve(__dirname, './.storybook/tsconfig.json'),
    //       transpileOnly: true
    //     },
    //     forkTsCheckerWebpackPluginOptions: {
    //       typescript: {
    //         configFile: path.resolve(__dirname, './.storybook/tsconfig.json'),
    //       }
    //     },
    //     transpileManager: false,
    //   },
    // },
    "@storybook/addon-links",
    {
      name: "@storybook/addon-essentials",
      options: {
        docs: {
          configureJSX: true,
          sourceLoaderOptions: {
            injectStoryParameters: false,
          },
        }
      }
    },
    "@storybook/addon-knobs",
    "@storybook/addon-a11y",
    {
      name: "@storybook/addon-storysource",
      options: {
        loaderOptions: {
          injectStoryParameters: false,
        },
      },
    },
    "storybook-addon-react-docgen",
  ],
  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      include: path.resolve(__dirname, "../packages"),
      use: [
        {
          loader: require.resolve("awesome-typescript-loader"),
          options: {
            configFileName: path.resolve(__dirname, "./tsconfig.json"),
          },
        },
        {
          loader: require.resolve("react-docgen-typescript-loader"),
          options: {
            tsconfigPath: path.resolve(__dirname, "./tsconfig.json"),
          },
        }
      ],
    })
    config.module.rules.push({
      test: /\.scss$/,
      use: [
        { loader: "style-loader" },
        { loader: "css-loader" },
        {
          loader: "sass-loader",
          options: {
            implementation: require("node-sass"),
          },
        },
      ],
      include: [path.resolve(__dirname, "../packages"), path.resolve(__dirname, ".")],
    })
    config.plugins.push(new TSDocgenPlugin())
    config.plugins.push(new IgnorePlugin({
      resourceRegExp: /__ignored__$/,
      contextRegExp: /moment$/
    }))
    config.resolve.extensions.push(".ts", ".tsx", ".mdx")
    config.entry.push(path.resolve(__dirname, "./scss-loader.scss"))
    return config
  },
  typescript: {
    check: false,
    checkOptions: {
      configFile: path.resolve(__dirname, "./tsconfig.json"),
    },
    reactDocgen: "react-docgen-typescript",
    reactDocgenTypescriptOptions: {
      shouldExtractLiteralValuesFromEnum: true,
      propFilter: (prop) => (prop.parent ? !/node_modules/.test(prop.parent.fileName) : true),
    },
  },
}

// // require('!style-loader!css-loader!sass-loader!./scss-loader.scss');

// module.exports = {
//   stories: ['../packages/^\.\/[^\/]+\/src\/.*stories\.@(js|jsx|ts|tsx)$/', '../packages/^\.\/[^\/]+\/src\/.*stories\.mdx?$/'],
//   addons: ["@storybook/addon-links", "@storybook/addon-essentials", "@storybook/addon-knobs", "@storybook/addon-a11y"],
// };
