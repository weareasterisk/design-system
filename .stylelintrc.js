module.exports = {
  "extends": "stylelint-config-recommended-scss",
  "plugins": [
    "stylelint-scss",
  ],
  "rules": {
    "indentation": 2,
    "string-quotes": "double",
    "no-duplicate-selectors": true,
    "color-hex-length": "long",
    "color-named": "never",
    "selector-max-id": 0,
    "selector-attribute-quotes": "always",
    "declaration-block-trailing-semicolon": "always",
    "declaration-no-important": true,
    "declaration-colon-space-before": "never",
    "property-no-vendor-prefix": true,
    "value-no-vendor-prefix": true,
    "number-leading-zero": "always",
    "function-url-quotes": "always",
    "function-url-scheme-blacklist": ["ftp"],
    "font-family-name-quotes": "always-unless-keyword",
    "at-rule-no-vendor-prefix": true,
    "selector-no-vendor-prefix": true,
    "selector-max-universal": 0,
    "media-feature-name-no-vendor-prefix": true
  }
}
