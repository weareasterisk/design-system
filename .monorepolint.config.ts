module.exports = {
  rules: {
    ":package-script": [
      {
        options: {
          scripts: {
            clean: "rimraf dist build lib",
          },
        },
      },
    ],
    ":package-order": {
      options: {
        order: [
          "name",
          "version",
          "author",
          "description",
          "contributors",
          "url",
          "license",
          "private",
          "engines",
          "bin",
          "main",
          "module",
          "jsnext:main",
          "typings",
          "variables",
          "source",
          "style",
          "sideEffects",
          "workspaces",
          "husky",
          "lint-staged",
          "files",
          "scripts",
          "resolutions",
          "dependencies",
          "peerDependencies",
          "peerDependenciesMeta",
          "devDependencies",
          "optionalDependencies",
          "publishConfig",
        ],
      },
    },
    ":alphabetical-dependencies": true,
    // ":alphabetical-scripts": true,
    ":consistent-dependencies": true,
    ":banned-dependencies": {
      options: {
        bannedDependencies: ["lodash"],
      },
    },
    ":package-entry": {
      options: {
        entries: {
          author: "Asterisk Solutions <hello@weareasterisk.com>",
          license: "SEE LICENSE IN LICENSE",
        },
        entriesExists: ["name", "version", "private", "description"],
      },
    },
  },
}
