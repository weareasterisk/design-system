# Design System

## Usage

To use `@weareasterisk/hyphen-ui-core`, please refer to
https://gitlab.com/weareasterisk/design-system/-/packages/ and find the latest package

To use `@weareasterisk/hyphen-ui-icons`, please refer to
https://gitlab.com/weareasterisk/design-system/-/packages/ and find the latest package

## Development

1. Clone this repo:

```bash
git clone git@gitlab.com:weareasterisk/design-system.git
```

2. Install dependencies with Yarn

```bash
yarn
```

3. Start the development server (Grab some coffee or tea, this will take a while... > 5 min)

```bash
yarn dev && yarn storybook
```

4.

// Determine this: yarn storybook? yarn dev && yarn storybook?
